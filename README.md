<p align="center">
<img src="http://users.iit.uni-miskolc.hu/~kecskemeti/DISSECT-CF/logo.jpg"/>
</p>

# Overview AutoAdjustingConsolidator expansion 

This section describes the additions to DISSECT-CF of the autoadjust - 
approach. In the following sections at first the structure of the addition is 
described. Afterwards the different packages are explained and how the added
"AutoAdjustingConsolidator / AAC" as well as the classes belonging to doing experiments
with different traces are used. Then we move on with the current state and
what could be the next steps.

The latest branch on this repository is "dcf_dcfe_helper", additionally containing 
the "DISSECT-CF-EXAMPLES" and the "DistSysJavaHelpers".

## External software and versions

For the P3-algorithm this additional software is requried: "https://www.gurobi.com/products/gurobi-optimizer/", Version 8.1.1 has been used.
For the DISSECT-CF-EXAMPLES the master branch on this [commit](https://github.com/kecskemeti/dissect-cf-examples/commit/0482ae340d2f045e11f1cd2c0c2984c195907030)
with the comment "Allows easier reuse of VMs" has been used.
For the DistSysJavaHelpers the master branch on the latest [commit](https://github.com/kecskemeti/DistSysJavaHelpers/commit/431fa85427db4cde51ef139f40ee31e7e64c33b8)
with the comment "SWF reader points to SWF's website..." has been used.


## Project Structure

The main additions are located in package "hu.mta.sztaki.lpds.cloud.
simulator.iaas.autoadjust". The classes inside this package are used for
several parts of the consolidator and contain self-explaining comments. But
besides that, there are several sub-packages that contain the main functionalities.

1. "hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.actions": This package
	has so-called actions that are created during the consolidation. They
	are used by the MAPE-loop parts of the AutoAdjustingConsolidator and are
	also commented. To find out what each action does and what parameters are
	used in which way, have a look at the class "Executer.java" in package 
	"hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape".

2. "hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.experiments": Here are
	the classes located used to do experiments with the AAC. The most important
	class here is "PlanetLabExperiment.java", which uses traces out of "PlanetLab" and
	allows to simulate a situation with real workload. One can choose between two types 
	of experiments, either doing 20 or 288 iterations. This is specified
	within the global variables with the variable "numSamples". This is further explained
	in section "Current State". There are a few parameters to run an experiment in this 
	class, which have to be passed via the run configuration parameters:

		* @param args[0] Determines whether defaults should be used.
		* @param args[1] Determines whether general logging should be activated.
		* @param args[2] Contains the path to the trace to be used.
		* @param args[3] Determines whether extended logging (logging the autoadjust decisions and loads of each iteration) should be activated
	
	Example configuration: **true true C:\Users\r-pon\bitbucket\dissect-cf-auto-adjusting\traces\planetlab\20110303\" false**.
	With this configuration defaults are used and basic logging is enabled. The path has to 
	match the path where the planetlab traces are located. The traces are also uploaded in 
	this repository, located in "dissect-cf-auto-adjusting\traces\planetlab". There are 
	also the bitbrains-tracefiles located, but in the current version they are not used. 

	The class "ExperimentController" is used to do consecutive simulations of 
	the PlanetLabExperiment. There are also some parameters to consider:

		* @param args[0]: Enables normal logging.
		* @param args[1]: Enables extended logging.

	An example configuration could be: **true false**. This would be passed to each simulation 
	and allow basic logging and deny extended logging for each simulation, which will be
	collect in a single log - file. 
	
	Inside the main()-function one can specify the experiments that should be 
	done as "PlanetLabTestCases". Each instance of this class contains the parameters for 
	one simulation. The class "ExperimentResults.java" represents
	the results of a simulation like energy consumption, used machines etc. 
	For more details look at the comments inside the mentioned class.

3. "hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape": The core
	functionality of the AAC. An "AutoAdjustingConsolidator.java" instance contains all
	parts that are used to do consolidation according to the autoadjust-approach.
	For further insides see the Paper "Auto-Adjusting Self-Adaptive Software Systems"
	by Zoltán Ádám Mann and Andreas Metzger.

	As a quick overview, a MAPE-loop is used, represented by the classes
	"Monitor.java", "Analyzer.java", "Planner.java" and "Executer.java". Their
	individual functionalities are documented inside the classes themself. You
	can find general comments regarding their usage at the top of the files and
	specific information on algorithms and other important functions in the function -
	specific comments. Important to mention is the class "AACHelper.java", that offers static functions
	and manages all parameters used for the consolidation. The helper gets initialized 
	during the creation of the AAC.

4. "hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information": There
	are all helper classes located like comparators and the inputs and outputs used
	and / or produced by the algorithms. 

5. Additional information: The consolidation is performed using a set of defined paramters. 
	The file "autoAdjustDefaults.xml" is located inside "dissect-cf-auto-adjusting\Code", before the 
	src - folder. The results of the ExperimentController are called "autoAdjustResults.csv"
	and are located in the same path. Further debugging information like loads and the
	decisions made, chosen by the AAC, are also saved there in separate files. The
	letter and number combinations at the beginning of each file indicate for what
	configuration the specific file has been created.

## Current State

The aim of this project is to compare the autoadjust-approach with the usage
of each of its algorithm-configuration on its own and to show, that the AAC
performs better using autoadjust then using only one configuration in general.
For a detailed explanation read the previously mentioned paper.

For the experiment execution and to produce the current results we used two types of
experiments. In general at the current state we use 40 PMs and 150 VMs.
The first type (small - scale) uses 20 iterations and the second typ (large - scale)
uses 288 iterations.

The current results are located inside "dissect-cf-auto-adjusting\ExperimentsResults".
One can find Excel-files there containing the results of previously done
experiments. The current problems and some thoughts to them follow:

1. The A3/P3-configuration: The predictions created in A3 do not improve the
	results. For that different n-values (the amount of values of the last iterations
	to consider for the predictions) are used, but with no luck. Normally one
	would expect that the predictions should decrease the number of used
	PMs in average as well as the overload because of the gurobi optimizer used
	in P3, but we can only minimize one of both metrics at the moment. With the
	current version we can achieve the least machines used overall, with the code in line
	478 used additionally to the current version we achieve best overload results.
2. A1-predictions: Same problem as before, the predictions make the results worse than
	the configuration without using predictions. One thought about this problem is
	that the PM-loads change very drastically or not at all, which could be
	responsible for bad predictions.
3. The usage of the correct ContextSituationClasses: This is done in the class
	"AutoAdjustingConsolidator" in function "determineContextSituationClass()".
	Currently the usage of each configuration is not as expected and the value
	of "thresholdLoadBehaviour" in the configuration file should be adjusted to
	allow better usage of the different configurations to get better results with
	the auto-adjust approach.
4. Changes to DISSECT-CF: There are some changes made to the simulator itself,
	because there were some problems with simulating the wanted behaviour correctly
	in the past. 
	
	4.1: At the moment there are no direct migrations done but instead of that the 
		VM is suspended and resumed on the target Host. This is done
		to make sure that each migration can be done and there is no problem with hosted
		VMs that still have to be migrated and because of that migrations to their host
		are cancelled due to too little resources. 
	
	4.2: Another important change is the behaviour of ResourceConsumptions, so that 
		they can be cancelled directly and
		resumed by other Consumers (which was not allowed before in DISSECT-CF). This
		was done to prevent situations where VMs do not fit on their host PMs anymore after
		the usage of the function "doVmSizing()" in "PlanetLabExperiment". 
	
	4.3: Furthermore, the needed time to do for example a PM startup or shutdown has 
		been adjusted (as well as VM migration, suspend and resume times). This is done 
		to ensure that all actions of the Executer are done before the "doVmSizing()" 
		is called to prevent race conditions and errors regarding the ResourceConsumptions. 
		All changes to the simulator are marked with either a "FIXME" or a "TODO" and a comment.
	
At this point the current state of the project is described and the description of
DISSECT-CF follows, written by Gabor Kecskemeti and published on the main repository
of DISSECT-CF: https://github.com/kecskemeti/dissect-cf


# Overview DISSECT-CF

This package represents the DIScrete event baSed Energy Consumption simulaTor
for Clouds and Federations (DISSECT-CF).

It is intended as a lightweight cloud simulator that enables easy customization
and experimentation. It was designed for research purposes only so researchers
could experiment with various internal cloud behavior changes without the need
to actually have one or even multiple cloud infrastructures at hand.

The development of DISSECT-CF started in MTA SZTAKI in 2012, major contirbutions
and design elements were incorporated at the University of Innsbruck. 

When using the simulator for scientific research please cite the following paper:
[Gabor Kecskemeti: DISSECT-CF: A simulator to foster energy-aware scheduling in infrastructure clouds. In Simulation Modelling Practice and Theory. 2015.](https://www.researchgate.net/publication/277297657_DISSECT-CF_a_simulator_to_foster_energy-aware_scheduling_in_infrastructure_clouds) DOI: [10.1016/j.simpat.2015.05.009](http://dx.doi.org/10.1016/j.simpat.2015.05.009)

Website:
https://github.com/kecskemeti/dissect-cf

Documentation website:
https://kecskemeti.github.io/dissect-cf

Archive for past (even pre-github) releases:
http://users.iit.uni-miskolc.hu/~kecskemeti/DISSECT-CF

Licensing:
GNU Lesser General Public License 3 and later

Optimisations on the code were done using the java profiler called [jprofiler](http://www.ej-technologies.com/products/jprofiler/overview.html). 

## Compilation & Installation

Prerequisites: [Apache Maven 3.](http://maven.apache.org/), Java 1.6

After cloning, run the following in the main dir of the checkout:

`mvn clean install javadoc:javadoc`

This command will download all other prerequisites for compilation and testing. Then it will compile the complete sources of DISSECT-CF as well as its test classes. If the compilation is successful, then the tests are executed. In case no test fails, maven proceeds with the packaging and istallation.

The installed simulator will be located in the default maven repository's (e.g., `~/.m2/repository`) following directory: 

`at/ac/uibk/dps/cloud/simulator/dissect-cf/[VERSION]/dissect-cf-[VERSION].jar`

Where `[VERSION]` stands for the currently installed version of the simulator.

The documentation for the simulator's java API will be generated in the following subfolder of the main dir of the checkout:

`target/site/apidocs`

## Getting started

### Minimum runtime dependencies
DISSECT-CF depends on the following libraries during its runtime: 
* Java 1.6
* [GNU trove4j 3.0.3](http://trove.starlight-systems.com)
* [Apache Commons Lang3 3.4](https://commons.apache.org/proper/commons-lang/)

###### Hint:
Although these dependencies can be collected individually. If one installed the simulator according to description above, then except for Java6, all dependencies are located in the local maven repository (e.g., `~/.m2/repository`).  

### Overview of the basic functionalities  

The test cases of the simulator contain many useful examples so one can start working right away with the simulator. In the following list one can find the most essential bits to get to know the internals and the behavior of the simulator:
* Basic time and event management:
  * `at.ac.uibk.dps.cloud.simulator.test.simple.TimedTest.singleEventFire`
  * `at.ac.uibk.dps.cloud.simulator.test.simple.TimedTest.repeatedEventFire`
* Basic use of one time events:
  * `at.ac.uibk.dps.cloud.simulator.test.simple.DeferredEventTest`
* Simple physical machine management and use:
  * `at.ac.uibk.dps.cloud.simulator.test.simple.cloud.PMTest.constructionTest`
  * `at.ac.uibk.dps.cloud.simulator.test.simple.cloud.PMTest.simpleTwoPhasedSmallVMRequest`
* Simple IaaS construction and use:
  * `at.ac.uibk.dps.cloud.simulator.test.simple.cloud.IaaSServiceTest.repoRegistrationTest` - to add storage
  * `at.ac.uibk.dps.cloud.simulator.test.simple.cloud.IaaSServiceTest.capacityMaintenanceTest` - pm addition and overall capacity monitoring
  * `at.ac.uibk.dps.cloud.simulator.test.simple.cloud.IaaSServiceTest.vmRequestTest` - scheduled vm creation
* Starter kit for VM operations:
  *  `at.ac.uibk.dps.cloud.simulator.test.simple.cloud.VMTest.subscriptionTest` - VM state monitoring
  *  `at.ac.uibk.dps.cloud.simulator.test.simple.cloud.VMTest.taskKillingSwitchOff` - New compute task creation
* General use of the resource sharing foundation:
  *  `at.ac.uibk.dps.cloud.simulator.test.simple.cloud.ResourceConsumptionTest.testConsumption`
* Networking basics:
  *  `at.ac.uibk.dps.cloud.simulator.test.simple.cloud.NetworkNodeTest.intraNodeTransferTest`
 
More elaborate examples can be found in the [dissect-cf-examples project](https://github.com/kecskemeti/dissect-cf-examples) and in the [dcf-exercises project](https://github.com/kecskemeti/dcf-exercises).

Also, the [wiki](https://github.com/kecskemeti/dissect-cf/wiki) provides further insights to advanced topics (like creating custom schedulers) and offers a [FAQ](https://github.com/kecskemeti/dissect-cf/wiki/Frequently-Asked-Questions). Apart from contributing with code, feel free to contribute there with documentation as well.

Do you still have some questions? If so, then please share them with the simulator's user and developer community at our [Q&A forum](https://groups.google.com/forum/#!forum/dissect-cf-discuss) - a registration is needed to send your questions in.

## Remarks

##### Warning: the master branch of the simulator is intended as a development branch, and might not contain a functional version!

<p align="left">
Build and testing status of the code in the repository:
<img src="https://travis-ci.org/kecskemeti/dissect-cf.svg?branch=master"/>
</p>
