autoadjust;AnalyzeParameter;PlanningParameter;Frequency;total energy consumption in kWh;average amount of active PMs;Total amount of migrations;total amount of VM suspensions;total amount of VM resumes;total amount of PM starts;total amount of PM stops;total overload (tpp) [%];total overload;total load;total duration in ms;nrIterations;total time of suspended VMs in ms
true;A1;P1;F2;11.115;21;0;334;334;7;26;0.022825;38740.0;1697245.0;687;22;3533
false;A1;P1;F1;11.483;22;0;484;484;6;24;0.025217;38957.0;1544895.0;480;20;3120
false;A2;P2;F1;11.056;21;0;195;195;4;23;0.022474;34720.0;1544895.0;200;20;511
false;A3;P3;F1;11.111;21;0;485;484;11;37;0.024696;38152.0;1544895.0;1131517;20;2497
false;A4;P4;F1;9.823;19;0;121;121;7;34;0.034241;52898.0;1544895.0;148;20;223
false;A1;P1;F2;11.04;21;0;695;695;5;24;0.013119;40534.0;3089790.0;517;40;3102
false;A2;P2;F2;10.755;20;0;195;195;4;23;0.011237;34720.0;3089790.0;173;40;523
false;A3;P3;F2;10.792;20;0;776;775;24;69;0.015656;48374.0;3089790.0;2119807;40;83038
false;A4;P4;F2;9.431;18;0;121;121;7;34;0.029531;91245.0;3089790.0;257;40;354
