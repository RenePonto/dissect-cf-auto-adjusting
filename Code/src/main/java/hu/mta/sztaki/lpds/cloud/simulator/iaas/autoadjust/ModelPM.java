package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.ModelVmIdComparator;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.AlterableResourceConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ConstantConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.UnalterableConstraintsPropagator;

/**
 * 		@author Julian Bellendorf, Rene Ponto
 *
 * 		This class represents a PhysicalMachine. It contains the original PM, 
 * 		its VMs in an ArrayList, the total resources as ConstantConstraints, 
 * 		the available resources as a ResourceVector and the possible States. 
 * 		The States are not the same as inside the simulator, because for 
 * 		migrating it is useful to introduce some new States for the allocation 
 * 		of the given PM.
 */
public class ModelPM {

	private final PhysicalMachine pm;		// the real PM inside the simulator
	private final List<ModelVM> vmList;
	private final int number;
	private double lowerThreshold, upperThreshold;

	/** all used ResourceConstraints for one PM */
	private final AlterableResourceConstraints consumedResources;
	private final AlterableResourceConstraints freeResources;
	public final UnalterableConstraintsPropagator consumed;
	public final UnalterableConstraintsPropagator free;
	private final AlterableResourceConstraints reserved;
	private final ConstantConstraints lowerThrResources, upperThrResources;

	/** the state of the PM */
	private State state;

	/**
	 * 
	 * 
	 * @param pm The real Physical Machine in the Simulator.
	 * @param number The number of the PM in its IaaS, used for debugging.
	 */
	/**
	 * This represents a Physical Machine of the simulator. It is abstract and 
	 * inherits only the methods and properties which are necessary to do the 
	 * consolidation inside this model. The threshold is defined by the start 
	 * of the consolidator and a percentage of the total resources. If the 
	 * allocation is  greater than the upper bound or less than the lower bound, 
	 * the state of the PM switches to OVERALLOCATED or UNDERALLOCATED.
	 * 
	 * @param pm The PhysicalMachine for what this model is created.
	 * @param lowerThreshold
	 * @param upperThreshold
	 */
	public ModelPM(final PhysicalMachine pm, double lowerThreshold, double upperThreshold, int id) {
		vmList = new ArrayList<>();
		
		this.pm = pm;
//		this.number = id;
		number = pm.hashCode();
		
		this.lowerThreshold = lowerThreshold;
		this.upperThreshold = upperThreshold;
		
		final AlterableResourceConstraints lower = new AlterableResourceConstraints(pm.getCapacities());
		final AlterableResourceConstraints upper = new AlterableResourceConstraints(pm.getCapacities());
		lower.multiply(lowerThreshold);
		upper.multiply(upperThreshold);
		this.lowerThrResources = new ConstantConstraints(lower);
		this.upperThrResources = new ConstantConstraints(upper);
		
		// set the current state depending on the state of the real PM
		if(pm.getState() == PhysicalMachine.State.SWITCHINGOFF || pm.getState() == PhysicalMachine.State.OFF) {
			setState(State.EMPTY_OFF);
		}
		else {
			if(pm.getState() == PhysicalMachine.State.RUNNING) {
				setState(State.NORMAL_RUNNING);
			}
			else if(pm.getState() == PhysicalMachine.State.SWITCHINGON) {
				setState(State.EMPTY_RUNNING);
			}
		}

		// initialize the resources 
		consumedResources = new AlterableResourceConstraints(0, pm.getCapacities().getRequiredProcessingPower(), 0);
		freeResources = new AlterableResourceConstraints(pm.getCapacities());
		consumed = new UnalterableConstraintsPropagator(consumedResources);
		free = new UnalterableConstraintsPropagator(freeResources);
		reserved = new AlterableResourceConstraints(ConstantConstraints.noResources);
	}

	/**
	 * Adds a given VM to this PM.
	 * 
	 * @param vm The VM which is going to be put on this PM.
	 */
	public boolean addVM(ModelVM vm) {		
		vmList.add(vm);
		ResourceConstraints rc = vm.getResources();
		consumedResources.singleAdd(rc);
		freeResources.subtract(rc);
		checkAllocation();
		
		// adding was successful
		return true;
	}

	/**
	 * Removes a given VM of this PM.
	 * 
	 * @param vm The VM which is going to be removed of this PM.
	 * 
	 * @return true if there has not occurred any problem.
	 */
	public boolean removeVM(ModelVM vm) {
		vmList.remove(vm);
		// adapt the consumed resources
		ResourceConstraints rc = vm.getResources();
		consumedResources.subtract(rc);
		freeResources.singleAdd(rc);
		checkAllocation();
		
		// removing was successful
		return true;
	}

	/**
	 * Migration of a VM from this PM to another.
	 * 
	 * @param vm The VM which is going to be migrated.
	 * @param target The target PM where to migrate.
	 */	
	public void migrateVM(ModelVM vm, ModelPM target) {
		target.addVM(vm);		
		this.removeVM(vm);
		vm.sethostPM(target);
	}

	/**
	 * Reserves resources for possible migrations. 
	 * 
	 * @param toReserve The ResourceConstraints of the VM which could be hosted in the future.
	 */
	public void reserveResources(ResourceConstraints toReserve) {
		this.reserved.add(toReserve);
	}
	
	/**
	 * Frees internal capacities of this PM.
	 * 
	 * @param toRemove The ResourceConstraints that will be removed.
	 */
	public void removeReservedResources(ResourceConstraints toRemove) {
		this.reserved.subtract(toRemove);
	}
	
	/**
	 * Resets the reserved resources.
	 */
	public void setResourcesFree() {
		this.reserved.subtract(reserved);
	}

	/**
	 * Checks if there are any VMs on this PM.
	 * 
	 * @return true if VMs are running on this PM.
	 */
	public boolean isHostingVMs() {
		return !getVMs().isEmpty();
	}
	
	/**
	 * The possible States for a PM in this abstract model.
	 * 
	 * For understanding, we need the 'double'-states because of the graph. If we shut down 
	 * a PM and have to restart it again, it would be an unnecessary action, so we mark them 
	 * as for example EMPTY_RUNNING or EMPTY_OFF. For the allocation we only have the x_RUNNING 
	 * State because the check of the allocation can only occur if the PM is running, otherwise it 
	 * would be empty.
	 * 
	 * Additionally we have one other State for OVERALLOCATED_RUNNING and UNDERALLOCATED_RUNNING,
	 * UNCHANGEABLE_OVERALLOCATED and UNCHANGEABLE_UNDERALLOCATED.
	 * This is important because of the possibility to determine how often it has been tried
	 * to migrate this PM or VMs of this PM without success. So the State UNCHANGEABLE_x symbolizes 
	 * that it will not be possible to get a successful migration in the future. In that case the 
	 * UNCHANGEABLE_x PM will be skipped inside the algorithm for now. 
	 */	
	public static enum State {		
		/**
		 * There are actually no VMs on this PM, PM is not running
		 */
		EMPTY_OFF,		
		/**
		 * PM is running and empty
		 */
		EMPTY_RUNNING,		
		/**
		 * allocation is between the upper and lower threshold, PM is running
		 */
		NORMAL_RUNNING,		
		/**
		 * allocation is lower than the lower threshold, PM is running
		 */
		UNDERALLOCATED_RUNNING,		
		/**
		 * allocation is higher than the upper threshold, PM is running
		 */
		OVERALLOCATED_RUNNING
	};
	
	/**
	 * Checks if this PM is currently running.
	 * 
	 * @return True, if the state of this PM is not EMPTY_OFF, false otherwise.
	 */
	public boolean isRunning() {
		return !getState().equals(ModelPM.State.EMPTY_OFF);
	}
	
	/**
	 * Changes the state of the PM so the graph can give the switch off information
	 * later to the simulation.
	 */	
	public void switchOff() {
		this.setState(State.EMPTY_OFF);
	}	
	/**
	 * Changes the state of the PM so the graph can give the switch on information
	 * later to the simulation.
	 */	
	public void switchOn() {
		this.setState(State.EMPTY_RUNNING);
	}
	
	/**
	 * Setter for the state of this PM.
	 * 
	 * @param state The new state for this PM.
	 */	
	public void setState(State state) {
		this.state = state;
	}
	
	/**
	 * In this method the status of this PM is considered. For that, the methods
	 * underAllocated() and overAllocated() are written. It is recognized if the last
	 * migration on this PM was not successful and in case of that the state remains
	 * unchanged.
	 */	
	public void checkAllocation() {
		if(isHostingVMs() == false) {
			setState(State.EMPTY_RUNNING);
		}
		else if(isUnderAllocated())  {
			setState(State.UNDERALLOCATED_RUNNING);
		}
		else {
			if(isOverAllocated()) {
					setState(State.OVERALLOCATED_RUNNING);
			}
			else
				setState(State.NORMAL_RUNNING);
			}
	}
	
	/**
	 * Method for checking if the actual PM is overAllocated.
	 * 
	 * @return True if overAllocated, false otherwise.
	 */	
	public boolean isOverAllocated() {
//		return consumedResources.getTotalProcessingPower() > upperThrResources.getTotalProcessingPower() || 
//				consumedResources.getRequiredMemory() > upperThrResources.getRequiredMemory();
		return consumedResources.getTotalProcessingPower() > upperThrResources.getTotalProcessingPower();
	}
	
	/**
	 * Method for checking if the current PM is underAllocated.
	 * 
	 * @return True if it is underAllocated, false otherwise.
	 */	
	public boolean isUnderAllocated() {
//		return consumedResources.getTotalProcessingPower() < lowerThrResources.getTotalProcessingPower() || 
//				consumedResources.getRequiredMemory() < lowerThrResources.getRequiredMemory();
		return consumedResources.getTotalProcessingPower() < lowerThrResources.getTotalProcessingPower();
	}
	
	/**
	 * This method checks if a given VM can be hosted on this PM without changing the state to overAllocated.
	 * 
	 * @param toAdd The VM which shall be added.
	 * 
	 * @return True if the VM can be added.
	 */
	public boolean isMigrationPossible(final ResourceConstraints toAdd) {
		final AlterableResourceConstraints available = new AlterableResourceConstraints(upperThrResources);
		available.subtract(consumedResources);
		available.subtract(reserved);
		
		// compare the available resources with the resources needed. 
		if(toAdd.getTotalProcessingPower() <= available.getTotalProcessingPower() && 
				toAdd.getRequiredMemory() <= available.getRequiredMemory() &&
				toAdd.getRequiredProcessingPower() <= available.getRequiredProcessingPower()) {
				return true;
		}
		else
			return false;
	}
	
	/**
	 * Getter for a specific VM.
	 * 
	 * @param position The desired position where the VM is.
	 * 
	 * @return The VM on this position, null if there is none.
	 */	
	public ModelVM getVM(int position) {
		if(getVMs().size() <= position) {
			return null;
		}
		else
			return getVMs().get(position);
	}
	
	/**
	 * Get the list which contains all actual running VMs on this PM.
	 * 
	 * @return The list which contains all actual running VMs on this PM.
	 */
	public List <ModelVM> getVMs() {
		return vmList;		
	}
	
	/**
	 * Getter for the non-abstract version of this PM.
	 * 
	 * @return The matching PM inside the simulator.
	 */
	public PhysicalMachine getPM() {
		return pm;
	}
	
	/**
	 * Getter for the state.
	 * 
	 * @return The current state of this PM.
	 */	
	public State getState() {
		return this.state;
	}
	
	/**
	 * Get the consumed resources of this PM.
	 * 
	 * @return cores, perCoreProcessing and memory of the PM as ResourceConstraints.
	 */	
	public ResourceConstraints getConsumedResources() {
		return consumed;
	}	
	
	/**
	 * Get the total resources of this PM.
	 * 
	 * @return cores, perCoreProcessing and memory of the PM as ResourceConstraints.
	 */	
	public ResourceConstraints getTotalResources() {
		return pm.getCapacities();
	}

	/**
	 * Getter.
	 * 
	 * @return The lower threshold for the PMs.
	 */
	public ConstantConstraints getLowerThreshold() {
		return lowerThrResources;
	}

	/**
	 * Getter.
	 * 
	 * @return The upper threshold ResourceConstraints for the PMs.
	 */
	public ConstantConstraints getUpperThreshold() {
		return upperThrResources;
	}
	
	/**
	 * 
	 * @return The lowerThreshold ResourceConstraints as a percentage.
	 */
	public double getLowerThresholdValue() {
		return lowerThreshold;
	}
	
	/**
	 * 
	 * @return The upperThreshold as a percentage.
	 */
	public double getUpperThresholdValue() {
		return upperThreshold;
	}
	
	/**
	 * Getter.
	 * 
	 * @return The id of this PM.
	 */
	@Override
	public int hashCode() {
		return number;
	}
	
	/**
	 * toString() is used for debugging and contains the number of the PM in the IaaS and its actual VMs.
	 * 
	 * @return A string containing the PMs number, the resources (cap and load), the state and its VMs.
	 */	
	public String toString() {
		String result = "PM " + number + ", free = " + "[C: " + freeResources.getRequiredCPUs() + " P: " + 
				freeResources.getRequiredProcessingPower() + " M: " + freeResources.getRequiredMemory() + "]" +
				", curr = " + "[C: " + consumedResources.getRequiredCPUs() + " P: " + 
				consumedResources.getRequiredProcessingPower() + " M: " + consumedResources.getRequiredMemory() + "]" + 
				", cap = " + "[C: " + getTotalResources().getRequiredCPUs() + " P: " + 
				getTotalResources().getRequiredProcessingPower() + " M: " + getTotalResources().getRequiredMemory() + "]" + 
				", state = " + state + ", hosted VMs = ";
		boolean first = true;
		for(ModelVM vm : vmList) {
			if(!first)
				result = result + ", ";
			result = result + vm.toString();
			first = false;
		}
		result = result + '\n';
		return result;
	}
	
	public void sortVmList() {
		Collections.sort(vmList, new ModelVmIdComparator());
	}
	
}