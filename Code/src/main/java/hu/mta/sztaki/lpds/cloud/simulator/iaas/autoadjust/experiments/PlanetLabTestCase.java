package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.experiments;

import java.util.logging.Logger;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AutoAdjustingConsolidator.AnalyzeParameter;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AutoAdjustingConsolidator.FrequencyParameter;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AutoAdjustingConsolidator.PlanningParameter;

/**
 * This class runs one test with given parameters.
 */
public class PlanetLabTestCase {
	
	/** determines if the test runs with static parameters or dynamic ones */
	private boolean useDefaults;
	private boolean useLogging;
	private boolean doExtendedLogging;
	
	/** parameters */
	private AnalyzeParameter analyzeParameter;
	private PlanningParameter planningParameter;
	private FrequencyParameter frequency;
	
	/**
	 * This empty constructor uses the standard version of the AutoAdjustConsolidator,
	 * which means using autoadjust and no defaults.
	 */
	public PlanetLabTestCase(boolean useLogging, boolean doExtendedLogging) {
		useDefaults = false;
		this.useLogging = useLogging;
		this.doExtendedLogging = doExtendedLogging;
		analyzeParameter = ExperimentController.defaultAnalyze;
		planningParameter = ExperimentController.defaultPlanning;
		frequency = ExperimentController.defaultFrequency;
	}
	
	/**
	 * Create a testcase using specific default values and without autoadjust.
	 * 
	 * @param analyzeParameter The value for the analyze - parameter.
	 * @param planningParameter The value for the planning - parameter.
	 * @param frequency The value for the frequency.
	 */
	public PlanetLabTestCase(AnalyzeParameter analyzeParameter, PlanningParameter planningParameter,
			FrequencyParameter frequency, boolean useLogging, boolean doExtendedLogging) {
		useDefaults = true;
		this.useLogging = useLogging;
		this.doExtendedLogging = doExtendedLogging;
		this.analyzeParameter = analyzeParameter;
		this.planningParameter = planningParameter;
		this.frequency = frequency;			
	}
	
	public String getAnalyzeParameter() {
		return analyzeParameter.toString();
	}
	
	public String getPlanningParameter() {
		return planningParameter.toString();
	}
	
	public String getFrequencyParameter() {
		return frequency.toString();
	}
	
	/**
	 * Creates a new instance of a PlanetLabExperiment and runs the experiment.
	 * Then the results are going to be saved.
	 */
	public ExperimentResults executeExperiment() {
		// the String has to contain the following arguments:
		// 1. useDefaults 2. useLogging 3. trace path
		String[] run = new String[] {"" + useDefaults, "" + useLogging, 
				"C:\\Users\\r-pon\\bitbucket\\dissect-cf-auto-adjusting\\traces\\planetlab\\20110303\\", "" + doExtendedLogging};
		
		// run the experiment
		ExperimentResults results = null;
		try {
			results = PlanetLabExperiment.runOneExperiment(run);
		} catch (Exception e1) {
			// if any exceptions occurs, stop the simulation and print it to the log
			Logger.getGlobal().info(e1.toString());
			e1.printStackTrace();
			System.exit(-1);
		}
		
		// add the constraints
		results.setSimulationConstraints(!useDefaults, analyzeParameter, planningParameter, frequency);
		
		// save the results
//		Logger.getGlobal().info("Simulation is done, now collecting the results.");
		
		return results;
		
	}
	
	public boolean isAutoAdjustUsed() {
		return !useDefaults;
	}
	
}
