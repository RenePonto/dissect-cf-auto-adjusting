package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information;

import java.util.Comparator;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;

public class PmIdComparator implements Comparator<PhysicalMachine> {

	public PmIdComparator() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int compare(PhysicalMachine o1, PhysicalMachine o2) {
		if(o1.hashCode() < o2.hashCode()) {
			return -1;
		}
		else if(o1.hashCode() == o2.hashCode()) {
			return 0;
		}
		else {
			return 1;
		}
		
	}

}
