package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust;

import java.util.ArrayList;
import java.util.List;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;

/**
 * Stores all necessary information about a given VM including the actual VM,
 * the current Load, the hosting PM and the previous measures of the load of this
 * VM. This object is created by Monitor and used by Analyzer and the AutoAdjusting-
 * Consolidator.
 */
public class VMInformation {
	
	/** current objects */
	private VirtualMachine vm;
	private PhysicalMachine hostPM;
	private ResourceConstraints currentLoad;
	private boolean suspended;
	private PMInformation hostPMInfo;
	
	/** historical objects */
	private int amountOfPrevious = 0;
	private List<ResourceConstraints> previousLoads;
	
	/**
	 * Creates a new VMInformation-object for the specified VM.
	 * 
	 * @param vm The VM this information is based on.
	 * @param host The hosting PM of this VM.
	 * @param currentLoad The needed resources of this VM to be hosted.
	 * @param amountOfPrevious The amount of previous measures to keep.
	 */
	public VMInformation(VirtualMachine vm, PhysicalMachine host, ResourceConstraints currentLoad, 
			int amountOfPrevious, boolean suspended) {
		this.vm = vm;
		hostPM = host;
		this.currentLoad = currentLoad;
		this.suspended = suspended;
		
		this.amountOfPrevious = amountOfPrevious;
		previousLoads = new ArrayList<ResourceConstraints>();
	}
	
	public int getAmountOfPrevious() {
		return amountOfPrevious;
	}	
	
	/**
	 * @return the hostPMInfo
	 */
	public PMInformation getHostPMInfo() {
		return hostPMInfo;
	}

	/**
	 * @param hostPMInfo the hostPMInfo to set
	 */
	public void setHostPMInfo(PMInformation hostPMInfo) {
		this.hostPMInfo = hostPMInfo;
	}

	/**
	 * Adds a new measure to this information as the current load. The previous
	 * current load is moved to a list containing the previous values. If the amount
	 * of elements inside the list is longer than previously defined by 'n', the oldest
	 * values is removed.
	 * 
	 * @param newLoad The new load to be added.
	 */
	public void addLoad(ResourceConstraints load) {
		previousLoads.add(0, currentLoad);
		while(previousLoads.size() > amountOfPrevious) {
			previousLoads.remove(amountOfPrevious);
		}
		currentLoad = load;
	}
	
	/**
	 * Get the VM this information is based on.
	 * 
	 * @return The VM this information is based on.
	 */
	public VirtualMachine getVM() {
		return vm;
	}
	
	/**
	 * Get the host PM of this VM.
	 * 
	 * @return The host PM of this VM.
	 */
	public PhysicalMachine getHost() {
		return hostPM;
	}
	
	/**
	 * Get the latest needed resources of this VM.
	 * 
	 * @return The latest needed resources of this VM.
	 */
	public ResourceConstraints getLoad() {
		return currentLoad;
	}
	
	/**
	 * Get the previous measures of this VM.
	 * 
	 * @return A list containing all n previous measures.
	 */
	public List<ResourceConstraints> getPrevious() {
		return previousLoads;
	}
	
	/**
	 * Sets a new hosting PM of this VM. Has to be called after migration.
	 * 
	 * @param pm The new host.
	 */
	public void setHost(PhysicalMachine pm) {
		this.hostPM = pm;
	}
	
	/**
	 * An easier way to determine if the VM of this information is suspended.
	 * 
	 * @return True if the VM is suspended, false otherwise.
	 */
	public boolean isSuspended() {
		return suspended;
	}
	
	/**
	 * Sets the suspended state for this VM.
	 * 
	 * @param suspended True if the regarding VM is suspended, false otherwise.
	 */
	public void setSuspended(boolean suspended) {
		this.suspended = suspended;
	}
	
	public String toString() {
		// check if there is a host for this VM, otherwise do not include the host in the string
		if(this.hostPM != null) {
			return "VMInfo for VM " + vm.hashCode() + ", hostPM: " + hostPM.hashCode() + " " + currentLoad.toString();
		}
		else {
			return "VMInfo for VM " + vm.hashCode() + ", unplaced, " + " " + currentLoad.toString();
		}
		
	}

}
