package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.actions;

import java.util.List;
import java.util.logging.Logger;

import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VMManager.VMManagementException;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelPM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelVM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AACHelper;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine.State;
import hu.mta.sztaki.lpds.cloud.simulator.io.NetworkNode.NetworkException;

/**
 * This class stores actions, which need to start a PM in the simulator.
 */
public class SuspensionAction extends Action implements VirtualMachine.StateChange {
	
	/** reference to the ModelVM */
	protected  ModelVM toSuspend;
	
	/** reference to the hosPM */
	protected  ModelPM currentHost;
	
	public SuspensionAction(int id, ModelVM vm) {
		super(id);
		toSuspend = vm;
		currentHost = toSuspend.gethostPM();
	}

	@Override
	public void determinePredecessors(List<Action> actions) {
		// there should be no migration before
	}

	@Override
	public void execute() {
		Logger.getGlobal().info("Executing at " + Timed.getFireCount() + ": " + toString());
		VirtualMachine vm = toSuspend.getVM();
		
		if(vm.getState().equals(State.SUSPENDED)) {
			// TODO this needs more investigation
			Logger.getGlobal().info("VM is suspended -> there is nothing to do");
			AACHelper.failedSuspensions++;
			finished();
		}
		else if(!vm.getState().equals(State.RUNNING)) {
			Logger.getGlobal().info("VM is not running -> there is nothing to do");
			AACHelper.failedSuspensions++;
			finished();
		}
		else {
			
			vm.subscribeStateChange(this);		// observe the VM which shall be suspended
			try {
				
				vm.suspend();
				
				// increase the counter
				AACHelper.suspensionCounter++;
				AACHelper.suspensions++;
				
				// start measuring the time
				vm.startSuspendedTimer();
				
				Logger.getGlobal().info("Suspension was successful: " + toString());
			} catch (VMManagementException e) {
				AACHelper.failedSuspensions++;
				finished();
				
				e.printStackTrace();
			} catch (NetworkException e) {
				AACHelper.failedSuspensions++;
				finished();
				
				e.printStackTrace();
			}
			
		}		
		
	}
	
	public ModelVM getModelVM() {
		return toSuspend;
	}
	
	public ModelPM getHostPM() {
		return currentHost;
	}


	@Override
	public Type getType() {
		return Type.SUSPENSION;
	}

	@Override
	public String toString() {
		return "Action: " + getType() + "  :VM " + toSuspend.getVM().hashCode();
	}

	@Override
	public void stateChanged(VirtualMachine vm, State oldState, State newState) {
		if(newState.equals(VirtualMachine.State.SUSPENDED)){
//			Logger.getGlobal().info("Suspension of VM " + vm.hashCode() + " successful.");
			vm.unsubscribeStateChange(this);			
			finished();
		}
	}

}
