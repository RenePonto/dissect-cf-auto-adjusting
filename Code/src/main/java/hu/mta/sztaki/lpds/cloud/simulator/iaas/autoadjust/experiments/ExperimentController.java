package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.experiments;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AutoAdjustingConsolidator.AnalyzeParameter;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AutoAdjustingConsolidator.FrequencyParameter;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AutoAdjustingConsolidator.PlanningParameter;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.experiments.PlanetLabTestCase;;

/**
 * This class has to be used to test the AutoAdjustingConsolidator. It allows several tests
 * with different combinations of the A/P - algorithms and the frequency - values.
 * 
 * The results are saved inside autoAdjustResults.csv-file.
 * 
 * @author Rene Ponto
 */
public class ExperimentController {
	
	/** the name of the output file to write the results in */
	private String outputFile = "autoAdjustResults.csv";
	
	/** the determined default values in general */
	public static AnalyzeParameter defaultAnalyze = AnalyzeParameter.A1;
	public static PlanningParameter defaultPlanning = PlanningParameter.P1;	
	public static FrequencyParameter defaultFrequency = FrequencyParameter.F2;
	
	public static long defaultF1 = 300000;
	public static long defaultF2 = 150000;
	
	/** the properties for the AAC */
	private Properties props;
	
	
	/**
	 * Default constructor.
	 */
	public ExperimentController() {
		
	}
	
	/**
	 * The main method to execute the wanted experiments.
	 * 
	 * @param args[0]: Enables normal logging.
	 * @param args[1]: Enables extended logging.
	 * 
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		
		// debug
		Map<String, Double> totalLoads = new LinkedHashMap<>();
		
		boolean doLogging = false;
		boolean doExtendedLogging = false;
		
		if(args.length == 2) {
			doLogging = Boolean.parseBoolean(args[0]);
			doExtendedLogging = Boolean.parseBoolean(args[1]);
		}
		else {
			System.err.println("Wrong amount of arguments.");
			System.exit(-1);
		}
		
		// create the controller and initialize the defaults
		ExperimentController controller = new ExperimentController();
		controller.loadProps();
		controller.restoreDefaults();
		
		// create the experiments
		PlanetLabTestCase[] tests = new PlanetLabTestCase[] {
				new PlanetLabTestCase(doLogging, doExtendedLogging),		// autoadjust
				new PlanetLabTestCase(AnalyzeParameter.A1, PlanningParameter.P1, FrequencyParameter.F1, 
						doLogging, doExtendedLogging),
				new PlanetLabTestCase(AnalyzeParameter.A2, PlanningParameter.P2, FrequencyParameter.F1, 
						doLogging, doExtendedLogging),
				new PlanetLabTestCase(AnalyzeParameter.A3, PlanningParameter.P3, FrequencyParameter.F1, 
						doLogging, doExtendedLogging),
				new PlanetLabTestCase(AnalyzeParameter.A4, PlanningParameter.P4, FrequencyParameter.F1, 
						doLogging, doExtendedLogging),
				new PlanetLabTestCase(AnalyzeParameter.A1, PlanningParameter.P1, FrequencyParameter.F2, 
						doLogging, doExtendedLogging),
				new PlanetLabTestCase(AnalyzeParameter.A2, PlanningParameter.P2, FrequencyParameter.F2, 
						doLogging, doExtendedLogging),
				new PlanetLabTestCase(AnalyzeParameter.A3, PlanningParameter.P3, FrequencyParameter.F2, 
						doLogging, doExtendedLogging),
				new PlanetLabTestCase(AnalyzeParameter.A4, PlanningParameter.P4, FrequencyParameter.F2, 
						doLogging, doExtendedLogging)
		};
		
		// run the experiments and print the results
		boolean first = true;
		for(int i = 0; i < tests.length; i++) {
			PlanetLabTestCase currentTest = tests[i];
			// save the current parameters as defaults
			controller.loadProps();
			controller.saveProps(currentTest.getAnalyzeParameter(), currentTest.getPlanningParameter(), 
					currentTest.getFrequencyParameter());
			ExperimentResults result = currentTest.executeExperiment();
			controller.writeOutput(first, result);	
			first = false;
			
			// debug: save the total load for comparison
			if(currentTest.isAutoAdjustUsed()) {
				totalLoads.put("autoadjust", result.getTotalLoad());
			}
			else {
				totalLoads.put(currentTest.getAnalyzeParameter() + " " + currentTest.getPlanningParameter() + " " + 
						currentTest.getFrequencyParameter(), result.getTotalLoad());
			}
			
		}
		
		// restore the defaults afterwards
		controller.restoreDefaults();		
		
		// debug: print the loads
		String output = "";
		for(String current: totalLoads.keySet()) {
			output += current + " " + totalLoads.get(current) + " | ";
		}
		
		System.out.println(output.substring(0, output.length() - 2));
		
		System.out.println("Finished all experiments successfully.");
	}
	
	/**
	 * Writes the parameterized results into the output file.
	 * 
	 * @param first Determines whether this is the first entry. Then the headline is
	 * 			written, too.
	 * @param result The ExperimentResults of an experiment.
	 * 
	 * @throws IOException
	 */
	public void writeOutput(boolean first, ExperimentResults result) throws IOException {
		File output = new File(outputFile);	
		BufferedWriter writer; 
		if(first) {
			writer = new BufferedWriter(new FileWriter(output));
			
			writer.write(result.getHeadline());
			writer.newLine();
		}
		else
			writer = new BufferedWriter(new FileWriter(output, true));
		
		writer.write(result.getCSVline());
		
		writer.newLine();		
		writer.close();
	}
	
	/**
	 * Loads the properties file with specific default values.
	 * 
	 * @throws InvalidPropertiesFormatException
	 * @throws IOException
	 */
	private void loadProps() throws InvalidPropertiesFormatException, IOException {
		props = new Properties();
		File file = new File("autoAdjustDefaults.xml");
		FileInputStream fileInput = new FileInputStream(file);
		props.loadFromXML(fileInput);
		fileInput.close();
	}
	
	/**
	 * Saves the properties after setting new default values.
	 * 
	 * @param a The AnalyzeParameter.
	 * @param p The PlanningParameter.
	 * @param f The Frequency.
	 * 
	 * @throws IOException
	 */
	private void saveProps(String a, String p, String f) throws IOException {
		
		props.put("defaultA", a);
		props.put("defaultP", p);
		props.put("defaultFrequency", f);			
		
		// replace the frequencies to ensure the correct F1/F2 values
		props.put("F1", ExperimentController.defaultF1 + "");
		props.put("F2", ExperimentController.defaultF2 + "");
		
		File file = new File("autoAdjustDefaults.xml");
		FileOutputStream fileOutput = new FileOutputStream(file);
		props.storeToXML(fileOutput, null);
		fileOutput.close();
	}
	
	/**
	 * Has to be called at the end of all experiments. Restores the default values
	 * for autoadjust.
	 * 
	 * @throws IOException
	 */
	private void restoreDefaults() throws IOException {
		props.put("defaultA", defaultAnalyze.toString());
		props.put("defaultP", defaultPlanning.toString());
		props.put("defaultFrequency", defaultFrequency.toString());		
		
		// replace the frequencies to ensure the correct F1/F2 values
		props.put("F1", ExperimentController.defaultF1 + "");
		props.put("F2", ExperimentController.defaultF2 + "");
		
		File file = new File("autoAdjustDefaults.xml");
		FileOutputStream fileOutput = new FileOutputStream(file);
		props.storeToXML(fileOutput, null);
		fileOutput.close();
	}
	
}
