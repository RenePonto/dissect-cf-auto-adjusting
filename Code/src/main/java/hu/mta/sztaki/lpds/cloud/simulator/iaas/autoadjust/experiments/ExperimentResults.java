package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.experiments;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AutoAdjustingConsolidator.AnalyzeParameter;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AutoAdjustingConsolidator.FrequencyParameter;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AutoAdjustingConsolidator.PlanningParameter;

/**
 * Helper class for the ExperimentController and the PlanetLabExperiment, stores all
 * wanted metrics and values to allow one to produce an .csv for example with them.
 */
public class ExperimentResults {
	
	// autoadjust,AnalyzeParameter,PlanningParameter,Frequency,total energy consumption,
	// average amount of active PMs,Total amount of migrations,total amount of VM suspensions,
	// total amount of VM resumes,total overload (in ResourceConstraints),total duration in ms,nrIterations,
	// total time of suspended VMs
	/** the metrics and values */
	private boolean autoadjust;
	private AnalyzeParameter ap;
	private PlanningParameter pp;
	private FrequencyParameter fp;
	private double totalEnergyConsumption;
	private long averageAmountActivePMs;
	private long totalAmountMigrations;
	private long totalAmountVMSuspensions;
	private long totalAmountVMResumes;
	private long totalAmountPMStarts;
	private long totalAmountPMStops;
	private double totalOverloadPercentage;	// overload is now in percentage
	private double totalOverload;
	private double totalLoad;
	private long totalDuration;
	private long nrIterations;
	private long totalSuspendedTime;
	
	/**
	 * Basic constructor, should not be used to avoid errors.
	 */
	protected ExperimentResults() {
		
	}
	
	/**
	 * Constructor to use when all results are known at creation.
	 * 
	 * @param autoadjust Determines whether autoadjust is used or not.
	 * @param ap The AnalyzeParameter for the current run.
	 * @param pp The PlanningParameter for the current run.
	 * @param fp The FrequencyParameter for the current run.
	 * @param totalEnergyConsumption The total energy consumption.
	 * @param averageAmountActivePMs The average amount of active PMs.
	 * @param totalAmountMigrations The total amount of VM migrations.
	 * @param totalAmountVMSuspensions The total amount of VM suspensions.
	 * @param totalAmountVMResumes The total amount of VM resumes.
	 * @param totalOverloadPercentage The total overload in a double.
	 * @param totalDuration The total duration of the simulation.
	 * @param nrIterations The total number of iterations.
	 * @param totalSuspendedTime The total time of VMs being suspended.
	 */
	public ExperimentResults(boolean autoadjust, AnalyzeParameter ap, PlanningParameter pp, 
			FrequencyParameter fp, double totalEnergyConsumption, long averageAmountActivePMs, long totalAmountMigrations,
			long totalAmountVMSuspensions, long totalAmountVMResumes, long totalAmountPMStarts, long totalAmountPMStops,
			double totalOverloadPercentage, double totalOverload, double totalLoad, long totalDuration, long nrIterations, long totalSuspendedTime) {
		this.autoadjust = autoadjust;
		this.ap = ap;
		this.pp = pp;
		this.fp = fp;
		this.totalEnergyConsumption = totalEnergyConsumption;
		this.averageAmountActivePMs = averageAmountActivePMs;
		this.totalAmountMigrations = totalAmountMigrations;
		this.totalAmountVMSuspensions = totalAmountVMSuspensions;
		this.totalAmountVMResumes = totalAmountVMResumes;
		this.totalAmountPMStarts = totalAmountPMStarts;
		this.totalAmountPMStops = totalAmountPMStops;
		this.totalOverloadPercentage = totalOverloadPercentage;
		this.totalOverload = totalOverload;
		this.totalLoad = totalLoad;
		this.totalDuration = totalDuration;
		this.nrIterations = nrIterations;
		this.totalSuspendedTime = totalSuspendedTime;
	}
	
	/**
	 * Normally this constructor is used to initialize the ExperimentResults and
	 * to set constraints afterwards.
	 * 
	 * @param totalEnergyConsumptionThe total energy consumption.
	 * @param averageAmountActivePMs The average amount of active PMs.
	 * @param totalAmountMigrations The total amount of VM migrations.
	 * @param totalAmountVMSuspensions The total amount of VM suspensions.
	 * @param totalAmountVMResumes The total amount of VM resumes.
	 * @param totalOverloadPercentage The total overload in a Double.
	 * @param totalDuration The total duration of the simulation.
	 * @param nrIterations The total number of iterations.
	 * @param totalSuspendedTime The total time of VMs being suspended.
	 */
	public ExperimentResults(double totalEnergyConsumption, long averageAmountActivePMs, long migrationCounter,
			long suspensionCounter, long resumeCounter, long totalAmountPMStarts, long totalAmountPMStops, 
			double totalOverloadPercentage, double totalOverload, double totalLoad, long duration, long iterationCount, long totalSuspendedTime) {
		this.totalEnergyConsumption = totalEnergyConsumption;
		this.averageAmountActivePMs = averageAmountActivePMs;
		this.totalAmountMigrations = migrationCounter;
		this.totalAmountVMSuspensions = suspensionCounter;
		this.totalAmountVMResumes = resumeCounter;
		this.totalAmountPMStarts = totalAmountPMStarts;
		this.totalAmountPMStops = totalAmountPMStops;
		this.totalOverloadPercentage = totalOverloadPercentage;
		this.totalOverload = totalOverload;
		this.totalLoad = totalLoad;
		this.totalDuration = duration;
		this.nrIterations = iterationCount;
		this.totalSuspendedTime = totalSuspendedTime;
	}
	
	/**
	 * Sets the constraints for the experiment regarding the current results.
	 * 
	 * @param autoadjust Determines whether autoadjust is used or not.
	 * @param ap The AnalyzeParameter of the regarding run.
	 * @param pp The PlanningParameter of the regarding run.
	 * @param fp The FrequencyParameter of the regarding run.
	 */
	public void setSimulationConstraints(boolean autoadjust, AnalyzeParameter ap, PlanningParameter pp, 
			FrequencyParameter fp) {
		this.autoadjust = autoadjust;
		this.ap = ap;
		this.pp = pp;
		this.fp = fp;		
	}
	
	/**
	 * The order for this is the following:<br>
	 * 1.autoadjust 2.AnalyzeParameter 3.PlanningParameter 4.Frequency 5.total energy consumption 
	 * 6.average amount of active PMs 7.Total amount of migrations 8.total amount of VM suspensions
	 * 9.total amount of VM resumes 10.total overload 11.total duration in ms 12.iterations
	 * 13.total time of suspended VMs
	 * 
	 * @return The results in a single String.
	 */
	public String getCSVline() {
		return "" + autoadjust + ';' + ap + ';' + pp + ';' + fp + ';' + totalEnergyConsumption + ';' + 
				averageAmountActivePMs + ';' + totalAmountMigrations + ';' + totalAmountVMSuspensions + ';' +
				totalAmountVMResumes + ';' + totalAmountPMStarts + ";" + totalAmountPMStops + ";" +
				totalOverloadPercentage + ';' + totalOverload + ';' + totalLoad + ';' + totalDuration + ';' + 
				nrIterations + ';' + totalSuspendedTime;
	}
	
	public String getHeadline() {
		return "autoadjust;AnalyzeParameter;PlanningParameter;Frequency;total energy consumption in kWh;"
				+ "average amount of active PMs;Total amount of migrations;total amount of VM suspensions;"
				+ "total amount of VM resumes;total amount of PM starts;total amount of PM stops;"
				+ "total overload (tpp) [%];total overload;total load;total duration in ms;nrIterations;"
				+ "total time of suspended VMs in ms";
	}
	
	public double getTotalLoad() {
		return totalLoad;
	}
	
	/**
	 * Returns the whole measured metrics with explanation in a more readable version.
	 */
	public String toString() {
		String result = "";
		
		// declare the type and then add the value
		result += "auto adjust used: " + autoadjust + '\n';
		result += "AnalyzeParameter: " + ap + '\n';
		result += "PlanningParameter: " + pp + '\n';
		result += "FrequencyParameter: " + fp + '\n';
		result += "total energy consumption: " + totalEnergyConsumption + '\n';
		result += "average amount of active PMs: " + averageAmountActivePMs + '\n';
		result += "total amount of migrations: " + totalAmountMigrations + '\n';
		result += "total amount of VM suspensions: " + totalAmountVMSuspensions + '\n';
		result += "total amount of VM resumes: " + totalAmountVMResumes + '\n';
		result += "total amount of PM starts: " + totalAmountPMStarts + '\n';
		result += "total amount of PM stops: " + totalAmountPMStops + '\n';
		result += "overload (tpp) [%]: " + totalOverloadPercentage + '\n';
		result += "total overload (tpp): " + totalOverload + '\n';
		result += "total load (tpp): " + totalLoad + '\n';
		result += "total duration in ms: " + totalDuration + '\n';
		result += "iterations: " + nrIterations + '\n';
		result += "total time of suspended VMs in ms: " + totalSuspendedTime + '\n';
		
		return result;
	}

}
