package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information;

import java.util.logging.Logger;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelPM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelVM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AutoAdjustingConsolidator.PlanningParameter;

/**
 * This class represents a plan for doing migrations, suspension etc. Depending
 * on the used algorithm we have different actions. To adapt the changes to the IaaSService,
 * the previous state and the current one are saved to allow execution of the necessary actions.
 * 
 * NOTE: A plan is always newly created at the start of each planning phase.
 */
public class Plan {
	
	/** is A3 size input used */
	private boolean isA3SizeInputUsed = false;
	
	/** initial situation */
	private ModelPM[] oldBins;
	private ModelVM[] oldItems;
	
	/** optimized situation */
	private ModelPM[] improvedBins;
	private ModelVM[] improvedItems;
	
	/** parameter */
	private PlanningParameter used;
	
	/**
	 * Default constructor, initializes the lists
	 */
	public Plan(ModelPM[] old, ModelVM[] vms, PlanningParameter used) {
		oldBins = old;
		oldItems = vms;
		improvedBins = new ModelPM[old.length];
		improvedItems = new ModelVM[vms.length];
		
		this.used = used;
	}
	
	/**
	 * Indicates that the determined sizes of A3 are used.
	 */
	public void setA3InputUsed() {
		isA3SizeInputUsed = true;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isA3SizeInputUsed() {
		return isA3SizeInputUsed;
	}
	
	public PlanningParameter getUsedParameter() {
		return used;
	}
	
	/**
	 * toString() returns both arrays containing the unoptimized and the
	 * optimized situation.
	 */
	public String toString() {			
		String result = "improvedBins to String: ";
		for (ModelPM bin : improvedBins) {								
			result = result + bin.toString();
			result = result + "\n";	
		}
		// now append the suspended VMs
		result = result + "\n" + ", currently suspended VMs: ";
		for(ModelVM vm: improvedItems) {
			if(vm.isSuspended()) {
				result = result + "VM " + vm.toString();
			}
		}
		
		return result;
	}
	
	private ModelPM findBinInArray(ModelPM[] array, int binID) {
		for(int i = 0; i < array.length; i++) {
			if(array[i].hashCode() == binID) {
				return array[i];
			}
		}
		
		return null;
	}
	
	public void getChangesBetweenBins() {		
		
		for(int i = 0; i < oldBins.length; i++) {
			boolean changes = false;
			String result = "";
			
			ModelPM old = oldBins[i];
			ModelPM current = findBinInArray(improvedBins, old.hashCode());
			if(old.hashCode() != current.hashCode() || current == null) {
				Logger.getGlobal().info("Error with the PMs to compare");
			}
			
			// compare the load of the PMs
			if (old.getConsumedResources() != current.getConsumedResources()) {
				changes = true;

				result += "PM [" + current.hashCode() + "] previous: " + old.getConsumedResources()
						+ " current: " + current.getConsumedResources();
				
				// compare the resources for each VM
				for (ModelVM oldVM : old.getVMs()) {
					if (current.getVMs().contains(oldVM)) {
						for (ModelVM currentVM : current.getVMs()) {
							if (oldVM.hashCode() == currentVM.hashCode()) {
								// comparison
								if (oldVM.getResources().equals(currentVM.getResources())) {
									// everything is the same here
								} else {
									result += "\n resources of VM " + oldVM.hashCode() + "changed from "
											+ oldVM.getResources() + " to " + currentVM.getResources();
								}
							}
						}
					} else {
						result += "\nVM " + oldVM.hashCode() + " of PM [" + old.hashCode() + "] is not hosted anymore... ";
					}
				}
			}

			// compare the amount of VMs
			if (old.getVMs().size() != current.getVMs().size()) {
				
				// print the VMs
				result += "\nPM " + current.hashCode() + ", previous hosted VMs: ";
				for (ModelVM vm : old.getVMs()) {
					result += "VM " + vm.hashCode();
				}
			}
			if(changes)
				Logger.getGlobal().info(result);
		}
		
	}
	
	/**
	 * Get the ModelPMs before optimization.
	 * 
	 * @return The ModelPMs before optimization.
	 */
	public ModelPM[] getOldPMs() {
		return oldBins;
	}
	
	/**
	 * Get the ModelVMs before optimization.
	 * 
	 * @return The ModelVMs before optimization.
	 */
	public ModelVM[] getOldVMs() {
		return oldItems;
	}
	
	/**
	 * Get the ModelPMs after optimization.
	 * 
	 * @return The ModelPMs after optimization.
	 */
	public ModelPM[] getImprovedPMs() {
		return improvedBins;
	}
	
	/**
	 * Get the ModelVMs after optimization.
	 * 
	 * @return The ModelVMs after optimization.
	 */
	public ModelVM[] getImprovedItems() {
		return improvedItems;
	}
	
	/**
	 * Set the ModelPMs after optimization.
	 * 
	 * @param old The ModelPMs after optimization.
	 */
	public void setCurrent(ModelPM[] pms, ModelVM[] vms) {
		improvedBins = pms;
		improvedItems = vms;
	}
	
	/**
	 * Helper method to get the currently running PMs after planning
	 * 
	 * @return
	 */
	public int getCurrentlyActivePMs() {
		int result = 0;
		
		for(ModelPM bin: improvedBins) {
			if(bin.isRunning())
				result++;
		}
		
		return result;
	}
	
}

