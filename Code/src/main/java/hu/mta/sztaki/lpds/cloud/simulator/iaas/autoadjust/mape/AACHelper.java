package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.math3.stat.regression.SimpleRegression;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.AlterableResourceConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;

/**
 * This class is used together with the AutoAdjustingConsolidator and
 * will not work without it.
 * 
 * The important statistics are managed with the help of this class to
 * ensure consistency and clarity. To allow the consolidator to only focus
 * on the consolidation loop, parameters are offloaded here.
 */
public class AACHelper {
	
	/** statics for monitoring */
	public static long iterationCount = 0, c0amount = 0, c1amount = 0, c2amount = 0, c3amount = 0, 
		migrationCounter = 0, suspensionCounter = 0, resumeCounter = 0, averageActivePMs = 0, 
		totalTimeSuspended = 0, pmStartCounter, pmStopCounter = 0;
	
	/** the total amount of overload, gets adjusted after each consolidation loop */
//	public static AlterableResourceConstraints totalOverload = AlterableResourceConstraints.getNoResources();
	public static AlterableResourceConstraints totalOverload = getNoResources();
	
	/** the total load in general, gets adjusted after each consolidation loop */
//	public static AlterableResourceConstraints totalLoad = AlterableResourceConstraints.getNoResources();
	public static AlterableResourceConstraints totalLoad = getNoResources();
	
	/** statics for the current iteration */
	public static long migrations = 0, suspensions = 0, resumes = 0, startPM = 0, stopPM = 0, 
		failedMigrations = 0, failedSuspensions = 0, failedResumes = 0, failedStartPM = 0, failedStopPM = 0;
	
	/** the amount of overload in one iteration */
//	public static AlterableResourceConstraints currentOverload = AlterableResourceConstraints.getNoResources();
	public static AlterableResourceConstraints currentOverload = getNoResources();
	
	/** the amount of previous values to be stored */
	private int amountOfPrevious;	
	/** the threshold for possibly overloaded PMs */
	private double thresholdA1;
	/** the threshold for overloaded PMs */
	private double thresholdA2;
	/** the threshold to determine significant changes */
	private double thresholdA4;
	/** the threshold for determining load changes */
	private double thresholdLoadBehaviour;
	/** the threshold to determine normal load of a PM */
	private double thresholdP1;
	/** the threshold to determine normal load of a PM */
	private double thresholdP2;
	/** the threshold to determine normal load of a PM */
	private double thresholdP4;
	/** counter for P3 */
	private int gurobiTimeLimit;
	/** threshold for placing VMs in P3 */
	private double correlationThreshold;	
	/** lower threshold for ModelPMs */
	private double lowerThreshold;
	/** upper threshold for ModelPMs */
	private double upperThreshold;
	/** upper critical threshold for ModelPMs (only higher load than this is seen as overload) */
	private double upperCriticalThreshold;
	/** the threshold to use in gurobi for a PM */
	private double thresholdGurobiPMs;

	/**
	 * Default constructor, sets all necessary thresholds.
	 * 
	 * @param amountOfPrevious The amount of values saved of previous iterations for PMInformation and VMInformation.
	 * @param thresholdA4 The threshold for A1.
	 * @param thresholdA4 The threshold for A2.
	 * @param thresholdA1 The threshold for A4.
	 * @param thresholdLoadBehaviour Used to determine the ContextSituationClass. 
	 * @param thresholdP1 The threshold for P1.
	 * @param thresholdP2 The threshold for P2.
	 * @param thresholdP4 The threshold for P4.
	 * @param gurobiTimeLimit The time limit for gurobi to find a solution.
	 * @param correlationThreshold The threshold used in A3/P3 to determine a high correlation between VMs.
	 * @param predictionValues The amount of previous values used to predict the next one.
	 * @param lowerThreshold The percentage of usage of a PM's resources to consider it to be on low load.
	 * @param upperThreshold The percentage of usage of a PM's resources to consider it to be on high load.
	 * @param upperCriticalThreshold The percentage of usage of a PM's resources to consider it to be on critical high load.
	 * @param thresholdGurobiPMs The threshold to use in gurobi for a PM.
	 */
	public AACHelper(int amountOfPrevious, double thresholdA1, double thresholdA2, double thresholdA4,
			double thresholdLoadBehaviour, double thresholdP1, double thresholdP2, double thresholdP4,
			int gurobiTimeLimit, double correlationThreshold, double lowerThreshold, double upperThreshold, 
			double upperCriticalThreshold, double thresholdGurobiPMs) {
		
		this.amountOfPrevious = amountOfPrevious;
		this.thresholdA1 = thresholdA1;
		this.thresholdA2 = thresholdA2;
		this.thresholdA4 = thresholdA4;
		this.thresholdLoadBehaviour = thresholdLoadBehaviour;
		this.thresholdP1 = thresholdP1;
		this.thresholdP2 = thresholdP2;
		this.thresholdP4 = thresholdP4;
		this.gurobiTimeLimit = gurobiTimeLimit;
		this.correlationThreshold = correlationThreshold;
		this.lowerThreshold = lowerThreshold;
		this.upperThreshold = upperThreshold;
		this.upperCriticalThreshold = upperCriticalThreshold;
		this.thresholdGurobiPMs = thresholdGurobiPMs;
		
		// resets all counters, used for multiple experiments in a row
		resetCounter();
	}	
	
	/**
	 * Helper function to determine the next load out of two loads.
	 * 
	 * @param current
	 * @param previous
	 * 
	 * @return
	 */
	public static ResourceConstraints calculateFutureLoad(ResourceConstraints current, ResourceConstraints previous, double maxCpu) {
		double cpu = 0, procPower = 0;
		long mem = 0;
		
//		if(current.getRequiredProcessingPower() != previous.getRequiredProcessingPower()) {
			procPower = current.getRequiredProcessingPower();
//		}		
		
		// cpu changes
		if(current.getRequiredCPUs() > previous.getRequiredCPUs()) {
			double diff = current.getRequiredCPUs() - previous.getRequiredCPUs();
			cpu = current.getRequiredCPUs() + diff;
		}
		else if(current.getRequiredCPUs() == previous.getRequiredCPUs()) {
			cpu = current.getRequiredCPUs();
		}
		else {
			double diff = previous.getRequiredCPUs() - current.getRequiredCPUs();
			cpu = current.getRequiredCPUs() - diff;
		}
		
		/** to prevent possible calculation errors, we round the value to 2 places */
		cpu = roundDouble(cpu, 2);
		
		
		// error cases
		if(cpu <= 0.0) {
			cpu = 0.1;
		}
		else if(cpu > maxCpu) {
			cpu = maxCpu;
		}
		
		// memory changes
		if(current.getRequiredMemory() > previous.getRequiredMemory()) {
			long diff = current.getRequiredMemory() - previous.getRequiredMemory();
			mem = current.getRequiredMemory() + diff;
		}
		else if(current.getRequiredMemory() == previous.getRequiredMemory()) {
			mem = current.getRequiredMemory();
		}
		else {
			long diff = previous.getRequiredMemory() - current.getRequiredMemory();
			mem = current.getRequiredMemory() - diff;
		}
		
		ResourceConstraints calculated = new AlterableResourceConstraints(cpu, procPower, mem);
		return calculated;
	}
	
	/**
	 * Helper function to determine the next load out of multiple loads.
	 * 
	 * @param current
	 * @param previous
	 * 
	 * @return
	 */
	public static ResourceConstraints calculateFutureLoad(List<ResourceConstraints> all, double maxCpu, int amountToConsider) {
		double cpu = 0, procPower = 0;
		long mem = 0;
		
		// error case: list is empty
		if(all.size() == 0) {
			return null;
		}
		// error case: list has less then two values
		if(all.size() < 2) {
			return all.get(0);
		}
		
		// if there are more then 6 values, only take the first 6
		List<ResourceConstraints> toUse;
		if(all.size() > 6) {
			toUse = all.subList(0, 5);
		}
		else {
			toUse = all;
		}
		
		/** NOTE: the processing power does not change currently */
		procPower = all.get(0).getRequiredProcessingPower();
		
		// cpu changes
		double diff = 0.0;
		for(int i = 0; i < toUse.size(); i++) {
			diff += toUse.get(i).getRequiredCPUs();
		}
		cpu = diff / toUse.size();
		
		/** to prevent possible calculation errors, we round the value to 2 places */
		cpu = roundDouble(cpu, 2);
		
		
		// error cases
		if(cpu <= 0.0) {
			cpu = 0.1;
		}
		else if(cpu > maxCpu) {
			cpu = maxCpu;
		}
		
		// pessimistic calculation: we only assume the load to get higher, otherwise we take the previous value
//		if(cpu > toUse.get(0).getRequiredCPUs())
//			cpu = toUse.get(0).getRequiredCPUs();
		
		// memory changes
		mem = toUse.get(0).getRequiredMemory();
//		if(current.getRequiredMemory() > previous.getRequiredMemory()) {
//			long diff = current.getRequiredMemory() - previous.getRequiredMemory();
//			mem = current.getRequiredMemory() + diff;
//		}
//		else if(current.getRequiredMemory() == previous.getRequiredMemory()) {
//			mem = current.getRequiredMemory();
//		}
//		else {
//			long diff = previous.getRequiredMemory() - current.getRequiredMemory();
//			mem = current.getRequiredMemory() - diff;
//		}
		
		ResourceConstraints calculated = new AlterableResourceConstraints(cpu, procPower, mem);
		return calculated;
	}
	
	/**
	 * Calculates the linear regression with one variable. Note that the current element has to be at the first
	 * place in the list and the oldest value at the last.
	 * 
	 * @param previousConstraints The data set to use for calculations.
	 * @return
	 */
	public static ResourceConstraints calculateSimpleLinearRegression(List<ResourceConstraints> resourceConstraints, boolean forPM) {
		
		// error case: only one value
		if(resourceConstraints.size() < 2) {
			// if there are less then two values, return only the current value
			return resourceConstraints.get(0);
		}
		
		/** use common maths */
		// the iterations and cpu lists
		List<Double> iterations = new ArrayList<>();
		List<Double> cpuLoads = new ArrayList<>();
		
		int length = resourceConstraints.size();
		
		for(int i = 0; i < length; i++) {
			iterations.add((double) (i + 1));
//			cpuLoads.add(resourceConstraints.get(i).getRequiredCPUs());
			cpuLoads.add(resourceConstraints.get(length - 1 - i).getRequiredCPUs());
		}
//		for(int i = length; i > 0; i--) {
//			iterations.add((double) (i));
//			cpuLoads.add(resourceConstraints.get((i - 1)).getRequiredCPUs());
//		}
		
		// error case: all values are the same in the cpuLoads
		boolean allTheSame = false;
		double duplicateValue = cpuLoads.get(0);
		for(Double curr: cpuLoads) {
						
			if(curr == duplicateValue)
				allTheSame = true;
			else {
				allTheSame = false;
				break;
			}
		}
		
		// only calculate the regression if there are different values
		double estimatedCPU;
		if(allTheSame) {
			estimatedCPU = duplicateValue;
		}
		else {
			SimpleRegression reg = new SimpleRegression();
			
			// add the data
			for(int i = 0; i < iterations.size(); i++) {
				reg.addData(iterations.get(i), cpuLoads.get(i));
			}
			
			// get the prediction
			estimatedCPU = reg.predict(iterations.size() + 1);
		}
     	
     	// debug: check the correctness
     	String prev = "Previous values: ";
     	for(int i = length; i > 0; i--) {
     		prev += resourceConstraints.get(i - 1).getRequiredCPUs() + " ";
     	}
//     	for(ResourceConstraints curr: resourceConstraints) {
//     		prev += curr.getRequiredCPUs() + " ";
//     	}
     	prev += "estimated: " + estimatedCPU;
     	Logger.getGlobal().info(prev);
     	
     	// error cases: load cannot be higher than 1.0 and lower than 0.01 for VMs, for PMs it is 2.0 and 0.00
     	double previousEstimation = estimatedCPU;
     	if(forPM) {
     		// PM
     		if(estimatedCPU > 2.0) {
            	estimatedCPU = 2.0;
            }
            else if(estimatedCPU < 0.00) {
            	estimatedCPU = 0.00;
            }
     	}
     	else {
     		// VM
     		if(estimatedCPU > 1.0) {
            	estimatedCPU = 1.0;
            }
            else if(estimatedCPU < 0.01) {
            	estimatedCPU = 0.01;
            }
     	}     	
        
        
        // log if there is a change
        if(previousEstimation != estimatedCPU) {
        	Logger.getGlobal().info("Error case of too high / too low load, new load: " + estimatedCPU);
        }
		
        // the estimated result constraints
     	ResourceConstraints calculated = new AlterableResourceConstraints(estimatedCPU, resourceConstraints.get(0).getRequiredProcessingPower(), 
     			resourceConstraints.get(0).getRequiredMemory());
		
		return calculated;
	}
	
	/**
	 * Rounds up a double value to the specified amount of places after the comma.
	 * 
	 * @param value
	 * @param places
	 * @return
	 */
	public static double roundDouble(double value, int places) {
	    if (places < 0) {
	    	return value;
	    }

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	/**
	 * Rounds up a double value to the specified amount of places after the comma.
	 * 
	 * @param value
	 * @param places
	 * @return
	 */
	public static int roundInteger(int value, int places) {
	    if (places < 0) {
	    	return value;
	    }

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.intValue();
	}
	
	/**
	 * Adds up the current overload to the total overload of the AutoAdjustConsolidator.
	 * 
	 * @param toAdd ResourceConstraints containing the current overload.
	 */
	public static void addUpOverload(ResourceConstraints toAdd) {
		totalOverload.add(toAdd);
	}
	
	/**
	 * Adds up the current load to the total load of the AutoAdjustConsolidator.
	 * 
	 * @param toAdd ResourceConstraints containing the complete load.
	 */
	public static void addUpTotalLoad(ResourceConstraints toAdd) {
		totalLoad.add(toAdd);
	}
	
	/**
	 * Adds up the overload of the current iteration of the AutoAdjustConsolidator.
	 * 
	 * @param toAdd ResourceConstraints containing the current overload.
	 */
	public static void addUpCurrentOverload(ResourceConstraints toAdd) {
		currentOverload.add(toAdd);
	}
	
	/**
	 * Resets the current overload to prepare the next iterations.
	 */
	public static void resetCurrentOverload() {
//		currentOverload = new AlterableResourceConstraints(AlterableResourceConstraints.getNoResources());
		currentOverload = getNoResources();
	}
	
	/**
	 * Resets the counter for each iteration for the migrations, suspensions and resumes.
	 */
	public static void resetCurrentCounter() {
		migrations = suspensions = resumes = startPM = stopPM = failedMigrations = failedSuspensions = 
				failedResumes = failedStartPM = failedStopPM = 0;
	}
	
	/**
	 * Adds up the suspended time of a VM.
	 * 
	 * @param time The suspended time of the VM.
	 */
	public static void addUpSuspendedTime(long time) {
		totalTimeSuspended += time;
	}
	
	
	/**
	 * Resets the counter, used for test-cases.
	 */
	public static void resetCounter() {
		iterationCount = c0amount = c1amount = c2amount = c3amount = migrationCounter = 
				suspensionCounter = resumeCounter = averageActivePMs = totalTimeSuspended = 
				pmStartCounter = pmStopCounter = 0;
	}
	
	/**
	 * Getter for the amount of the average active PMs per consolidation run.
	 * 
	 * @return The amount of the average active PMs per consolidation run, 0 if there
	 * 			are either no PMs running so far or it is during the first iteration.
	 */
	public static long getAverageActivePMs() {
		if(averageActivePMs == 0 || iterationCount == 0)
			return 0;
		return averageActivePMs/iterationCount;
	}
	
	/**
	 * @return The amountOfPrevious out of the properties.
	 */
	public int getAmountOfPrevious() {
		return amountOfPrevious;
	}

	/**
	 * @return the thresholdA1 out of the properties.
	 */
	public double getThresholdA1() {
		return thresholdA1;
	}

	/**
	 * @return the thresholdA2 out of the properties.
	 */
	public double getThresholdA2() {
		return thresholdA2;
	}
	
	/**
	 * @return the thresholdA4 out of the properties.
	 */
	public double getThresholdA4() {
		return thresholdA4;
	}

	/**
	 * @return the thresholdLoadBehaviour out of the properties.
	 */
	public double getThresholdLoadBehaviour() {
		return thresholdLoadBehaviour;
	}

	/**
	 * @return the thresholdP1 out of the properties.
	 */
	public double getThresholdP1() {
		return thresholdP1;
	}
	
	/**
	 * @return the thresholdP2 out of the properties.
	 */
	public double getThresholdP2() {
		return thresholdP2;
	}

	/**
	 * @return the thresholdP4 out of the properties.
	 */
	public double getThresholdP4() {
		return thresholdP4;
	}

	/**
	 * @return the gurobiTimeLimit out of the properties.
	 */
	public int getGurobiTimeLimit() {
		return gurobiTimeLimit;
	}

	/**
	 * @return the correlationThreshold out of the properties.
	 */
	public double getCorrelationThreshold() {
		return correlationThreshold;
	}
	
	/**
	 * @return the lowerThreshold out of the properties.
	 */
	public double getLowerThreshold() {
		return lowerThreshold;
	}
	
	/**
	 * @return the upperThreshold out of the properties.
	 */
	public double getUpperThreshold() {
		return upperThreshold;
	}
	
	/**
	 * @return the upperThreshold out of the properties, which is used to determine overload.
	 */
	public double getUpperCriticalThreshold() {
		return upperCriticalThreshold;
	}
	
	/**
	 * @return the thresholdGurobiPMs out of the properties, which is used inside the ILP consolidator.
	 */
	public double getThresholdGurobiPMs() {
		return thresholdGurobiPMs;
	}
	
	public static AlterableResourceConstraints getNoResources() {
		return new AlterableResourceConstraints(0, 2660, 0);
	}
	
}
