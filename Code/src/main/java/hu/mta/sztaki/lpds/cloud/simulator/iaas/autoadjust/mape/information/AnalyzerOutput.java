package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AutoAdjustingConsolidator.AnalyzeParameter;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;

/**
 * Object to store the output of the analysis. Note that not every list
 * will be filled after analyzing but only the ones the specific algorithms
 * care for.
 * 
 * Every consolidation-round the AnalyzerOutput gets newly created.
 */
public class AnalyzerOutput {
	
	/** current mapping of VMs to PMs */
	private SortedMap<PhysicalMachine, List<VirtualMachine>> mapping;	
	
	/** A1/A2: contains the currently overloaded PMs */
	private List<PhysicalMachine> overloadedPMs;
	
	/** A1: contains PMs that could be overloaded in the future */
	private Map<PhysicalMachine, ResourceConstraints> possibleOverloadedPMs;
	
	/** A4: contains the PMs with significant changes */
	private List<PhysicalMachine> significantIncreasingPMs;
	private List<PhysicalMachine> significantDecreasingPMs;
	
	/** A2: contains the suspended VMs */
	private List<VirtualMachine> suspendedVMs;
	
	/** A3: new sizes of the VMs */
	private Map<VirtualMachine, ResourceConstraints> sizes;
	
	/** A3: correlation between the VMs */
	private Map<VirtualMachine[], Double> correlations;		
	
	/** parameter */
	private AnalyzeParameter used;
	
	/** additional unplaced VMs for the planner */
	private Map<VirtualMachine, ResourceConstraints> toBePlaced;
	
	/**
	 * Default constructor, initializes the lists
	 */
	public AnalyzerOutput(SortedMap<PhysicalMachine, List<VirtualMachine>> mapping, AnalyzeParameter used) {
		this.mapping = mapping;
		
		overloadedPMs = new ArrayList<PhysicalMachine>();
		possibleOverloadedPMs = new HashMap<PhysicalMachine, ResourceConstraints>();
		significantIncreasingPMs = new ArrayList<PhysicalMachine>();
		significantDecreasingPMs = new ArrayList<PhysicalMachine>();
		suspendedVMs = new ArrayList<VirtualMachine>();
		sizes = new HashMap<VirtualMachine, ResourceConstraints>();
		correlations = new HashMap<VirtualMachine[], Double>();
		
		this.used = used;
		toBePlaced = new HashMap<>();
	}
	
	public void setToBePlaced(Map<VirtualMachine, ResourceConstraints> toBePlaced) {
		this.toBePlaced = toBePlaced;
	}
	
	public Map<VirtualMachine, ResourceConstraints> getToBePlaced() {
		return toBePlaced;
	}
	
	/**
	 * Currently printing the mapping.
	 */
	public String mappingToString() {
		String result = "";
		PhysicalMachine[] pms = new PhysicalMachine[mapping.keySet().size()];
		mapping.keySet().toArray(pms);
		
		for(int i = 0; i < pms.length; i++) {
			result += "PM " + pms[i].hashCode() + ", hosting VMs ";
			for(VirtualMachine hosted: mapping.get(pms[i])) {
				result += hosted.hashCode() + " ";
			}
			result += '\n';
		}
		
		return result;
	}
	
	/**
	 * Prints the content of each list.
	 */
	public String toString() {
		String result = "";
		
		// we have to print the overloadedPMs, possibleOverloadedPMs, significantIncreasingPMs, significantDecreasingPMs,
		// suspendedVMs, sizes, correlations and the used parameter
		result += "overloadedPMs: ";
		if(overloadedPMs.isEmpty())
			result += "is empty.\n";
		else {
			for(PhysicalMachine pm: overloadedPMs) {
				result += pm.hashCode() + ", ";
			}
			// remove the ", "
			result = result.substring(0, result.length() - 2);
			result += "\n";
		}
		
		result += "possibleOverloadedPMs: ";
		if(possibleOverloadedPMs.isEmpty())
			result += "is empty.\n";
		else {
			for(PhysicalMachine pm: possibleOverloadedPMs.keySet()) {
				result += pm.hashCode() + ", ";
			}
			// remove the ", "
			result = result.substring(0, result.length() - 2);
			result += "\n";
		}
		
		result += "significantIncreasingPMs: ";
		if(significantIncreasingPMs.isEmpty())
			result += "is empty.\n";
		else {
			for(PhysicalMachine pm: significantIncreasingPMs) {
				result += pm.hashCode() + ", ";
			}
			// remove the ", "
			result = result.substring(0, result.length() - 2);
			result += "\n";
		}
		
		result += "significantDecreasingPMs: ";
		if(significantDecreasingPMs.isEmpty())
			result += "is empty.\n";
		else {
			for(PhysicalMachine pm: significantDecreasingPMs) {
				result += pm.hashCode() + ", ";
			}
			// remove the ", "
			result = result.substring(0, result.length() - 2);
			result += "\n";
		}
		
		result += "suspendedVMs: ";
		if(suspendedVMs.isEmpty())
			result += "is empty.\n";
		else {
			for(VirtualMachine vm: suspendedVMs) {
				result += vm.hashCode() + ", ";
			}
			// remove the ", "
			result = result.substring(0, result.length() - 2);
			result += "\n";
		}
		
		result += "sizes: ";
		if(sizes.isEmpty())
			result += "is empty.\n";
		else {
			for(VirtualMachine vm: sizes.keySet()) {
				result += vm.hashCode() + " with " + sizes.get(vm).toString() + ", ";
			}
			// remove the ", "
			result = result.substring(0, result.length() - 2);
			result += "\n";
		}
		
		// we do not print the correlations (too much values to print)
//		result += "correlations: ";
//		if(correlations.isEmpty())
//			result += " is empty.\n";
//		else {
//			for(VirtualMachine[] vms: correlations.keySet()) {
//				result += vms[0].hashCode() + " and " + vms[1].hashCode() + " with " + correlations.get(vms) + ", ";
//			}
//			// remove the ", "
//			result = result.substring(0, result.length() - 2);
//			result += "\n";
//		}
		
		result += "used parameter: " + getUsedParameter();
		
		return result;
	}
	
	/**
	 * Getter for the current A-parameter.
	 * 
	 * @return The current A-parameter.
	 */
	public AnalyzeParameter getUsedParameter() {
		return used;
	}
	
	/**
	 * Get the current mapping of VMs to PMs.
	 * 
	 * @return The current mapping.
	 */
	public SortedMap<PhysicalMachine, List<VirtualMachine>> getMapping() {
		return mapping;
	}
	
	/**
	 * Get the currently overloaded PMs.
	 * 
	 * @return The currently overloaded PMs.
	 */
	public List<PhysicalMachine> getOverloaded() {
		return overloadedPMs;
	}
	
	/**
	 * Get the PMs with significant changes.
	 * 
	 * @return The PMs with significant changes.
	 */
	public List<PhysicalMachine> getSignificantIncreasingPMs() {
		return significantIncreasingPMs;
	}
	
	/**
	 * Get the PMs with significant changes.
	 * 
	 * @return The PMs with significant changes.
	 */
	public List<PhysicalMachine> getSignificantDecreasingPMs() {
		return significantDecreasingPMs;
	}
	
	/**
	 * Get the in the future possibly overloaded PMs.
	 * 
	 * @return The in the future possibly overloaded PMs.
	 */
	public Map<PhysicalMachine, ResourceConstraints> getPossibleOverloaded() {
		return possibleOverloadedPMs;
	}
	
	/**
	 * Get the currently suspended VMs.
	 * 
	 * @return The currently suspended VMs.
	 */
	public List<VirtualMachine> getSuspendedVMs() {
		return suspendedVMs;
	}
	
	/**
	 * Get the calculated new sizes for each VM.
	 * 
	 * @return The calculated new sizes for each VM.
	 */
	public Map<VirtualMachine, ResourceConstraints> getSizes() {
		return sizes;
	}
	
	/**
	 * Get the correlations between each VM.
	 * 
	 * @return The correlations between each VM.
	 */
	public Map<VirtualMachine[], Double> getCorrelations() {
		return correlations;
	}
	
	/**
	 * Used to set the currently overloaded PMs.
	 * 
	 * @param overloadedPMs A list containing the currently overloaded PMs.
	 */
	public void setOverloaded(List<PhysicalMachine> overloadedPMs) {
		this.overloadedPMs = overloadedPMs;
	}
	
	/**
	 * Used to set the PMs with significant changes.
	 * 
	 * @param significantPMs A list containing the PMs with significant changes.
	 */
	public void setSignificantIncreasingPMs(List<PhysicalMachine> significantPMs) {
		this.significantIncreasingPMs = significantPMs;
	}
	
	/**
	 * Used to set the PMs with significant changes.
	 * 
	 * @param significantPMs A list containing the PMs with significant changes.
	 */
	public void setSignificantDecreasingPMs(List<PhysicalMachine> significantPMs) {
		this.significantDecreasingPMs = significantPMs;
	}
	
	/**
	 * Used to set the in the future possibly overloaded PMs.
	 * 
	 * @param possibleOverloadedPMs A list containing the in the future possibly overloaded PMs.
	 */
	public void setPossibleOverloaded(Map<PhysicalMachine, ResourceConstraints> possibleOverloadedPMs) {
		this.possibleOverloadedPMs = possibleOverloadedPMs;
	}
	
	/**
	 * Used to set the currently suspended VMs.
	 * 
	 * @param suspendedVMs A list containing the currently suspended VMs.
	 */
	public void setSuspendedVMs(List<VirtualMachine> suspendedVMs) {
		this.suspendedVMs = suspendedVMs;
	}
	
	/**
	 * Sets the calculated ResourceConstraints for each VM.
	 * 
	 * @param sizes The calculated ResourceConstraints for each VM.
	 */
	public void setSizes(Map<VirtualMachine, ResourceConstraints> sizes) {
		this.sizes = sizes;
	}
	
	/**
	 * Sets the correlations of each VM-pair.
	 * 
	 * @param correlations The correlations of each VM-pair.
	 */
	public void setCorrelations(Map<VirtualMachine[], Double> correlations) {
		this.correlations = correlations;
	}
	
	
}

