package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.PMInformation;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.VMInformation;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AutoAdjustingConsolidator.AnalyzeParameter;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.AnalyzerOutput;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.MonitorInformation;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.AlterableResourceConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;

/**
 * This class is used together with the AutoAdjustingConsolidator. Its function
 * is to analyze the current situation in the cloud accordingly to parameter A.
 * In total there are four different scenarios possible:
 * 
 * A1: Identify servers that are either overloaded or likely to become 
 * overloaded soon
 * 
 * A2: Identify overloaded servers and suspended VMs
 * 
 * A3: Assess which subsets of VMs have load patterns that are compatible for 
 * long-term co-residence
 * 
 * A4: Identify significant changes since the last invocation
 */
public class Analyzer {
	
	/** the threshold for possibly overloaded PMs */
	private double thresholdA1;
	/** the threshold to determine overloaded PMs */
	private double thresholdA2;
	/** the threshold to determine significant changes */
	private double thresholdA4;
	/** determines whether the default or autoadjust is used */
	private boolean useDefaults;
	
	/** time measurement */
	private long duration;
	
	/**
	 * Default constructor. Has to be empty, because one instance is intended to
	 * be alive as long as the instance of the consolidator.
	 * 
	 * The inputs and possible parameters are set before each usage separately.
	 */
	public Analyzer(boolean useDefaults) {
		this.useDefaults = useDefaults;
	}
	
	/**
	 * Getter for the duration of the current analysis run. Is intended to be called 
	 * after the analysis is done.
	 * 
	 * @return The duration of the current A-algorithm in milliseconds.
	 */
	public long getDuration() {
		return duration;
	}
	
	/**
	 * Use the determined algorithm for analyzing.
	 * 
	 * @param p Determines the wanted algorithm for analyzing.
	 * @param monitoredInformation The MonitorInformation to analyze.
	 * @param thresholdA1 The threshold used in algorithm A1.
	 * @param thresholdA2 The threshold used in algorithm A2.
	 * @param thresholdA4 The threshold used in algorithm A4.
	 * @param upperThreshold The upperThreshold to consider a PM overloaded.
	 * 
	 * @return The AnalyzerOutput of the used algorithm.
	 */
	public AnalyzerOutput analyze(AnalyzeParameter p, MonitorInformation monitoredInformation, 
			double thresholdA1, double thresholdA2, double thresholdA4) {
		
		Logger.getGlobal().info("started to analyze with parameter " + p);
		
		long startTime = Calendar.getInstance().getTimeInMillis();		
		
		this.thresholdA1 = thresholdA1;
		this.thresholdA2 = thresholdA2;
		this.thresholdA4 = thresholdA4;
		
		// create a new output to save the results in
		AnalyzerOutput output = new AnalyzerOutput(monitoredInformation.getMapping(), p);
		if(monitoredInformation.areUnplacedVMs())
			output.setToBePlaced(monitoredInformation.getUnplacedVMs());
		
		switch(p) {
		case A1:
			useAlgorithmA1(monitoredInformation, output);
			break;
		case A2:
			useAlgorithmA2(monitoredInformation, output);
			break;
		case A3:
			useAlgorithmA3(monitoredInformation, output);
			break;
		case A4:
			useAlgorithmA4(monitoredInformation, output);
			break;
		default:
			Logger.getGlobal().info("This should never happen.");
			System.exit(-1);
			break;		
		}
		
		// determine the durations
		long endTime = Calendar.getInstance().getTimeInMillis();		
		duration = endTime - startTime;
		
		return output;
	}
	
	/**
	 * Identify servers that are either overloaded or likely to become 
	 * overloaded soon
	 * 
	 * At first we have to determine the already overloaded PMs. Then
	 * we have to check the previous values out of the MonitorInformation-
	 * object (if there is one) and compare the other machines with each other.
	 * 
	 * @param toAnalyze The MonitorInformation, containing the results to analyze.
	 * @param output The AnalyzerOutput to save the results in. 
	 */
	private void useAlgorithmA1(MonitorInformation toAnalyze, AnalyzerOutput output) {
		
		/** store the overloaded and possibly overloaded PMs */
		List<PhysicalMachine> overloaded = new ArrayList<PhysicalMachine>();
		Map<PhysicalMachine, ResourceConstraints> possibleOverloaded = new HashMap<PhysicalMachine, ResourceConstraints>();
		
		// identify overloaded PMs 
		if(toAnalyze.areOverloadedServers()) {
			for(int i = 0; i < toAnalyze.getPMInformation().size(); i++) {
				PMInformation currentInfo = toAnalyze.getPMInformation().get(i);
				PhysicalMachine current = currentInfo.getPM();

				// identify the overloaded servers
				AlterableResourceConstraints toTest = getThresholdResourcesA1(current);
				if(current.availableCapacities.getTotalProcessingPower() < toTest.getTotalProcessingPower()
						|| current.availableCapacities.getRequiredMemory() < toTest.getRequiredMemory()) {
					overloaded.add(current);
//					Logger.getGlobal().info("Added pm " + current.toString() + " to overloaded pms.");
				}
			}
		}
		
		// identify possible overloaded PMs in the future
		for(int i = 0; i < toAnalyze.getPMInformation().size(); i++) {
			PMInformation currentInfo = toAnalyze.getPMInformation().get(i);
			PhysicalMachine current = currentInfo.getPM();
			
			// skip this part if there is no previous value for the current PM
			if(currentInfo.areLastValues()) {
				
				// the amount of previous values
//				int lastValues = currentInfo.getAmountOfPrevious();
				
				List<ResourceConstraints> all = new ArrayList<>();
				all.add(currentInfo.getLoad());

				// only use n values in total
//				int max = currentInfo.getLastValues().size() > (lastValues - 1) ? lastValues : currentInfo.getLastValues().size();
//				for(int j = 0; j < max; j++) {
//					all.add(currentInfo.getLastValues().get(j));
//				}
//				all.addAll(currentInfo.getPrevious());
				
				// only use the last value and the current one
				all.add(currentInfo.getLastValues().get(0));
				
				// debug: log the amount of values that are considered
//				Logger.getGlobal().info("For linear regression considered amount of values: " + all.size());

				ResourceConstraints calc = AACHelper.calculateSimpleLinearRegression(all, true);
				ResourceConstraints predicted = new AlterableResourceConstraints(
						AACHelper.roundDouble(calc.getRequiredCPUs(), 2), calc.getRequiredProcessingPower(),
						calc.getRequiredMemory());
				
				// compare the resources	TODO previous version
//				ResourceConstraints previousLoad = currentInfo.getLastValues().get(0);
//				ResourceConstraints currentLoad = currentInfo.getLoad();
//				
//				List<ResourceConstraints> lastTwoValues = new ArrayList<>();
//				lastTwoValues.add(currentLoad);
//				lastTwoValues.add(previousLoad);
//				
//				ResourceConstraints predicted = AACHelper.calculateSimpleLinearRegression(lastTwoValues, true);
				AlterableResourceConstraints toTest = getThresholdResourcesA1(current);
				
				if( (predicted.getTotalProcessingPower() * thresholdA1) > toTest.getTotalProcessingPower() ||
						(predicted.getRequiredMemory() * thresholdA1) > toTest.getRequiredMemory()) {
					possibleOverloaded.put(current, predicted);
				}
						
			}
		}
		
		// if defaults are used we have to collect the suspended VMs, too
		if(useDefaults) {
			List<VirtualMachine> suspended = new ArrayList<VirtualMachine>();
			
			if(toAnalyze.areSuspendedVMs()) {
				for(VMInformation info: toAnalyze.getVMInformation()) {
					if(info.isSuspended()) {
						suspended.add(info.getVM());					
					}
				}
			}
			output.setSuspendedVMs(suspended);
		}
		
		Logger.getGlobal().info("Found " + overloaded.size() + " overloaded PMs and " + 
				possibleOverloaded.size() + " possible overloaded PMs.");
		
		// set the values
		output.setOverloaded(overloaded);
		output.setPossibleOverloaded(possibleOverloaded);				
	}
	
	
	private AlterableResourceConstraints getThresholdResourcesA1(PhysicalMachine current) {
		// identify the overloaded servers
		AlterableResourceConstraints threshold = new AlterableResourceConstraints(
				current.getCapacities().getRequiredCPUs() * thresholdA1,
				current.getCapacities().getRequiredProcessingPower(),
				(long) (current.getCapacities().getRequiredMemory() * thresholdA1));

		AlterableResourceConstraints toTest = new AlterableResourceConstraints(current.getCapacities());

		// after getting the threshold resources we subtract them from the whole capacities to get
		// only the resources which have to be left so the PM is not considered overloaded
		toTest.subtract(threshold);
		return toTest;
	}
	
	/**
	 * Identify overloaded servers and suspended VMs
	 * 
	 * Determine the currently overloaded PMs. Go through each VM and
	 * check whether its state is suspended.
	 * 
	 * @param output The AnalyzerOutput to save the results in.
	 * @param toAnalyze The MonitorInformation, containing the results to analyze.
	 */
	private void useAlgorithmA2(MonitorInformation toAnalyze, AnalyzerOutput output) {
		
		/** store the overloaded PMs and suspended VMs */
		List<PhysicalMachine> overloaded = new ArrayList<PhysicalMachine>();
		List<VirtualMachine> suspended = new ArrayList<VirtualMachine>();
		
		// identify overloaded PMs 
		if(toAnalyze.areOverloadedServers()) {
			for(int i = 0; i < toAnalyze.getPMInformation().size(); i++) {
				PMInformation currentInfo = toAnalyze.getPMInformation().get(i);
				PhysicalMachine current = currentInfo.getPM();
					
				// identify the overloaded servers
				AlterableResourceConstraints threshold = new AlterableResourceConstraints(
						current.getCapacities().getRequiredCPUs() * thresholdA2, current.getCapacities().
						getRequiredProcessingPower(), (long) (current.getCapacities().getRequiredMemory() 
								* thresholdA2));
				
				AlterableResourceConstraints toTest = new AlterableResourceConstraints(current.getCapacities());
				
				// after getting the threshold resources we subtract them from the whole capacities to get 
				// only the resources which have to be left so the PM is not considered overloaded
				toTest.subtract(threshold);
				if(current.availableCapacities.getTotalProcessingPower() < toTest.getTotalProcessingPower() || 
						current.availableCapacities.getRequiredMemory() < toTest.getRequiredMemory()) { 
					overloaded.add(current);
//					Logger.getGlobal().info("Added pm " + current.toString() + " to overloaded pms.");
				}
			}
		}		
		
		// identify suspended VMs 
		if(toAnalyze.areSuspendedVMs()) {
//			Logger.getGlobal().info("There are suspended VMs to find...");
			for(VMInformation info: toAnalyze.getVMInformation()) {
				if(info.isSuspended()) {
					suspended.add(info.getVM());					
				}
			}
		}
		
		Logger.getGlobal().info("Found " + overloaded.size() + " overloaded PMs and " + 
				suspended.size() + " suspended VMs.");
				
		// set the values
		output.setOverloaded(overloaded);
		output.setSuspendedVMs(suspended);
	}

	/**
	 * Assess which subsets of VMs have load patterns that are compatible for 
	 * long-term co-residence. For the analysis of the correlations between
	 * each VM-pair the current CPU-load is used.
	 * 
	 * FIXME: NOTE: currently there is no correlation calculation done due to the traces length (no long term usage)
	 * 
	 * @param output The AnalyzerOutput to save the results in.
	 * @param toAnalyze The MonitorInformation, containing the results to analyze.
	 */
	private void useAlgorithmA3(MonitorInformation toAnalyze, AnalyzerOutput output) {
		// to find correlations between the VMs, we have to add all previous values
		// of the VM's resources to be compared and than calculate the correlation
				
		List<VMInformation> allInfo = toAnalyze.getVMInformation();
		
		/** contains the correlation of a VM pair */
//		Map<VirtualMachine[], Double> resultOne = algorithmA3correlationCalculation(allInfo);
		/** contains the adjusted resources for each VM */
		Map<VirtualMachine, ResourceConstraints> resultTwo = algorithmA3sizePrediction(allInfo);
		
//		Logger.getGlobal().info("Created " + resultOne.size() + " correlation.");
		
		// save the data of the analysis (the new sizes of each VM and the correlations)		
//		output.setCorrelations(resultOne);
		output.setSizes(resultTwo);
	}
	
	@SuppressWarnings("unused")
	private Map<VirtualMachine[], Double> algorithmA3correlationCalculation(List<VMInformation> allInfo) {
		Map<VirtualMachine[], Double> correlations = new HashMap<VirtualMachine[], Double>();
		
		// only use the first 200 VMs and ignore the rest
//		if(toAnalyze.getVMInformation().size() >= 200) {
//			allInfo = allInfo.subList(0, 200);
//		}
		
		for(VMInformation currentInfo: allInfo) {
			for(VMInformation toTestInfo: allInfo) {
				VirtualMachine current = currentInfo.getVM();
				VirtualMachine toTest = toTestInfo.getVM();
				
				// if both VMs are the same, continue
				if(current.hashCode() == toTest.hashCode()) {
					continue;
				}
				else {
					// check if there is already a value for both VMs together
					if(correlations.containsKey(new VirtualMachine[]{current, toTest}) ||
							correlations.containsKey(new VirtualMachine[]{toTest, current})) {
						continue;
					}
					// there is no value for this VM-pair
					else {
						int valuesToConsider = currentInfo.getPrevious().size();
						if(valuesToConsider > toTestInfo.getPrevious().size()) {
							valuesToConsider = toTestInfo.getPrevious().size();
						}
						// add up the current load
						valuesToConsider++;
						
						// check if there are at least 2 values
						if(valuesToConsider < 2) {
							correlations.put(new VirtualMachine[]{current, toTest}, 0.0);
						}
						else {
							// array 1
							double[] arr1 = new double[valuesToConsider];
							List<ResourceConstraints> all = new ArrayList<>(currentInfo.getPrevious());
							all.add(0, currentInfo.getLoad());
							for(int i = 0; i < valuesToConsider; i++) {
								arr1[i] = all.get(i).getTotalProcessingPower();
							}
							
							// array 2
							double[] arr2 = new double[valuesToConsider];
							List<ResourceConstraints> all2 = new ArrayList<>(toTestInfo.getPrevious());
							all2.add(0, toTestInfo.getLoad());
							for(int i = 0; i < valuesToConsider; i++) {
								arr2[i] = all2.get(i).getTotalProcessingPower();
							}
							
							// the actual calculation of the coefficient
							double cor = calculatePearsonsCorrelation(arr1, arr2);
							
							if(cor == 0.0) {
								String errorString = "Correlation of VM-pair " + current.hashCode() + " - " + toTest.hashCode() + 
										" is zero (0.0). First ttp values: [";
								for(Double curr: arr1) {
									errorString += curr + ", ";
								}
								
								errorString = errorString.substring(0, errorString.length() - 2);
								errorString += "] Second ttp values: [";
								
								for(Double curr: arr2) {
									errorString += curr + ", ";
								}
								
								errorString = errorString.substring(0, errorString.length() - 2);
								errorString += "]";
								
								
								Logger.getGlobal().info(errorString);
								correlations.put(new VirtualMachine[]{current, toTest}, 0.0);
							}
							else if(Double.isNaN(cor)) {
								String errorString = "VM-Pair " + current.hashCode() + " - " + toTest.hashCode() + 
										", error during calculation. First ttp values: [";
								for(Double curr: arr1) {
									errorString += curr + ", ";
								}
								
								errorString = errorString.substring(0, errorString.length() - 2);
								errorString += "] Second ttp values: [";
								
								for(Double curr: arr2) {
									errorString += curr + ", ";
								}
								
								errorString = errorString.substring(0, errorString.length() - 2);
								errorString += "]";
								
								
								Logger.getGlobal().info(errorString);
								correlations.put(new VirtualMachine[]{current, toTest}, 0.0);
							}
							else {
								// if a correct correlation could be determined, we set it here, otherwise 0.0 is set
								// to prevent errors inside gurobi
								correlations.put(new VirtualMachine[]{current, toTest}, cor);
							}
							
						}
												
					}
				}
			}
			
		}
		
		return correlations;
	}
	
	private Map<VirtualMachine, ResourceConstraints> algorithmA3sizePrediction(List<VMInformation> allInfo) {
		Map<VirtualMachine, ResourceConstraints> sizes = new HashMap<VirtualMachine, ResourceConstraints>();
		
		// collect the current and all previous sizes of each VM and use the average of them
		for (VMInformation currentInfo : allInfo) {

			// we need at least 2 infos
			if (currentInfo.getPrevious().size() > 0) {
				int lastValues = currentInfo.getAmountOfPrevious();
				ResourceConstraints current = currentInfo.getLoad();

				List<ResourceConstraints> all = new ArrayList<>();
				all.add(current);

				// only use n values in total
				int max = currentInfo.getPrevious().size() > (lastValues - 1) ? lastValues : currentInfo.getPrevious().size();
				for(int i = 0; i < max; i++) {
					all.add(currentInfo.getPrevious().get(i));
				}
//				all.addAll(currentInfo.getPrevious());
				
				// debug: log the amount of values that are considered
				Logger.getGlobal().info("For linear regression considered amount of values: " + all.size());

				ResourceConstraints calc = AACHelper.calculateSimpleLinearRegression(all, false);
				ResourceConstraints predicted = new AlterableResourceConstraints(
						AACHelper.roundDouble(calc.getRequiredCPUs(), 2), calc.getRequiredProcessingPower(),
						calc.getRequiredMemory());

				// FIXME: produces lowest overload, but most PMs used
//				if(predicted.getRequiredCPUs() < currentInfo.getLoad().getRequiredCPUs()) {
//					predicted = new AlterableResourceConstraints(currentInfo.getLoad());
//				}

				// debug string
//				String debug = "VM " + currentInfo.getVM().hashCode() + 
//						", last 2 values: " + "[" + previous.getRequiredCPUs() + 
//						"," + previous.getRequiredProcessingPower() + "," + previous.getRequiredMemory() + "] " +
//						"[" + current.getRequiredCPUs() + "," + current.getRequiredProcessingPower() + "," + 
//						current.getRequiredMemory() + "] " + 
//						" determined: " + "[" + predicted.getRequiredCPUs() + "," + predicted.getRequiredProcessingPower() + "," + 
//						predicted.getRequiredMemory() + "]";
//				Logger.getGlobal().info(debug);

				sizes.put(currentInfo.getVM(), predicted);
			}

		}
		
		return sizes;
	}
	
	/**
	 * Helper method to determine the pearsons correlation out of two double-arrays.
	 * 
	 * @param first The first array.
	 * @param second The second array.
	 * 
	 * @return The correlation, if it could be determined.
	 */
	@SuppressWarnings("unused")
	private double calculatePearsonsCorrelation(double[] first, double[] second) {
		return new PearsonsCorrelation().correlation(first, second);
	}

	/**
	 * Identify significant changes since the last invocation
	 * 
	 * At first set a threshold to determine a significant change. After that compare
	 * the last MonitorInformation-object with the current one and use the defined
	 * threshold to find those changes.
	 * 
	 * If there are no changes, nothing is analyzed.
	 * 
	 * @param output The AnalyzerOutput to save the results in.
	 * @param toAnalyze The MonitorInformation, containing the results to analyze.
	 */
	private void useAlgorithmA4(MonitorInformation toAnalyze, AnalyzerOutput output) {
		
		List<PhysicalMachine> significantIncPMs = new ArrayList<PhysicalMachine>();
		List<PhysicalMachine> significantDecPMs = new ArrayList<PhysicalMachine>();
		
		// check if there is a previous value for the load of each PM
		// skip those with no previous loads, continue with the rest
		List<PMInformation> prev = new ArrayList<>();
		String log = "There are no previous measures for the following PMs: ";
		
		// find the PMs with previous measures
		for(PMInformation info: toAnalyze.getPMInformation()) {
			if(!info.areLastValues())
				log += info.getPM().hashCode() + ", ";
			else
				prev.add(info);
		}
		
		// only use the algorithm if at least one VM has previous information available
		if(!prev.isEmpty()) {

			if(!(prev.size() == toAnalyze.getPMInformation().size()))
				Logger.getGlobal().info(log);

			// compare the current MonitorInformation with the previous one:
			// if there are no significant changes, create an empty output
			for(PMInformation info : prev) {
				PhysicalMachine current = info.getPM();

				// if the resources are different from before, we need to find load differences
				// in the hosted VMs and to check whether new VMs are hosted or old ones are dismissed, 
				// otherwise nothing is done
				if(info.getLastValues().get(0).compareTo(info.getLoad()) != 0) {

					// collect the VM loads and filter out new VMs
					Map<VMInformation, ResourceConstraints> vmLoads = new HashMap<VMInformation, ResourceConstraints>();
					for(VMInformation vmInfo : info.getVMInfo()) {
						// filter out new VMs since the last run
						if(vmInfo.getPrevious().size() != 0) {
							vmLoads.put(vmInfo, vmInfo.getLoad());
						}
					}
					
					// error case: vmLoads.size() is not the amount of hosted VMs
					if(vmLoads.size() != info.getVMs().size()) {
						Logger.getGlobal().info("Not all hosted VMs of PM " + info.getPM().hashCode() + " have previous information.");
					}

					// calculate a prediction
					double prevTotal = 0, currentTotal = 0;
					for(VMInformation curr : vmLoads.keySet()) {
						prevTotal += curr.getPrevious().get(0).getTotalProcessingPower();
						currentTotal += curr.getLoad().getTotalProcessingPower();
					}
					
					// compare the previous load with the current one and determine whether it has increased / decreased 
					// according to the threshold
					double thresholdTotal = prevTotal * thresholdA4;
					double upperBound = prevTotal + thresholdTotal;
					double lowerBound = prevTotal - thresholdTotal;
					
					// involve the A4 threshold
					if (currentTotal > upperBound) {
						// load increased
						significantIncPMs.add(current);
					} else if (currentTotal < lowerBound) {
						// load decreased
						significantDecPMs.add(current);
					}					
						
				} 
				else {
					Logger.getGlobal().info("No previous information available for PM " + current.hashCode() + ".");
				}
			}

		}

		// debug
		Logger.getGlobal().info("Found " + significantIncPMs.size() + " PMs with significant" + " increased load and "
				+ significantDecPMs.size() + " PMs with significant" + " decreased load.");

		// set the values
		output.setSignificantIncreasingPMs(significantIncPMs);
		output.setSignificantDecreasingPMs(significantDecPMs);

	}

}
