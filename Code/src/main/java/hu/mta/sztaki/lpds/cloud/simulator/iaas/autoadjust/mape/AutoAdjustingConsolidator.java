package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.AnalyzerOutput;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.MonitorInformation;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.Plan;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.consolidation.Consolidator;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.AlterableResourceConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;

/**
 * An implementation of a consolidator using auto-adjust to do the consolidation.
 */
public class AutoAdjustingConsolidator extends Consolidator {
	
	/** the name of the file to write the decisions and loads into */
	private String baseOutputFile = "DecisionsAndLoads.csv";
	private boolean first = true;
	
	/** IaaSService */
	private IaaSService toConsolidate;
	
	/** internal objects */
	private Monitor monitor;
	private Analyzer analyzer;
	private Planner planner;
	private Executer executer;
	
	/** Helper class for statistics like counters */
	public AACHelper helper;
	
	/** current and previous situations */
	private MonitorInformation information;
	private AnalyzerOutput analysis;	
	private Plan plan;
	
	/** the properties file, containing default values and other information */
	private Properties props;
	
	/** Contains time measurements for each algorithm. The key is the consolidation run, for the values
	 * we have an array containing for values, one for each used algorithm, so the first value represents
	 * the monitor, the second one the analysis, the third one the planning and the last one the execution. */
	private Map<Long, Long[]> algorithmDurations;	
	
	/** contains VMs that exceeded their previous allocation and thus are not placed on a PM anymore */
	private Map<VirtualMachine, ResourceConstraints> toBePlaced = new HashMap<>();
	
	
	/** internal state */
	private ContextSituationClass contextSituationClass;
	private AnalyzeParameter analyzeParameter;
	private PlanningParameter planningParameter;
	private FrequencyParameter frequencyParameter;
	private long freq;
	private boolean useDefaults;
	private boolean doExtendedLogging;
	private boolean suspendAndResume;
	
	/** information of the planner algorithms */
	private boolean allowP1suspensions, allowP4suspensions;	
		
	/** the long-values for the frequency */
	private long f1, f2;
	
	/** context situation classes */
	public static enum ContextSituationClass {
		/** load is quickly growing */
		C1,
		/** load is decreasing; there are suspended VMs and/or overloaded servers */
		C2,
		/** load is decreasing; there are neither suspended VMs nor overloaded servers */
		C3,
		/** normal operation, none of the above applies */
		C0
	} 
	
	/** parameter for the analyze step */
	public static enum AnalyzeParameter {
		/** identify servers that are either overloaded or likely to become overloaded soon */
		A1,
		/** identify overloaded servers and suspended VMs */
		A2,
		/** assess which subsets of VMs have load patterns that are compatible for long-term 
		    co-residence */
		A3,
		/** identify significant changes since the last invocation */
		A4
	}
	
	/** parameter for the planning step */
	public static enum PlanningParameter {
		/** quickly find a plan for relieving highly loaded servers with migrations or 
		    VM suspensions */
		P1,
		/** quickly find a plan to resume suspended VMs and relieve overloaded servers */
		P2,
		/** find the globally best allocation of VMs to servers */
		P3,
		/** devise a set of local actions to address changes since the last invocation */
		P4
	}
	
	/** frequency of the loop */
	public static enum FrequencyParameter {
		/** normal execution */
		F1,
		/** more frequent execution */
		F2
	}
	
	/**
	 * Default Constructor for simulations, enables logging and disables defaults.
	 * 
	 * @param toConsolidate The IaaSService to be consolidated.
	 * @param consFreq The frequency of this consolidator to work.
	 */
	public AutoAdjustingConsolidator(IaaSService toConsolidate, long consFreq) {
		// create a new instance without using defaults but with logging enabled
		this(toConsolidate, consFreq, false, true, true);
	}	

	/**
	 * Constructor for the consolidator supporting auto-adjusted consolidation.
	 * The consolidator is expected to be created once and to live until consolidation
	 * is no longer needed. 
	 * 
	 * Note that we use default parameters in the first iteration after creating
	 * an instance of this consolidator.
	 * 
	 * @param toConsolidate The IaaSService to be consolidated.
	 * @param consFreq The frequency of this consolidator to work.
	 * @param useDefaults Determines if auto-adjust is skipped and default 
	 * 			parameters are used.
	 * @param doLogging Enables or disables logging.
	 */
	public AutoAdjustingConsolidator(IaaSService toConsolidate, long consFreq, boolean useDefaults, boolean doLogging,
			boolean doExtendedLogging) {
		super(toConsolidate, consFreq);
		
		// initialize / set the necessary objects of this class
		this.toConsolidate = toConsolidate;
		this.doExtendedLogging = doExtendedLogging;
		
		algorithmDurations = new HashMap<Long, Long[]>();
		
		// false: use auto-adjust
		// true: use defaults out of the properties
		this.useDefaults = useDefaults;
				
		// load the properties
		try {
			setProps();
		} catch (InvalidPropertiesFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		monitor = new Monitor();
		analyzer = new Analyzer(useDefaults);
		planner = new Planner(useDefaults, allowP1suspensions, allowP4suspensions);
		executer = new Executer(suspendAndResume);
				
		if(!doLogging) {
			Logger.getGlobal().setLevel(Level.OFF);
		}
		
		// if we are in the first iteration, we have to determine the default parameters
		// and create a new MonitorInformation
		if(AACHelper.iterationCount == 0) {			
			String defaultFrequency = props.getProperty("defaultFrequency");	
			String defaultA = props.getProperty("defaultA");
			String defaultP = props.getProperty("defaultP");
			setDefaults(defaultFrequency, defaultA, defaultP);		
			
			// create a new MonitorInformation
			information = new MonitorInformation(helper.getAmountOfPrevious(), helper.getThresholdLoadBehaviour());
		}
		if(!useDefaults) {
			Logger.getGlobal().info("Created the AAC using autoadjust.");
		}
		else {
			Logger.getGlobal().info("Created the AAC using " + analyzeParameter + " " + planningParameter + " " + 
					frequencyParameter + ".");
		}		
		
	}

	/**
	 * Here the MAPE-loop has to be used. It shall be possible to configure each part of
	 * the loop and the loop itself.
	 * 
	 * The MAPE loop, which goes through this process repeatedly, is aimed at finding 
	 * system problems as early as possible and addressing them efficiently.
	 * 
	 * Despite the description in the paper, we do everything sequential here.
	 */
	@Override
	protected void doConsolidation(PhysicalMachine[] pmList) {
		
		Logger.getGlobal().info("Start to consolidate, iteration: " + AACHelper.iterationCount);
		
		if(AACHelper.iterationCount > 0)
			executer.compareActionsWithChanges();
		
		/** beginning of the MAPE loop */
		monitoring();
		
		// the extended log is written at the beginning of each iteration to also contain 
		// the actions executed after consolidation has finished
		if(doExtendedLogging) {
			String appendum = "";
			if(useDefaults)
				appendum += analyzeParameter.toString() + planningParameter.toString() + frequencyParameter.toString();
			else
				appendum = "autoAdjust";

			// write the load log
			try {
				writeLogfile(first, appendum);
			} catch (IOException e) {
				e.printStackTrace();
			}
			first = false;
		}
						
		// only use autoAdjust() if we do not use defaults
		if(!useDefaults) {
			autoAdjust();
		}		
						
		analyzing();
				
		planning();
					
		executing();
		
		/** end of the MAPE loop */
		
		// reset the list with the unplaced VMs
		toBePlaced.clear();		
		
		// add all currently active machines and divide them add the end through the amount of iterations
		AACHelper.averageActivePMs += toConsolidate.runningMachines.size();
				
		Logger.getGlobal().info(getLoadAndContextSituationClass());
//		Logger.getGlobal().info("cpu overload in this iteration: " + AACHelper.currentOverload.getTotalProcessingPower());
		Logger.getGlobal().info("Finished iteration " + AACHelper.iterationCount + " at " + Timed.getFireCount());
		
//		AlterableResourceConstraints loadAfterCons = information.calculateTotalLoadWithoutReset();
//		Logger.getGlobal().info("after consolidation, current load: " + loadAfterCons + ", in tpp: " + 
//				loadAfterCons.getTotalProcessingPower());
		
		// after doing one consolidation loop, we do not need default parameters
		AACHelper.iterationCount++;
		
		// save the durations
		readDurations();
		
		// print all decisions made so far
//		Logger.getGlobal().info(decisionsToString());
	}
	
	/**
	 * Writes the decisions and loads into a .csv-file after each iteration.
	 * 
	 * @throws IOException 
	 */
	private void writeLogfile(boolean first, String appendum) throws IOException {
		File output = new File(appendum + baseOutputFile);	
		BufferedWriter writer; 
		if(first) {
			writer = new BufferedWriter(new FileWriter(output));
			
			// write the first line
			writer.write("iteration;contextSituationClass;current_load;previous_load;upper_threshold_previous;"
					+ "lower_threshold_previous;active_PMs;current_overload;migrations;suspends;resumes;"
					+ "started_PMs;stopped_PMs");
			writer.newLine();
		}
		else
			writer = new BufferedWriter(new FileWriter(output, true));
		
		// write a line with all results
		if(first) {
			writer.write(AACHelper.iterationCount + ";" + this.contextSituationClass + ";" + 
					AACHelper.roundDouble(information.getTotalLoad().getTotalProcessingPower(), 3) + ";null;null;null;" + 
					monitor.getActivePMs() + ";" + 
					AACHelper.roundDouble(AACHelper.currentOverload.getTotalProcessingPower(), 3) + ";" + 
					AACHelper.migrations + ";" + 
					AACHelper.suspensions + ";" + 
					AACHelper.resumes + ";" +
					AACHelper.startPM + ";" + 
					AACHelper.stopPM);
		}
		else {
			writer.write(AACHelper.iterationCount + ";" + this.contextSituationClass + ";" + 
					AACHelper.roundDouble(information.getTotalLoad().getTotalProcessingPower(), 3) + ";" + 
					AACHelper.roundDouble(information.getPreviousTotalLoad().getTotalProcessingPower(),3 ) + ";" + 
					AACHelper.roundDouble(information.getUpperPreviousTotalLoad().getTotalProcessingPower(), 3) + ";" + 
					AACHelper.roundDouble(information.getLowerPreviousTotalLoad().getTotalProcessingPower(), 3) + ";" + 
					monitor.getActivePMs() + ";" + 
					AACHelper.roundDouble(AACHelper.currentOverload.getTotalProcessingPower(), 3) + ";" + 
					AACHelper.migrations + ";" + 
					AACHelper.suspensions + ";" + 
					AACHelper.resumes + ";" +
					AACHelper.startPM + ";" + 
					AACHelper.stopPM);
		}		
		
		writer.newLine();		
		writer.close();
	}
	
	/**
	 * Save the unplaced VMs inside the consolidator to pass them later.
	 * 
	 * @param toBePlaced The unplaced VMs in the IaaS.
	 */
	public void passUnplacedVMs(Map<VirtualMachine, ResourceConstraints> toBePlaced) {
		this.toBePlaced = toBePlaced;
	}
	
	/**
	 * Example parameter: Frequency of sensor readings
	 * Here we fill our lists with all PMs and their hosted VMs.
	 * 
	 * The whole output is used by auto-adjust, but it shall be
	 * possible to only use only a part of it, too.
	 */
	private void monitoring() {
		
		// overload is directly passed to the AACHelper
		information = monitor.monitor(toConsolidate, information, helper.getAmountOfPrevious(), 
				toBePlaced, helper.getUpperThreshold(), helper.getThresholdLoadBehaviour(), helper.getUpperCriticalThreshold());
	}
	
	/**
	 * Example parameter: Number of past observations to take into account
	 * 
	 * Possible usage: 
	 * 		- auto-adjust and analyze run in parallel without affecting each other
	 * 		- the same, but auto-adjust may cancel analyze
	 * 		- analyze starts after auto-adjust has finished and has possibly been
	 * 		  reconfigured
	 */
	private void analyzing() {
		analysis = analyzer.analyze(analyzeParameter, information, helper.getThresholdA1(), 
				helper.getThresholdA2(), helper.getThresholdA4());		
	}
	
	/**
	 * Example parameter: Allowed execution time for optimization algorithm
	 */
	private void planning() {
		plan = planner.plan(planningParameter, analysis, helper.getThresholdP1(), helper.getThresholdP2(),
				helper.getThresholdP4(), helper.getGurobiTimeLimit(), helper.getCorrelationThreshold(), 
				helper.getLowerThreshold(), helper.getUpperThreshold(), helper.getThresholdGurobiPMs());
	}
	
	/**
	 * Example parameter: Number of adaptations to carry out in parallel
	 * 
	 * At the moment we do not use concurrency.
	 */
	private void executing() {
		executer.execute(plan, toConsolidate);
	}
	
	/**
	 * The auto-adjust uses two sub-steps to configure the rest of the run. It
	 * uses (a part of) the monitoring-output to determine the current situation
	 * and to configure the other steps accordingly. For that, different
	 * context-situation classes are used.
	 */
	private void autoAdjust() {		
		
		/**
		 * Coarse-grained categorization of the current situation (including the type of 
		 * change that is going on) based on the monitoring input. This involves 
		 * defining a set C of context situation classes, and mapping each possible 
		 * monitoring input to a context situation class.
		 */
		determineContextSituationClass();		
		
		/**
		 * Determining appropriate parameter settings based on the identified situation. 
		 * The second step can be formalized as a function h, mapping each context 
		 * situation class to the appropriate parameter settings.
		 */
		switch(contextSituationClass) {
		case C1: 
			this.analyzeParameter = AnalyzeParameter.A1;
			this.planningParameter = PlanningParameter.P1;
			this.frequencyParameter = FrequencyParameter.F2;
			this.freq = f1;
			break;
		case C2:
			this.analyzeParameter = AnalyzeParameter.A2;
			this.planningParameter = PlanningParameter.P2;
			this.frequencyParameter = FrequencyParameter.F1;
			this.freq = f1;
			break;
		case C3:
			this.analyzeParameter = AnalyzeParameter.A3;
			this.planningParameter = PlanningParameter.P3;
			this.frequencyParameter = FrequencyParameter.F1;
			this.freq = f1;
			break;
		case C0: 
			this.analyzeParameter = AnalyzeParameter.A4;
			this.planningParameter = PlanningParameter.P4;
			this.frequencyParameter = FrequencyParameter.F1;
			this.freq = f2;
			break;
		}
		
		updateFrequency(freq);		
	}
	
	/**
	 * Evaluate the output of the monitoring-step and sets the current ContextSituationClass
	 * based on it.
	 * 
	 * we use an aggregate metric that shows whether there is an overall
	 * growth in load: the ratio of VMs whose load grew by at least
	 * a given percentage since the last measurement
	 */
	private void determineContextSituationClass() {
		
		// if this is the first iteration, expect normal operation
		if(AACHelper.iterationCount == 0) {
			this.contextSituationClass = ContextSituationClass.C0;
			AACHelper.c0amount++;
		}
		// compare the current total load with the previous one
		else {
			AlterableResourceConstraints current = new AlterableResourceConstraints(information.getTotalLoad());
			
			// debug
//			Logger.getGlobal().info("considering totalProcPower, current: " + current.getTotalProcessingPower() + ", upperPrev: " + 
//					upperPrevious.getTotalProcessingPower() + ", lowerPrev: " + lowerPrevious.getTotalProcessingPower());
			
			// load is quickly growing; check the upper bound
			if(current.getTotalProcessingPower() > information.getUpperPreviousTotalLoad().getTotalProcessingPower()) {
				
				this.contextSituationClass = ContextSituationClass.C1;	
				AACHelper.c1amount++;
			}
			// load is decreasing; check the lower bound
			else {
				if(current.getTotalProcessingPower() < information.getLowerPreviousTotalLoad().getTotalProcessingPower()) {
					
					if(information.areOverloadedServers() || information.areSuspendedVMs()) {
						// there are suspended VMs and/or overloaded servers
						this.contextSituationClass = ContextSituationClass.C2;	
						AACHelper.c2amount++;
					}
					else {
						// there are neither suspended VMs nor overloaded servers
						this.contextSituationClass = ContextSituationClass.C3;
						AACHelper.c3amount++;
					}
				}
				else {
					// normal operation, none of the above applies
					this.contextSituationClass = ContextSituationClass.C0;
					AACHelper.c0amount++;
				}
			}
				
		}		
//		Logger.getGlobal().info("after evaluating used: " + contextSituationClass);		
	}
	
	private String getLoadAndContextSituationClass() {
		// in the first iteration only print the current load
		if(information.getPreviousTotalLoad() == null) {
//			String result = "considering tpp * mem, current: " + (information.getTotalLoad().getTotalProcessingPower() * 
//					information.getTotalLoad().getRequiredMemory());
			String result = "considering tpp , current: " + information.getTotalLoad().getTotalProcessingPower();
			
			result += "\n" + "using default values instead of a CSC: ";
			result += this.analyzeParameter + " " + this.planningParameter + " " + this.frequencyParameter;
			
			return result;
		}		
		// in all other iterations, print the whole log
		else {
			String result = "considering tpp, current: " + information.getTotalLoad().getTotalProcessingPower() + 
					", upperPrev: " + information.getUpperPreviousTotalLoad().getTotalProcessingPower() + 
					", lowerPrev: " + information.getLowerPreviousTotalLoad().getTotalProcessingPower();
			
			result += "\n" + "used CSC: " + this.contextSituationClass;			
			return result;
		}
		
	}
	
	/**
	 * Get the MonitorInformation.
	 * 
	 * @return The MonitorInformation.
	 */
	public MonitorInformation getMonitorInformation() {
		return information;
	}
	
	/**
	 * Get the AnalyzerOutput.
	 * 
	 * @return The AnalyzerOutput.
	 */
	public AnalyzerOutput getAnalyzerOutput() {
		return analysis;
	}
	
	/**
	 * Get the current Plan.
	 * 
	 * @return The current Plan.
	 */
	public Plan getPlan() {
		return plan;
	} 
	
	/**
	 * Represents the state of the AutoAdjustingConsolidator.
	 */
	public String toString() {
		String result = "";
		
		// get the important parameters		
		result += "parameters: " + analyzeParameter + ", " + planningParameter 
			+ ", " + frequencyParameter + ", context class: ";
		if(contextSituationClass == null) {
			result += "no class determined yet\n";
		}
		else {
			result += contextSituationClass + "\n";
		}		
		
		// get the total overload to this point
		result += "total overload so far: " + AACHelper.totalOverload.getTotalProcessingPower() + "\n";
		
		// get the decisions made so far
		result += decisionsToString();
		
		if(AACHelper.iterationCount == 0) {
			result += "\n" + "no iterations done yet";
		}
		
		return result;
	}
	
	/**
	 * Determines if one iteration is finished.
	 * 
	 * @return True if the executer is finished. This is the case, when the actions
	 * 			have been created and the execution of them has been started. NOTE
	 * 			that the actions do not necessarily need to be finished at this point.
	 */
	public boolean hasExecuterFinished() {
		return executer.isFinished();
	}
	
	/**
	 * Resets the counter of the AACHelper.
	 */
	public static void resetAll() {
		AACHelper.resetCounter();
		AACHelper.totalOverload = AACHelper.getNoResources();
		AACHelper.totalLoad = AACHelper.getNoResources();
	}
	
	/**
	 * A String representation of the made decisions of auto adjust.
	 * 
	 * @return A String representation of the made decisions of auto adjust.
	 */
	public static String decisionsToString() {
		return "Decisions, C0: " + AACHelper.c0amount + ", C1: " + AACHelper.c1amount + 
				", C2: " + AACHelper.c2amount + ", C3: " + AACHelper.c3amount;
	}
	
	/**
	 * Helper function for monitoring each algorithm's duration. The values are saved in an
	 * internal mapping containing the current iteration and the durations.
	 * 
	 * @return The duration of each part in a String.
	 */
	private String readDurations() {
		algorithmDurations.put(AACHelper.iterationCount, new Long[] {
				monitor.getDuration(),
				analyzer.getDuration(),
				planner.getDuration(),
				executer.getDuration()
		});
		
		return "Durations of the algorithms: \n monitoring: " + monitor.getDuration() + ", "
				+ analyzeParameter + ": " + analyzer.getDuration() + ", " + planningParameter + 
				": " + planner.getDuration() + ", execution: " + executer.getDuration() + ".";
	}
	
	/**
	 * Helper method to get insight on the duration of each algorithm.
	 * 
	 * @param iteration Determines the round to look for the durations.
	 * @return An array containing the durations of each algorithm according to MAPE in ms. If
	 * 				the parameter is out of the bounds, it will return null.
	 */
	public Long[] getSpecificDurations(long iteration) {
		if(iteration < 0 || iteration > AACHelper.iterationCount) {
			System.err.println("There are no durations for this iteration: " + iteration + ".");
			return null;
		}
		
		return algorithmDurations.get(iteration);
	}
	
	/**
	 * Sets the properties with the regarding properties file and instantiates the
	 * helper.
	 * 
	 * @throws InvalidPropertiesFormatException
	 * @throws IOException
	 */
	private void setProps() throws InvalidPropertiesFormatException, IOException {
		props = new Properties();
		File file = new File("autoAdjustDefaults.xml");
		FileInputStream fileInput = new FileInputStream(file);
		props.loadFromXML(fileInput);
		fileInput.close();
		
		// read the properties
		this.f1 = Long.parseLong(props.getProperty("F1")); 
		this.f2 = Long.parseLong(props.getProperty("F2"));
		this.suspendAndResume = Boolean.parseBoolean(props.getProperty("suspendAndResume"));
		this.allowP1suspensions = Boolean.parseBoolean(props.getProperty("allowP1suspensions"));
		this.allowP4suspensions = Boolean.parseBoolean(props.getProperty("allowP4suspensions"));		
		
		// instantiate the AACHelper with all properties
		helper = new AACHelper(
				Integer.parseInt(props.getProperty("n")), 
				Double.parseDouble(props.getProperty("thresholdA1")), 
				Double.parseDouble(props.getProperty("thresholdA2")), 
				Double.parseDouble(props.getProperty("thresholdA4")),
				Double.parseDouble(props.getProperty("thresholdLoadBehaviour")), 
				Double.parseDouble(props.getProperty("thresholdP1")), 
				Double.parseDouble(props.getProperty("thresholdP2")), 
				Double.parseDouble(props.getProperty("thresholdP4")), 
				Integer.parseInt(props.getProperty("gurobiTimeLimit")), 
				Double.parseDouble(props.getProperty("correlationThreshold")),
				Double.parseDouble(props.getProperty("lowerThreshold")), 
				Double.parseDouble(props.getProperty("upperThreshold")),
				Double.parseDouble(props.getProperty("upperCriticalThreshold")),
				Double.parseDouble(props.getProperty("thresholdGurobiPMs"))
				);
		
	}
	
	/**
	 * For the first run this method is called since autoadjust has not been done before.
	 * 
	 * @param defaultFrequency Current value for the frequency.
	 * @param defaultA Current value for A-parameter.
	 * @param defaultP Current value for P-parameter.
	 */
	private void setDefaults(String defaultFrequency, String defaultA, String defaultP) {
		
		// frequency
		if(defaultFrequency.equals("F1")) {
			this.frequencyParameter = FrequencyParameter.F1;
			this.freq = f1;
		}				
		else {
			this.frequencyParameter = FrequencyParameter.F2;
			this.freq = f2;
		}
		
		// each time the frequency is changed we have to update the subscription
		updateFrequency(freq);
		
		// analyzer
		switch(defaultA) {
		case "A1":
			this.analyzeParameter = AnalyzeParameter.A1;
			break;
		case "A2":
			this.analyzeParameter = AnalyzeParameter.A2;
			break;
		case "A3":
			this.analyzeParameter = AnalyzeParameter.A3;
			break;
		case "A4":
			this.analyzeParameter = AnalyzeParameter.A4;
			break;
		default:
			Logger.getGlobal().info("unknown analyze parameter, A1 is used.");
			this.analyzeParameter = AnalyzeParameter.A1;
			break;
		}
		
		// planner
		switch(defaultP) {
		case "P1":
			this.planningParameter = PlanningParameter.P1; 
			break;
		case "P2":
			this.planningParameter = PlanningParameter.P2; 
			break;
		case "P3":
			this.planningParameter = PlanningParameter.P3; 
			break;
		case "P4":
			this.planningParameter = PlanningParameter.P4; 
			break;
		default:
			Logger.getGlobal().info("unknown planning parameter, P1 is used.");
			this.planningParameter = PlanningParameter.P1; 
			break;
		}
		
	}
	
	/**
	 * 
	 * @return The current frequency in milliseconds.
	 */
	public long getCurrentFrequency() {
		return freq;
	}
	
	/**
	 * 
	 * @return The used instance of the Monitor.
	 */
	public Monitor getMonitor() {
		return monitor;
	}
	
	/**
	 * 
	 * @return The used instance of the Analyzer.
	 */
	public Analyzer getAnalyzer() {
		return analyzer;
	}
	
	/**
	 * 
	 * @return The used instance of the Planner.
	 */
	public Planner getPlanner() {
		return planner;
	}
	
	/**
	 * 
	 * @return The used instance of the Executer.
	 */
	public Executer getExecuter() {
		return executer;
	}
	
	/**
	 * 
	 * @return The currently used A-parameter (note that this parameter can change during the same 
	 * iteration after autoadjust is used).
	 */
	public AnalyzeParameter getAnalyzeParameter() {
		return analyzeParameter;
	}
	
	/**
	 * 
	 * @return The currently used P-parameter (note that this parameter can change during the same 
	 * iteration after autoadjust is used).
	 */
	public PlanningParameter getPlanningParameter() {
		return planningParameter;
	}
	
	/**
	 * 
	 * @return The currently used F-parameter (note that this parameter can change during the same 
	 * iteration after autoadjust is used).
	 */
	public FrequencyParameter getFrequencyParameter() {
		return frequencyParameter;
	}
	
	/**
	 * 
	 * @return Determines whether autoadjust or default values are used.
	 */
	public boolean isAutoAdjustUsed() {
		return !useDefaults;
	}

}
