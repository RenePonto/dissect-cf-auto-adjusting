package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.actions;

import java.util.List;
import java.util.logging.Logger;

import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VMManager.VMManagementException;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelPM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelVM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AACHelper;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine.ResourceAllocation;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine.State;
import hu.mta.sztaki.lpds.cloud.simulator.io.NetworkNode.NetworkException;
import hu.mta.sztaki.lpds.cloud.simulator.io.Repository;

/**
 * This class stores actions, which need to start a PM in the simulator.
 */
public class ResumeVMAction extends Action implements VirtualMachine.StateChange {
	
	/** the future allocation of this VM */
	protected ResourceAllocation ra;
	
	/** reference to the ModelVM */
	protected ModelVM toResume;
	
	/** reference to the new host */
	protected ModelPM newHost;
	
	/** reference to the source repository, where the suspended tasks are */
	protected Repository vaSource;
	
	/** determines whether this is a normal resume action or part of a migration */
	protected boolean suspendAndResume = false;

	/**
	 * Standard constructor with all necessary elements to do a VMResume.
	 * 
	 * @param id The id of this action.
	 * @param vm The ModelVM that shall be resumed.
	 * @param newHost The future host of this VM.
	 * @param vaSource The repository containing the suspended tasks of the VM.
	 * @param suspendAndResume Determines whether this is a normal resume action or part of a migration.
	 */
	public ResumeVMAction(int id, ModelVM vm, ModelPM newHost, Repository vaSource, boolean suspendAndResume) {
		super(id);
		toResume = vm;
		this.newHost = newHost;
		this.vaSource = vaSource;
		this.suspendAndResume = suspendAndResume;
	}

	@Override
	public void determinePredecessors(List<Action> actions) {
				
		for(Action action : actions) {
			
			// if there is a startPMAction for the new host to resume the given VM, let it be done first
			if(action.getType().equals(Type.PMSTART)){
				if((((StartPMAction) action).pmToStart.equals(newHost))) {
					this.addPredecessor(action);
				}				
			}
			else {
				// if we replace migrations with suspend/resume, all suspensions have to be done first,
				// otherwise only suspensions of the same VM (if that occurs)
				if(!suspendAndResume) {
					if(action.getType().equals(Type.SUSPENSION) && (((SuspensionAction) action).getModelVM().equals(toResume))) {
						this.addPredecessor(action);
					}
				}
				else {
					if(action.getType().equals(Type.SUSPENSION)) {
						this.addPredecessor(action);
					}
				}
			}
			
		}
		
	}

	@Override
	public void execute() {
		Logger.getGlobal().info("Executing at " + Timed.getFireCount() + ": " + toString());
		
		VirtualMachine vm = toResume.getVM();
		PhysicalMachine host = newHost.getPM();
		ResourceConstraints toAllocate = vm.getPrevRes();
		
		if(newHost.getPM().getState() != State.RUNNING) {
			Logger.getGlobal().info("Chosen PM as host is not running -> there is nothing to do");
			AACHelper.failedResumes++;
			finished();
		}
		else if(!vm.getState().equals(VirtualMachine.State.SUSPENDED)) {
			Logger.getGlobal().info("VM is not suspended -> there is nothing to do");
			AACHelper.failedResumes++;
			finished();
		}
		else if(!newHost.getPM().isHostableRequest(vm.getPrevRes())) {
			Logger.getGlobal().info("Target host cannot resume VM -> there is nothing to do");
			AACHelper.failedResumes++;
			finished();
		}
		else {
			vm.subscribeStateChange(this);
			try {
				ra = host.allocateResources(toAllocate, 
						true, PhysicalMachine.migrationAllocLen);
				if (ra == null) {
					// save the previous host PM
					vm.setPreviousHostPM(toResume.getInitialHost().getPM());					
					
					AACHelper.failedResumes++;
					vm.unsubscribeStateChange(this);
					
					Logger.getGlobal().info("Resuming was not successful of VM " + vm.hashCode());
					Logger.getGlobal().info("\n" + "Needed resources: " + toAllocate + ", free resources: " + host.freeCapacities + 
							"\n" + "previous host: " + vm.getPreviousHostPM().hashCode() + ", previous host free resources: " + 
							vm.getPreviousHostPM().freeCapacities + "\n");
					
					// TODO debug: try to resume the VM elsewhere maybe?
//					ra = toResume.getInitialHost().getPM().allocateResources(toAllocate, 
//							true, PhysicalMachine.migrationAllocLen);
//					if(ra == null) {
//						// save the previous host PM
//						vm.setPreviousHostPM(toResume.getInitialHost().getPM());					
//						
//						AACHelper.failedResumes++;
//						vm.unsubscribeStateChange(this);
//						
//						Logger.getGlobal().info("Resuming was not successful of VM " + vm.hashCode());
//						Logger.getGlobal().info("\n" + "Needed resources: " + toAllocate + ", free resources: " + host.freeCapacities + 
//								"\n" + "previous host: " + vm.getPreviousHostPM().hashCode() + ", previous host free resources: " + 
//								vm.getPreviousHostPM().freeCapacities + "\n");
//					}
				}
				else {
					vm.setResourceAllocation(ra);
					vm.resume(host);
					
					// increase the counter
					AACHelper.resumeCounter++;
					AACHelper.resumes++;
					
					// stop the timer and set
					long duration = vm.stopTimerAndReturn();
					AACHelper.addUpSuspendedTime(duration);
				}
				
//				Logger.getGlobal().info("Resuming was successful: " + toString());
			} catch (VMManagementException e) {
				AACHelper.failedResumes++;
				vm.unsubscribeStateChange(this);
				
				Logger.getGlobal().info("Resuming was not successful \n" + e);
				e.printStackTrace();
			} catch (NetworkException e) {
				AACHelper.failedResumes++;				
				vm.unsubscribeStateChange(this);
				
				Logger.getGlobal().info("Resuming was not successful \n" + e);
				e.printStackTrace();
			} 
		}
				
	}
	
	/**
	 * 
	 * @return Reference to the model of the PM, which contains the VM after migrating it
	 */
	public ModelPM getTarget(){
		return newHost;
	}
	
	/**
	 * 
	 * @return Reference to the model of the VM, which needs be migrated
	 */
	public ModelVM getVm(){
		return toResume;
	}

	@Override
	public Type getType() {
		return Type.VMRESUME;
	}

	@Override
	public String toString() {
		return "Action: " + getType() + "  : VM " + toResume.getVM().hashCode() + " to PM " + newHost.getPM().hashCode();
	}
	
	/**
	 * The stateChanged-logic, if the VM changes its state to RUNNING after migrating,
	 * then it do not has to be observed any longer.
	 */
	@Override
	public void stateChanged(VirtualMachine vm, VirtualMachine.State oldState, VirtualMachine.State newState) {
		if(newState != VirtualMachine.State.SUSPENDED){
			
			vm.unsubscribeStateChange(this);
			//Logger.getGlobal().info("Resume action finished");
			//Logger.getGlobal().info("Finished at "+Timed.getFireCount()+": "+toString()+", hash="+Integer.toHexString(System.identityHashCode(this)));
			finished();
		}
	}

}
