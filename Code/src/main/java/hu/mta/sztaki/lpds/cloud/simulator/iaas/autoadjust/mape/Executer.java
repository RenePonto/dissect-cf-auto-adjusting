package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelPM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelVM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelPM.State;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.actions.Action;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.actions.MigrationAction;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.actions.ResumeVMAction;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.actions.StartPMAction;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.actions.StopPMAction;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.actions.SuspensionAction;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.Plan;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.consolidation.SimpleConsolidator;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.pmscheduling.IControllablePmScheduler;

/**
 * This class is used together with the AutoAdjustingConsolidator. Its function
 * is to execute the calculated plan to improve the current situation in the cloud.
 * 
 * This class uses a plan to execute necessary changes.
 */
public class Executer {
	
	/** The IaaSService to work with */
	private IaaSService toConsolidate;
	/** determines whether the execution has finished */
	private boolean hasFinished;	
	private boolean suspendAndResume;
	
	/** time measurement */
	private long duration;
	
	/** the created actions, used for debugging */
	private List<Action> actions;
	private int amountMigrationActions, amountResumeActions, amountStartPMActions, 
		amountStopPMActions, amountSuspendVMActions;
	
	
	@SuppressWarnings("unused")
	private Executer() {
		
	}
	
	/**
	 * Default constructor. Has to be empty, because one instance is intended to
	 * be alive as long as the instance of the consolidator.
	 * 
	 * The inputs and possible parameters are set before each usage separately.
	 */
	public Executer(boolean suspendAndResume) {		
		this.suspendAndResume = suspendAndResume;
	}
	
	/**
	 * Executes the found plan of the planner, which uses a model to find the best solution
	 * according to the chosen parameter for the planner. Additionally, sets the IaaSService 
	 * to allow doing changes.
	 * 
	 * @param plan The found plan to execute.
	 * @param toConsolidate The IasSService to consolidate.
	 */
	public void execute(Plan plan, IaaSService toConsolidate) {
		
		Logger.getGlobal().info("started to execute");
		
		long startTime = Calendar.getInstance().getTimeInMillis();
		
		this.toConsolidate = toConsolidate;
		hasFinished = false;
		
		// debug
//		Logger.getGlobal().info(plan.toString());
//		Logger.getGlobal().info(plan.getChangesBetweenBins());
		
		// create all actions and implement the best solution
		resetActionCounter();
		actions = modelDiff(plan);
		createGraph(actions);
		performActions(actions);
		
		Logger.getGlobal().info(amountOfCreatedActionsToString());
		
		hasFinished = true;
		
		// determine the durations
		long endTime = Calendar.getInstance().getTimeInMillis();
		duration = endTime - startTime;
	}
	
	/**
	 * Helper method to reset the action counter, called before creating new actions.
	 */
	private void resetActionCounter() {
		this.amountMigrationActions = 0;
		this.amountResumeActions = 0;
		this.amountStartPMActions = 0;
		this.amountStopPMActions = 0;
		this.amountSuspendVMActions = 0;
	}
	
	/**
	 * This method is to be used after doing a consolidation run. It compares the determined amount of actions
	 * with the actions done by the simulator. If the amounts are different, a log entry is printed.
	 */
	public void compareActionsWithChanges() {
		Logger.getGlobal().info("considered migration actions: " + amountMigrationActions + 
				", done migration actions: " + AACHelper.migrations + ", failed: " + AACHelper.failedMigrations);
		
		if(amountResumeActions != AACHelper.resumes) {
			Logger.getGlobal().info("considered resume actions: " + amountResumeActions + 
					", done resume actions: " + AACHelper.resumes + ", failed: " + AACHelper.failedResumes);
		}
		if(amountStartPMActions != AACHelper.startPM) {
			Logger.getGlobal().info("considered startPM actions: " + amountStartPMActions + 
					", done startPM actions: " + AACHelper.startPM + ", failed: " + AACHelper.failedStartPM);
		}
		if(amountStopPMActions != AACHelper.stopPM) {
			Logger.getGlobal().info("considered stopPM actions: " + amountStopPMActions + 
					", done stopPM actions: " + AACHelper.stopPM + ", failed: " + AACHelper.failedStopPM);
		}
		if(amountSuspendVMActions != AACHelper.suspensions) {
			Logger.getGlobal().info("considered suspension actions: " + amountSuspendVMActions + 
					", done suspension actions: " + AACHelper.suspensions + ", failed: " + AACHelper.failedSuspensions);
		}
	}
	
	/**
	 * Creates the actions-list with migration-/start- and shutdown-actions.
	 * 
	 * @return The list with all the actions.
	 */
	private List<Action> modelDiff(Plan plan) {
		// If we have an externally controllable PM scheduler, then we also create start-up and 
		// shut-down actions, otherwise only migration actions
		IControllablePmScheduler controllablePmScheduler = null;
		if(toConsolidate.pmcontroller instanceof IControllablePmScheduler) {
			controllablePmScheduler = (IControllablePmScheduler) toConsolidate.pmcontroller;
		}
		
		// handle the differences found as solutions		
		List<Action> actions = new ArrayList<>();
		int i = 0;
		for(ModelPM bin : plan.getImprovedPMs()) {
			if(controllablePmScheduler != null) {
				
				// current situation of this pm
//				Logger.getGlobal().info(bin.toString() + "\n" + bin.getPM().toString());
				
				if(bin.getState() != State.EMPTY_OFF && !bin.getPM().isRunning() && bin.isHostingVMs()) {
					actions.add(new StartPMAction(i++, bin, controllablePmScheduler));
					amountStartPMActions++;
//					Logger.getGlobal().info("Added new StartPMAction for PM " + bin.hashCode());
				}
				if(bin.getState() == State.EMPTY_OFF && bin.getPM().isRunning() && !bin.isHostingVMs()) {
					actions.add(new StopPMAction(i++, bin, controllablePmScheduler));
					amountStopPMActions++;
//					Logger.getGlobal().info("Added new StopPMAction for PM " + bin.hashCode());
				}
				else if(bin.getState() == State.EMPTY_RUNNING && bin.getPM().isRunning() && !bin.isHostingVMs()) {
					actions.add(new StopPMAction(i++, bin, controllablePmScheduler));
					amountStopPMActions++;
//					Logger.getGlobal().info("Added new StopPMAction for PM " + bin.hashCode());
				}
			}
		}
		for(ModelVM item : plan.getImprovedItems()) {
			if(controllablePmScheduler != null) {
				// previously running, now suspended
				if(item.isSuspended() && item.gethostPM() == null ) {
					
					// check if the VM was suspended before
					if(item.getVM().getState().equals(VirtualMachine.State.SUSPENDED)) {
						// do nothing then
					}
					else {
						actions.add(new SuspensionAction(i++, item));
						amountSuspendVMActions++;
//						Logger.getGlobal().info("Added new SuspensionAction for VM " + item.hashCode());
					}					
					
				}
				// previously suspended, now resumed
				if(!item.isSuspended() && item.getInitialHost() == null && item.gethostPM() != null) {
					actions.add(new ResumeVMAction(i++, item, item.gethostPM(), toConsolidate.repositories.get(0), 
							suspendAndResume));
					amountResumeActions++;
//					Logger.getGlobal().info("Added new ResumeAction for VM " + item.hashCode());
				}
			}
			// if we suspend or resume a VM, there should no MigrationAction be created
			if(item.gethostPM() != null && item.getInitialHost() != null) {
				// case: unplaced VM
//				if(item.getInitialHost() == null) {					
//					
//					try {
//						item.getVM().switchoff(true);
//						ResourceAllocation ra = item.gethostPM().getPM().allocateResources(item.getResources(), 
//								true, PhysicalMachine.migrationAllocLen);
//						if (ra == null) {
//							throw new VMManagementException("Not enough resources on target host for deploying unplaced "
//									+ "VM " + item.toShortString());
//						}
//						item.getVM().setResourceAllocation(ra);
//					} catch (VMManagementException e) {
//						e.printStackTrace();
//					}
//				}
				/*else*/ if(item.gethostPM() != item.getInitialHost()) {
					
					// check whether we shall not do migrations and instead suspends/resumes
					if(suspendAndResume) {
//						actions.add(new MigrationAction(i++, item.getInitialHost(), item.gethostPM(), item, true));
						actions.add(new SuspensionAction(i++, item));
						actions.add(new ResumeVMAction(i++, item, item.gethostPM(), toConsolidate.repositories.get(0), 
								suspendAndResume));
						
						amountSuspendVMActions++;
						amountResumeActions++;
					}
					else {
						actions.add(new MigrationAction(i++, item.getInitialHost(), item.gethostPM(), item));
						SimpleConsolidator.migrationCount++;
						amountMigrationActions++;
					}					
//					Logger.getGlobal().info("Added new MigrationAction for VM " + item.hashCode());
				}
			}
			
		}
		
		return actions;
	}
	
	/**
	 * Debugging, creates a String containing the amount of each action that has been made 
	 * for execution.
	 * 
	 * @return
	 */
	public String amountOfCreatedActionsToString() {
		
		if( (amountMigrationActions == 0) && (amountResumeActions == 0) && (amountStartPMActions == 0)
				&& (amountStopPMActions == 0) && (amountSuspendVMActions == 0) ) {
			return "No actions were created.";
		}
		else {
			String result = "\n";
			// get all the actions made
			result += "Amount of MigrationActions: " + amountMigrationActions + "\n";
			result += "Amount of ResumeVMActions: " + amountResumeActions + "\n";
			result += "Amount of StartPMActions: " + amountStartPMActions + "\n";
			result += "Amount of StopPMActions: " + amountStopPMActions + "\n";
			result += "Amount of SuspendVMActions: " + amountSuspendVMActions;
			
			return result;
		}		
		
	}
	
	/**
	 * Determines the dependencies between the actions.
	 * 
	 * @param actions The action-list with all changes that have to be done inside the simulator.
	 */
	private void createGraph(List<Action> actions) {
		for (Action action : actions) {
			action.determinePredecessors(actions);
		}
	}

	/**
	 * Checks if there are any predecessors of the actual action. If not, its
	 * execute()-method is called.
	 * 
	 * @param actions The action-list with all changes that have to be done inside the simulator.
	 */
	private void performActions(List<Action> actions) {
//		List<Action> laterToMig = new ArrayList<>();
//		Map<VirtualMachine, PhysicalMachine> vmToNewHostMapping = new HashMap<>();
		Collections.sort(actions);
		
		for (Action action : actions) {
			if (action.getPredecessors().isEmpty()) {
				action.execute();
			}
		}
	}
	
	/**
	 * Checks whether there are circular dependencies between PMs, that want to migrate VMs on each other.
	 * 
	 * @param actions
	 * 
	 * @return
	 */
	@SuppressWarnings("unused")
	private boolean checkForCircularDependency(List<Action> actions) {
		for(Action action: actions) {
			for(Action toCheck: action.getPredecessors()) {
				// now we have to check whether this action has the previous also as a predecessor
				if(toCheck.getPredecessors().contains(action)) {
					return true;
				}
				
			}
		}
		
		return false;
	}
	
	/**
	 * Getter for the duration of the current execution run. Is intended to be called 
	 * after the execution is done.
	 * 
	 * @return The duration of the execution in milliseconds.
	 */
	public long getDuration() {
		return duration;
	}
	
	/**
	 * Determines whether the Executer has finished.
	 * 
	 * @return True if all operations are done, false otherwise.
	 */
	public boolean isFinished() {
		return hasFinished;
	}
	
	public List<Action> getActions() {
		return actions;
	}
	
	public List<Action> getPendingActions() {
		List<Action> undoneActions = new ArrayList<>();
		
		for(Action action: actions) {
			if(!action.isFinished())
				undoneActions.add(action);
		}
		
		return undoneActions;
	}
	
	public int getAmountOfStartPMActions() {
		return amountStartPMActions;
	}
	
	public int getAmountOfStopPMActions() {
		return amountStopPMActions;
	}

}
