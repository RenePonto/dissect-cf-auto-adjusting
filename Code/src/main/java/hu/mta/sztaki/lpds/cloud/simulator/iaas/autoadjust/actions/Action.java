package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import hu.mta.sztaki.lpds.cloud.simulator.Timed;

/**
 * This abstract class stores the actions that need to be done inside the simulator.
 */
public abstract class Action implements Comparable<Action> {

	/**
	 * Classifies actions and sets values for the priority of execution of each action type.
	 * The lower the value the higher the priority is regarding the order of execution.
	 */
	public static enum Type {
		/**
		 * The current action needs to start another PM.
		 */
		PMSTART(1),
		
		/**
		 * The current action needs to migrate a VM.
		 */
		MIGRATION(3),
		
		/**
		 * The current action needs to shut down a PM.
		 */
		PMSHUTDOWN(5),
		
		/**
		 * The current action suspends a VM.
		 */
		SUSPENSION(2),
		
		/**
		 * The current action resumes a VM.
		 */
		VMRESUME(4);
		
		private final int value;
		
		private Type(int value) {
			this.value = value;
		}
	}

	protected int id;
	//List of actions, which need to be completed before the action starts
	protected List<Action> predecessors;
	//List of actions, which need to start after completion of this one
	protected List<Action> successors;
	
	protected boolean finished = false;

	/**
	 * Constructor. Instantiates the empty lists for its predecessors and successors.
	 * @param id
	 */
	public Action(int id){
		this.id = id;
		predecessors=new ArrayList<>();
		successors=new ArrayList<>();
	}

	/**
	 * Adds v as a predecessor of this action AND also this action as a 
	 * successor of v.
	 */
	public void addPredecessor(Action v){
		predecessors.add(v);
		v.addSuccessor(this);
	}

	/**
	 * Removes v as a predecessor of this action AND also this action as a 
	 * successor of v.
	 */
	public void removePredecessor(Action v){
		predecessors.remove(v);
		//v.removeSuccessor(this); //commented out to avoid concurrent modification exception
	}
	
	public List<Action> getPredecessors(){
		return predecessors;
	}

	public void addSuccessor(Action v){
		successors.add(v);
	}

	public void removeSuccessor(Action v){
		successors.remove(v);
	}

	public List<Action> getSuccessors(){
		return successors;
	}

	/**
	 * This method determines the predecessors of this action, based on its type.
	 */
	public abstract void determinePredecessors(List<Action> actions);
	
	/**
	 * This method is individualized by every subclass and performs the actions.
	 */
	public abstract void execute();

	/**
	 * This method is to be called when the action has been finished.
	 */
	public void finished() {
		finished = true;
		Logger.getGlobal().info("Finished at "+Timed.getFireCount()+": "+toString());
		for(Action a : successors) {
			a.removePredecessor(this);
			if(a.getPredecessors().isEmpty())
				a.execute();
		}
	}

	/**
	 * This method returns the type of the action.
	 */
	public abstract Type getType();

	/**
	 * The toString()-method, which has to be implemented individually by each action type.
	 */
	public abstract String toString();
	
	/**
	 * Offers a comparator to another action. If this action is more
	 * urgent to compute, the function returns 1. Otherwise -1 is returned.
	 * If both actions are of the same type, 0 is returned.
	 */
	public int compareTo(Action o) {
		if (this.getType().value > o.getType().value) {
			return 1;
		} 
		else if (this.getType().value < o.getType().value) {
			return -1;
		} 
		else
			return 0;
	}
	
	public boolean isFinished() {
		return finished;
	}
	
}
