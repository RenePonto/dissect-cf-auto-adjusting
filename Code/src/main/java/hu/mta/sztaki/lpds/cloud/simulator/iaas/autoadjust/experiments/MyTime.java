package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.experiments;

/**
 * Helper to convert seconds to ticks and vice versa.
 */
public class MyTime {
	private static final double tickPerSecond = 1000;

	/**
	 * Convert an amount of ticks to seconds.
	 * 
	 * @param ticks The value to be converted.
	 * 
	 * @return The same value in seconds.
	 */
	public static double tick2sec(double ticks) {
		return ticks/tickPerSecond;
	}

	/**
	 * Convert an amount of seconds to ticks.
	 * 
	 * @param seconds The value to be converted.
	 * 
	 * @return The same value in ticks.
	 */
	public static long sec2tick(double seconds) {
		long result = (long)(seconds * tickPerSecond);
		if(result < 1)
			result = 1;
		return result;
	}

	public static double mips2instrpertick(double mips) {
		return mips * 1000000 / tickPerSecond;
	}
}
