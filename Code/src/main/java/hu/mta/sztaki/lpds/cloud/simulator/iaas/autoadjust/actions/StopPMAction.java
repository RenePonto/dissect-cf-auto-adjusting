package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.actions;

import java.util.List;
import java.util.logging.Logger;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine.State;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelPM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AACHelper;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.pmscheduling.IControllablePmScheduler;

/**
 * This class stores actions, which need to shut down a PM in the simulator.
 */
public class StopPMAction extends Action implements PhysicalMachine.StateChangeListener {

	//Reference to the model of the PM, which needs to shut down
	protected ModelPM pmToShutDown;

	/** PM scheduler */
	protected IControllablePmScheduler pmScheduler;

	/**
	 * Constructor for an action to shut a PM down.
	 * @param id The ID of this action.
	 * @param pmToShutDown The reference to the PM inside the simulator to get shut down.
	 * @param pmScheduler Reference to the PM scheduler of the IaaS service
	 */
	public StopPMAction(int id, ModelPM pmToShutDown, IControllablePmScheduler pmScheduler) {
		super(id);
		this.pmToShutDown = pmToShutDown;
		this.pmScheduler = pmScheduler;
		//Logger.getGlobal().info("ShutDownAction created");
	}

	public ModelPM getPmToShutDown(){
		return pmToShutDown;
	}

	/**
	 * This method determines the predecessors of this action. A predecessor of 
	 * a shut-down action is a migration from this PM.
	 */
	@Override
	public void determinePredecessors(List<Action> actions) {		
		//looking for migrations with this PM as source
		for(Action action : actions) {
			if(action.getType().equals(Type.MIGRATION)){
				if((((MigrationAction) action).getTarget()) == pmToShutDown) {
					Logger.getGlobal().info("PM to shut down is target of a migration, skipping shutdown");
					AACHelper.failedStopPM++;
					finished();
				}
				
				// in case of suspensions we have null as previous PM
				if((((MigrationAction) action).getSource()) == null) {
					this.addPredecessor(action);
				}
				else if((((MigrationAction) action).getSource()).equals(pmToShutDown)){
					this.addPredecessor(action);
				}
			}
			// check for VMs that should be resumed on this PM
//			if(action.getType().equals(Type.VMRESUME)) {
//				if(((ResumeVMAction) action).getTarget() == pmToShutDown) {
//					Logger.getGlobal().info("PM to shut down is target of a resume, skipping shutdown");
//					AACHelper.failedStopPM++;
//					finished();
//				}
//				
//			}
			// we have to check if VMs of this PM are going to be suspended
			if(action.getType().equals(Type.SUSPENSION)) {
				if(pmToShutDown.getVMs().contains(((SuspensionAction)action).getModelVM())) {
					this.addPredecessor(action);
				}
				
			}
			
			// debug: only shut down PMs when all resumes have been successfully done
			if(action.getType().equals(Type.VMRESUME)) {
				this.addPredecessor(action);
			}
			
		}
	}

	@Override
	public Type getType() {
		return Type.PMSHUTDOWN;
	}

	@Override
	public String toString() {
		return "Action: "+getType()+"  : PM " + getPmToShutDown().getPM().hashCode();
	}

	/**
	 * This method shuts the PM inside the simulator down.
	 */
	@Override
	public void execute() {
//		Logger.getGlobal().info("Executing at "+Timed.getFireCount()+": "+toString());
		PhysicalMachine pm = this.getPmToShutDown().getPM();
		if(pm.isHostingVMs()) {
			Logger.getGlobal().info("PM not empty -> nothing to do");
			AACHelper.failedStopPM++;
			finished();
		}
		// the PM is already off or about to be shut down
		else if(pm.getState().equals(State.OFF) || pm.getState().equals(State.SWITCHINGOFF)) {
			Logger.getGlobal().info("PM is off or about to stop -> nothing to do");
			AACHelper.failedStopPM++;
			finished();
		}
		else {
			pm.subscribeStateChangeEvents(this);		//observe the PM before turning it off
			pmScheduler.switchOff(pm);
			AACHelper.pmStopCounter++;
			AACHelper.stopPM++;
		}
			
	}
	
	/**
	 * The stateChanged-logic, if the PM which has been started changes its state to OFF,
	 * we can stop observing it.
	 */
	@Override
	public void stateChanged(PhysicalMachine pm, hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine.State oldState,
			hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine.State newState) {
		if(newState.equals(PhysicalMachine.State.OFF)){
			pm.unsubscribeStateChangeEvents(this);			
			finished();
		}
	}

}