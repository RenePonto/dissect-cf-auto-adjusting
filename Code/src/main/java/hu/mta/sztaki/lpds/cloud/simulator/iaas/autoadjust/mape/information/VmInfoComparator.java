package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information;

import java.util.Comparator;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.VMInformation;

public class VmInfoComparator implements Comparator<VMInformation> {

	@Override
	public int compare(VMInformation o1, VMInformation o2) {
		if(o1.getVM().hashCode() < o2.getVM().hashCode()) {
			return -1;
		}
		else if(o1.getVM().hashCode() == o2.getVM().hashCode()) {
			return 0;
		}
		else {
			return 1;
		}
	}

}
