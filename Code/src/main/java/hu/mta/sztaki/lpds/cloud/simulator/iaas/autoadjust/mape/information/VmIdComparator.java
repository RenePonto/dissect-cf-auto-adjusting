package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information;

import java.util.Comparator;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;

public class VmIdComparator implements Comparator<VirtualMachine> {
	
	@Override
	public int compare(VirtualMachine o1, VirtualMachine o2) {
		if(o1.hashCode() < o2.hashCode()) {
			return -1;
		}
		else if(o1.hashCode() == o2.hashCode()) {
			return 0;
		}
		else {
			return 1;
		}
		
	}

}
