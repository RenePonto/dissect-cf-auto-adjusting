package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.VmInfoComparator;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;

/**
 * Stores all necessary information about a given PM including the actual PM,
 * the current Load, the hosted VMs and the previous measures of the load of this
 * PM. This object is created by Monitor and used by Analyzer and the AutoAdjusting-
 * Consolidator.
 */
public class PMInformation {
	
	/** current objects */
	private PhysicalMachine pm;
	private ResourceConstraints currentLoad;
	
	/** historical objects */
	private int amountOfPrevious;
	private List<ResourceConstraints> lastMeasures;
	
	/** VMInformation for hosted VMs */
	private List<VMInformation> vmsInfo;
	
	/**
	 * Constructor for one PMInformation object. Needs the PM and the 
	 * parameter for defining the amount of kept values of the past as
	 * input.
	 * 
	 * @param pm The PhysicalMachine the information is based on.
	 * @param currentLoad The resources of this PM.
	 * @param hostedVMs The VMs hosted by this PM as a list.
	 * @param amountOfPrevious Sets the amount of saved historical values.
	 */
	public PMInformation(PhysicalMachine pm, ResourceConstraints currentLoad, int amountOfPrevious) {
		lastMeasures = new ArrayList<ResourceConstraints>();
		vmsInfo = new ArrayList<VMInformation>();
		
		this.pm = pm;
		this.currentLoad = currentLoad;
		this.amountOfPrevious = amountOfPrevious;		
	}
	
	/**
	 * Adds a new measure to this information as the current load. The previous
	 * current load is moved to a list containing the previous values. If the amount
	 * of elements inside the list is longer than previously defined by 'n', the oldest
	 * values is removed.
	 * 
	 * @param newLoad The new load to be added.
	 */
	public void addMeasure(ResourceConstraints newLoad) {
		lastMeasures.add(0, currentLoad);
		
		// remove the last measure of the list if the limit is reached
		while(lastMeasures.size() > amountOfPrevious) {
			lastMeasures.remove(lastMeasures.size()-1);
		}
		currentLoad = newLoad;
	}
	
	public int getAmountOfPrevious() {
		return amountOfPrevious;
	}
	
	/**
	 * Get the PM this information is based on.
	 * 
	 * @return The PM this information is based on.
	 */
	public PhysicalMachine getPM() {
		return pm;
	}
	
	/**
	 * Get the current load of the PM this information is based on.
	 * 
	 * @return The current load of the PM this information is based on.
	 */
	public ResourceConstraints getLoad() {
		return currentLoad;
	}
	
	/**
	 * Get the hosted VMs currently running on the PM this information is based on.
	 * 
	 * @return The hosted VMs currently running on the PM this information is based on.
	 */
	public List<VirtualMachine> getVMs() {
		List<VirtualMachine> results = new ArrayList<>();

		// sort the list
		Collections.sort(vmsInfo, new VmInfoComparator());
		
		for(VMInformation info: vmsInfo) {
			results.add(info.getVM());
		}
		return results;
//		return new ArrayList<VirtualMachine>(pm.listVMs());
	}
	
	/**
	 * Get the historical values of this information.
	 * 
	 * @return The previous measures.
	 */
	public List<ResourceConstraints> getLastValues() {
		return lastMeasures;
	}
	
	/**
	 * Checks if this information contains any previous measures.
	 * 
	 * @return True if there is are at least two measures, false otherwise.
	 */
	public boolean areLastValues() {
		return !lastMeasures.isEmpty();
	}
	
	/**
	 * Getter for the current VMInformation.
	 * 
	 * @return The current VMInformation.
	 */
	public List<VMInformation> getVMInfo() {
		return vmsInfo;
	}
	
	/**
	 * Replace the current VMinfos.
	 * 
	 * @param vmsInfo
	 */
	public void replaceInfo(List<VMInformation> vmsInfo) {
		this.vmsInfo.clear();
		this.vmsInfo = vmsInfo;
		
		// sort the list
		Collections.sort(vmsInfo, new VmInfoComparator());
	}
	
	public String toString() {
		String result = "PMinfo for PM " + pm.hashCode() + " " + currentLoad.toString() + " VMInfos: ";
		for(VMInformation current: vmsInfo) {
			result += current.toString();
		}		
		return result;
	}

}
