package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Logger;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.PMInformation;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.VMInformation;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.AlterableResourceConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;

/**
 * Contains the output of one monitoring run, which contains all existing PMs
 * and VMs as well as their mapping containing each other. Additionally, the
 * total load is calculated and it is marked whether there are some suspended VMs
 * or overloaded PMs.
 * 
 * Note that the previous information of each PM and VM is saved inside each
 * information object, respectively. 
 */
public class MonitorInformation {
	
	private double highestProcessingPower = 0;
	
	/** all used physical machines */
	private List<PMInformation> currentPMInformation;
	/** all virtual machines */
	private List<VMInformation> currentVMInformation;
	
	/** current mapping of VMs to PMs */
	private SortedMap<PhysicalMachine, List<VirtualMachine>> mapping;
	
	/** current total load*/
	private AlterableResourceConstraints totalLoad;
	
	/** previous total load*/
	private AlterableResourceConstraints prevTotalLoad;
	/** previous total load with the upper threshold */
	private AlterableResourceConstraints upperPrevious;
	/** previous total load with the lower threshold */
	private AlterableResourceConstraints lowerPrevious;
	
	/** determines whether there are overloaded PMs */
	private boolean overloaded;
	
	/** determines whether there are suspended VMs */
	private boolean suspended;
	
	/** the amount of previous value for each PM and VM to be saved */
	private int amountOfPrevious = 0;
	
	/** contains the unplaced VMs */
	private Map<VirtualMachine, ResourceConstraints> unplacedVMs = new HashMap<>();
	
	private double thresholdLoadBehaviour;
	
	/**
	 * This constructor expects an integer, which is passed to the PMInformation-
	 * objects and VMInformation-objects at their creation. It contains one PMInformation
	 * or VMInformation for each PM or VM in the IaaS, respectively. Note that
	 * one entry can contain more than one value for its load. This represents the different
	 * points of time of measuring the load.
	 */
	public MonitorInformation(int amountOfPrevious, double thresholdLoadBehaviour) {			
		currentPMInformation = new ArrayList<PMInformation>();
		currentVMInformation = new ArrayList<VMInformation>();
		
		mapping = new TreeMap<PhysicalMachine, List<VirtualMachine>>();
		
		overloaded = false;
		suspended = false;
		this.amountOfPrevious = amountOfPrevious;
		this.thresholdLoadBehaviour = thresholdLoadBehaviour;
	}
	
	public void setUnplacedVMs(Map<VirtualMachine, ResourceConstraints> unplacedVMs) {
		this.unplacedVMs = unplacedVMs;
	}
	
	public Map<VirtualMachine, ResourceConstraints> getUnplacedVMs() {
		return unplacedVMs;
	}

	/**
	 * Determines whether this object contains suspended VMs.
	 * 
	 * @param suspended True, if there are suspended VMs, false otherwise.
	 */
	public void setSuspended(boolean suspended) {
		this.suspended = suspended;
	}
	
	/**
	 * Determines whether this object contains overloaded PMs.
	 * 
	 * @param overloaded True, if there are overloaded PMs, false otherwise.
	 */
	public void setOverloaded(boolean overloaded) {
		this.overloaded = overloaded;
	}
	
	public void setHighestProcessingPower(double highest) {
		this.highestProcessingPower = highest;
	}
	
	public double getHighestProcessingPower() {
		return this.highestProcessingPower;
	}
	
	/**
	 * Adds a new PMInformation with the parameterized information to this MonitorInformation.
	 * If there is an existing PMInformation for the parameterized PM, than it is only updated,
	 * meaning the previous load is pushed to the list containing the previous measures and 
	 * the rest is saved properly. 
	 * 
	 * @param pm The PM to create the information for.
	 * @param currentLoad The currently measured load of this PM.
	 * @param hostedVMs A list containing the hosted VMs on this PM.
	 */
	public PMInformation addPMInformation(PhysicalMachine pm, ResourceConstraints currentLoad, List<VMInformation> vmInfos) {
		
		// check whether the PM exists already
		for(int i = 0; i < currentPMInformation.size(); i++) {
			if(currentPMInformation.get(i).getPM() == pm) {
//				Logger.getGlobal().info("Previously added PM " + pm.hashCode() + 
//						", only updating information, current State: " + pm.getState());
				
				return updatePMInfo(getSpecificPMInformation(pm), currentLoad, vmInfos);				
			}
		}
		
		PMInformation created = new PMInformation(pm, currentLoad, amountOfPrevious);
		
		// only add a new information if there is none for this PM
		currentPMInformation.add(created);
//		Logger.getGlobal().info("Added new PMInformation for PM " + pm.hashCode());
		return created;
	}
	
	/**
	 * Updates the parameterized PMInformation and returns it.
	 * 
	 * @param info
	 * @param currentLoad
	 * @param vmInfos
	 * 
	 * @return
	 */
	private PMInformation updatePMInfo(PMInformation info, ResourceConstraints currentLoad, List<VMInformation> vmInfos) {		
		info.addMeasure(currentLoad);					
		
		// clear previous VMInfos
		info.replaceInfo(vmInfos);					
		
		return info;
	}
	
	/**
	 * Adds a new VMInformation with the parameterized information to this MonitorInformation.
	 * If there is an existing VMInformation for the parameterized VM, than it is only updated,
	 * meaning the previous load is pushed to the list containing the previous measures and 
	 * the rest is saved properly. 
	 * 
	 * @param vm The VM to create the information for.
	 * @param host The host PM of this VM.
	 * @param currentLoad The currently measured load of this VM.
	 */
	public VMInformation addVMInformation(VirtualMachine vm, PhysicalMachine host, 
			ResourceConstraints currentLoad, boolean suspended) {
		
		// set the suspended state
		if(suspended) {
			setSuspended(true);
		}
		
		// check whether the VM exists already
		for(VMInformation current: currentVMInformation) {
			if(current.getVM() == vm) {
//				Logger.getGlobal().info("Previously added VM " + vm.hashCode() + 
//						", only updating information, current State: " + vm.getState());
					
				return updateVMInfo(getSpecificVMInformation(vm), host, currentLoad, suspended);				
			}
		}
		
		// only add a new information if there is none for this VM
		VMInformation info = new VMInformation(vm, host, currentLoad, amountOfPrevious, suspended);
		currentVMInformation.add(info);
		
//		Logger.getGlobal().info("Added new VMInformation for " + vm.getState() + " VM " + vm.hashCode());
		
		return info;
	}
	
	/**
	 * Updates the parameterized VMInformation and returns it.
	 * 
	 * @param info
	 * @param host
	 * @param currentLoad
	 * @param suspended
	 * 
	 * @return
	 */
	private VMInformation updateVMInfo(VMInformation info, PhysicalMachine host, 
			ResourceConstraints currentLoad, boolean suspended) {
		
		info.addLoad(currentLoad);
		info.setHost(host);
		info.setSuspended(suspended);
		
		return info;
		
	}
	
	/**
	 * Calculate the load without resetting it.
	 */
	public AlterableResourceConstraints calculateTotalLoadWithoutReset() {
		
		// TODO this was resetting before and is now commented out, check this
		AlterableResourceConstraints totalLoad = new AlterableResourceConstraints(0.0, highestProcessingPower, 0);
		
		// add up the resources of each active VM
		for(VMInformation info: currentVMInformation) {
			totalLoad.singleAdd(info.getLoad());				
		}
		
		return totalLoad;
	}
	
	/**
	 * Resets the ResourceConstraints and calculates the complete allocated resources.
	 */
	public ResourceConstraints calculateTotalLoad() {
		
		// copy the current value
		if(totalLoad != null) {
			prevTotalLoad = new AlterableResourceConstraints(0.0, highestProcessingPower, 0);
			prevTotalLoad.singleAdd(totalLoad);
		}						
		totalLoad = new AlterableResourceConstraints(0.0, highestProcessingPower, 0);
		
		// add up the resources of each active VM
		for(VMInformation info: currentVMInformation) {
			totalLoad.singleAdd(info.getLoad());				
		}
		
		// calculate the thresholds
		if(prevTotalLoad != null) {
			// calculates the bounds of the previous total load combined with the threshold
			AlterableResourceConstraints previous = new AlterableResourceConstraints(prevTotalLoad);
			previous.multiply(thresholdLoadBehaviour);
			
			// upper bound
			upperPrevious = new AlterableResourceConstraints(prevTotalLoad);
			upperPrevious.singleAdd(previous);
			
			// lower bound
			lowerPrevious = new AlterableResourceConstraints(prevTotalLoad);
			lowerPrevious.subtract(previous);
		}
		
//		logCalculations();		
		
		return totalLoad;
	}
	
	/**
	 * Print all calculations and the toStrings for the PM- and VMInformation.
	 */
	public void logCalculations() {
		String toLog = "\n";
		toLog += "current: " + "[" + totalLoad.getRequiredCPUs() + "|" + totalLoad.getRequiredProcessingPower() + "|" + 
				totalLoad.getRequiredMemory() + "] TPP: " + totalLoad.getTotalProcessingPower() + ";" + "\n";
		if(prevTotalLoad != null) {
			toLog += "previous: " + "[" + prevTotalLoad.getRequiredCPUs() + "|" + prevTotalLoad.getRequiredProcessingPower() + "|" + 
					prevTotalLoad.getRequiredMemory() + "] TPP: " + prevTotalLoad.getTotalProcessingPower() + ";" + "\n";
		}
		else {
			toLog += "no previous load" + "\n";
		}
		
		toLog += "amount of PMs: " + currentPMInformation.size();
		for(PMInformation current: currentPMInformation) {
			toLog += "|| PM " + current.getPM().hashCode() + " load: " + current.getLoad() + " hosted VMs:";
			for(VirtualMachine vm: current.getVMs()) {
				toLog += " " + vm.hashCode();
			}
		}
		toLog += "\n";
		
		toLog += "amount of VMs: " + currentVMInformation.size();
		for(VMInformation current: currentVMInformation) {
			toLog += " " + current.toString();
		}
		toLog += "\n";
		
		Logger.getGlobal().info(toLog);
	}
	
	/**
	 * Determines if there are currently unplaced VMs.
	 * 
	 * @return True if there are currently unplaced VMs, false otherwise.
	 */
	public boolean areUnplacedVMs() {
		return !unplacedVMs.isEmpty();
	}
	
	/**
	 * Determines if there are currently suspended VMs.
	 * 
	 * @return True if there are currently suspended VMs, false otherwise.
	 */
	public boolean areSuspendedVMs() {
		return suspended;
	}
	
	/**
	 * Determines if there are currently overloaded PMs.
	 * 
	 * @return True if there are currently overloaded PMs, false otherwise.
	 */
	public boolean areOverloadedServers() {
		return overloaded;
	}
	
	/**
	 * Get the List containing all saved PMInformation.
	 * 
	 * @return The List containing all saved PMInformation.
	 */
	public List<PMInformation> getPMInformation() {
		return currentPMInformation;
	}
	
	/**
	 * Get the List containing all saved VMInformation for the VMs.
	 * 
	 * @return The List containing all saved VMInformation.
	 */
	public List<VMInformation> getVMInformation() {
		return currentVMInformation;
	}
	
	/**
	 * Get all the currently allocated resources.
	 * 
	 * @return All currently allocated resources added.
	 */
	public ResourceConstraints getTotalLoad() {
		return totalLoad;
	}
	
	/**
	 * Get all the previously allocated resources.
	 * 
	 * @return All previously allocated resources added.
	 */
	public ResourceConstraints getPreviousTotalLoad() {
		return prevTotalLoad;
	}
	
	public ResourceConstraints getUpperPreviousTotalLoad() {
		return upperPrevious;
	}
	
	public ResourceConstraints getLowerPreviousTotalLoad() {
		return lowerPrevious;
	}
	
	/**
	 * Look for a specific PMInformation inside the internal list.
	 * 
	 * @param pm The PhysicalMachine to get the information for.
	 * 
	 * @return The wanted PMInformation if it exists, null otherwise.
	 */
	public PMInformation getSpecificPMInformation(PhysicalMachine pm) {
		PMInformation result = null;
		
		for(int i = 0; i < currentPMInformation.size(); i++) {
			if(currentPMInformation.get(i).getPM() == pm) {
				result = currentPMInformation.get(i);
				break;
			}
		}
		
//		if(result == null)
//			Logger.getGlobal().info("PM " + pm.toString() + "not found as a PMInformation.");
		return result;
			
	}
	
	/**
	 * Look for a specific VMInformation inside the internal list of the VMs.
	 * 
	 * @param vm The VirtualMachine to get the information for.
	 * 
	 * @return The wanted VMInformation if it exists, null otherwise.
	 */
	public VMInformation getSpecificVMInformation(VirtualMachine vm) {
		VMInformation result = null;
		
		for(int i = 0; i < currentVMInformation.size(); i++) {
			if(currentVMInformation.get(i).getVM() == vm) {
				result = currentVMInformation.get(i);
				break;
			}
		}			
		
//		if(result == null)
//			Logger.getGlobal().info("VM " + vm.toString() + "not found as a VMInformation.");
		return result;				
	}
	
	/**
	 * Sets the current mapping of VMs to PMs.
	 * 
	 * @param mapping The current mapping.
	 */
	public void setMapping(SortedMap<PhysicalMachine, List<VirtualMachine>> mapping) {
		this.mapping = mapping;
	}
	
	/**
	 * Get the current mapping of VMs to PMs.
	 * 
	 * @return The current mapping.
	 */
	public SortedMap<PhysicalMachine, List<VirtualMachine>> getMapping() {
		return mapping;
	}
	
	/**
	 * Get the amount of previous loads to use in calculations.
	 * 
	 * @return The amount of previous loads to use in calculations.
	 */
	public int getAmountOfPrevious() {
		return amountOfPrevious;
	}	
	
	/**
	 * Helper method to be called at the end of monitoring. Sorts the lists.
	 */
	public void sortAllLists() {
		Collections.sort(currentPMInformation, new PmInfoComparator());
		Collections.sort(currentVMInformation, new VmInfoComparator());
	}
	
	
}
