package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Logger;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine.State;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.PMInformation;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.VMInformation;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.MonitorInformation;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.PmIdComparator;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.AlterableResourceConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;

/**
 * This class is used together with the AutoAdjustingConsolidator. Its function
 * is to determine the following aspects of the current situation in the cloud:
 * 
 * 1. the load of VMs
 * 2. the load of PMs
 * 3. the state of each VM
 * 
 * The output is saved inside an instance of MonitorInformation. Note that there
 * are also previous values saved, but not in the first iteration. Each other part
 * of the MAPE-loop has to be able to work with only one saved value.
 */
public class Monitor {
	
	/** time measurement */
	private long duration;
	
	/** the upper threshold */
	private double upperThreshold;
	
	/** the critical upper threshold, used to determine overload */
	private double critUpperThreshold;
	
	/** VMs without an allocation */
	private Map<VirtualMachine, ResourceConstraints> toBePlaced = new HashMap<>();
	
	/** the currently active PMs at the time of the monitoring */
	private int activePMs;
	
	
	/**
	 * Default constructor. Has to be empty, because one instance is intended to
	 * be alive as long as the instance of the consolidator.
	 * 
	 * The inputs and possible parameters are set before each usage separately.
	 */
	public Monitor() {
		
	}
	
	/**
	 * Getter for the duration of the current monitor run. Is intended to be called 
	 * after the monitoring is done.
	 * 
	 * @return The duration of the monitoring in milliseconds.
	 */
	public long getDuration() {
		return duration;
	}
	
	/**
	 * Checks every PM and VM inside the IaaS and collects information-objects
	 * for each of them containing necessary information. The load of a PM is measured 
	 * by the sum of the loads of its hosted VMs.
	 * 
	 * The previous lists of the MonitorInformation are replaced by the current ones and saved inside
	 * previous lists.
	 * 
	 * @return All collected information inside a MonitorInformation.
	 */
	public MonitorInformation monitor(IaaSService toConsolidate, MonitorInformation output, 
			int amountOfPrevious, Map<VirtualMachine, ResourceConstraints> toBePlaced, double upperThreshold,
			double thresholdLoadBehaviour, double critUpperThreshold) {
		
		// debug: print the current load balance
//		Logger.getGlobal().info(getLoadAtMonitoring(toConsolidate.runningMachines));
		
		// determine the highest processing power
		double highestProcessingPower = 0.0;
		
		if(output == null) {
//			Logger.getGlobal().info("MonitorInformation is null at this point.");
			output = new MonitorInformation(amountOfPrevious, thresholdLoadBehaviour);
		}
		
		activePMs = 0;
		this.toBePlaced = toBePlaced;
		this.upperThreshold = upperThreshold;
		this.critUpperThreshold = critUpperThreshold;
		
		long startTime = Calendar.getInstance().getTimeInMillis();
		
		// reset suspended and the overload
		output.setSuspended(false);
		output.setOverloaded(false);
		
		// reset the current overload
		AACHelper.resetCurrentOverload();
		AACHelper.resetCurrentCounter();
		
		// counter for logging
		int vmCounter = 0;
		int pmCounter = 0;
		
		// we use a sorted map to guarantee a certain order
		SortedMap<PhysicalMachine, List<VirtualMachine>> mapping = new TreeMap<>(new PmIdComparator());		
		
		// remove all destroyed VMs
		removeDestroyedVMs(output);
//		Logger.getGlobal().info("Removed " + removeDestroyedVMs(output) + " info for destroyed VMs.");
		
		// create the PMInformation
		for(PhysicalMachine pm: toConsolidate.machines) {
			
			if(pm.isRunning()) {
				activePMs++;
			}
			
			// do the VMs first
			List<VMInformation> vms = new ArrayList<VMInformation>();
			for(VirtualMachine vm: pm.listVMs()) {
				ResourceConstraints vmLoad = vm.getResourceAllocation().allocated;
				
				// check for the processing power
				if(vmLoad.getRequiredProcessingPower() > highestProcessingPower)
					highestProcessingPower = vmLoad.getRequiredProcessingPower();
				
				// add the VMInformation
				vms.add(output.addVMInformation(vm, vm.getResourceAllocation().getHost(), vmLoad, false));
				vmCounter++;
			}
			
			// add one entry in the mappings
			List<VirtualMachine> converted = new ArrayList<>(pm.listVMs());
			mapping.put(pm, converted);
			
			// get the load of the current PM
			AlterableResourceConstraints occupied = AACHelper.getNoResources();
			for(VirtualMachine vm: pm.publicVms) {
				occupied.singleAdd(vm.getResourceAllocation().allocated);
//				Logger.getGlobal().info("occupied: " + occupied.toString());
			}
			
			// check for the overload
			checkForOverloadAndSet(pm, output);				
			
			// add the PMInformation
			PMInformation created = output.addPMInformation(pm, occupied, vms);
			pmCounter++;
			
			// add the info the each VMInfo
			for(VMInformation current: vms) {
				current.setHostPMInfo(created);
			}
			
		}
		
		// create VMInformation for all suspended VMs 
		for(VirtualMachine vm: toConsolidate.listSuspendedVMs().keySet()) {			
			ResourceConstraints vmLoad = toConsolidate.listSuspendedVMs().get(vm);
			// add the VMInformation
			output.addVMInformation(vm, null, vmLoad, true);
			vmCounter++;
		}
		
		// create VMInformation for all unplaced VMs
		for(VirtualMachine vm: toBePlaced.keySet()) {
			ResourceConstraints vmLoad = toBePlaced.get(vm);
			// add the VMInformation
			output.addVMInformation(vm, null, vmLoad, false);
			vmCounter++;
		}
		
		// set the highest processing power
		output.setHighestProcessingPower(highestProcessingPower);
		
		// set the ordering of VMInformation and PMInformation to ascending order
		output.sortAllLists();
		
		// set the mapping, the total load and the unplaced VMs
		output.setMapping(mapping);		
		// set the total Load
		AACHelper.addUpTotalLoad(output.calculateTotalLoad());		
		output.setUnplacedVMs(toBePlaced);		
		
		// determine the durations
		long endTime = Calendar.getInstance().getTimeInMillis();
		duration = endTime - startTime;
		
		// debug
		Logger.getGlobal().info(compareVMInfoWithVMs(toConsolidate, output));
		
		String toLog = "Monitored " + pmCounter + " PMs (" + toConsolidate.runningMachines.size() + 
				" running | " + (toConsolidate.machines.size() - toConsolidate.runningMachines.size()) + 
				" shut down) and " + vmCounter + " VMs";
		if(toBePlaced.size() != 0) {
			toLog += ", of that " + toBePlaced.size() + " unplaced VMs.";
		}
		Logger.getGlobal().info(toLog);
//		Logger.getGlobal().info("current load is in tpp: " + output.getTotalLoad().getTotalProcessingPower());
//		Logger.getGlobal().info("current overload is in tpp: " + AACHelper.currentOverload.getTotalProcessingPower());
		
		return output;
	}
	
	/**
	 * Is called for each monitored PM and checks the possible overload of the parameterized
	 * PM. If it is overloaded, the overload is added to the totalOverload of the Monitor-
	 * Information, otherwise nothing is done.
	 * 
	 * @param pm The PM to be checked.
	 */
	private void checkForOverloadAndSet(PhysicalMachine pm, MonitorInformation output) {
		
		if(!output.areOverloadedServers()) {
			AlterableResourceConstraints thresholdResources = new AlterableResourceConstraints(
					pm.getCapacities().getRequiredCPUs() * upperThreshold, pm.getCapacities().
					getRequiredProcessingPower(), (long) (pm.getCapacities().getRequiredMemory() 
							* upperThreshold));
			
			AlterableResourceConstraints neededFreeResources = new AlterableResourceConstraints(pm.getCapacities());
			
			// after getting the threshold resources we subtract them from the whole capacities to get 
			// only the resources which have to be left so the PM is not considered overloaded
			neededFreeResources.subtract(thresholdResources);
			
			ResourceConstraints reallyAvailable = pm.availableCapacities;
			
//			if(reallyAvailable.getTotalProcessingPower() < neededFreeResources.getTotalProcessingPower() || 
//					reallyAvailable.getRequiredMemory() < neededFreeResources.getRequiredMemory()) {
			if(reallyAvailable.getTotalProcessingPower() < neededFreeResources.getTotalProcessingPower()) {
				output.setOverloaded(true);
			}
		}
		
		// now check for the overload
		AlterableResourceConstraints thresholdResources = new AlterableResourceConstraints(
				pm.getCapacities().getRequiredCPUs() * critUpperThreshold, pm.getCapacities().
				getRequiredProcessingPower(), (long) (pm.getCapacities().getRequiredMemory() 
						* critUpperThreshold));
		
		AlterableResourceConstraints neededFreeResources = new AlterableResourceConstraints(pm.getCapacities());
		
		// after getting the threshold resources we subtract them from the whole capacities to get 
		// only the resources which have to be left so the PM is not considered overloaded
		neededFreeResources.subtract(thresholdResources);
		
		ResourceConstraints reallyAvailable = pm.availableCapacities;
		
//		if(reallyAvailable.getTotalProcessingPower() < neededFreeResources.getTotalProcessingPower() || 
//				reallyAvailable.getRequiredMemory() < neededFreeResources.getRequiredMemory()) {
		if(reallyAvailable.getTotalProcessingPower() < neededFreeResources.getTotalProcessingPower()) {
			output.setOverloaded(true);
			
			// the difference is the overload
			AlterableResourceConstraints toAdd = new AlterableResourceConstraints(neededFreeResources);
			toAdd.subtract(reallyAvailable);
			AACHelper.addUpOverload(toAdd);
			AACHelper.addUpCurrentOverload(toAdd);
		}
		else
			return;
	}
	
	/**
	 * Helper method to clear finished VMInformation out of the list.
	 * 
	 * @return The amount of finished and removed VMInformation.
	 */
	private int removeDestroyedVMs(MonitorInformation output) {
		int removed = 0;
		List<VMInformation> toDelete = new ArrayList<VMInformation>();
		
		for(VMInformation info: output.getVMInformation()) {
			if(info.getVM().getState().equals(State.DESTROYED)) {				
				toDelete.add(info);				
			}
		}
		
		// actual removal
		for(VMInformation remove: toDelete) {
			removed++;
//			Logger.getGlobal().info("VM " + remove.getVM().hashCode() + " is now destroyed, removed. "
//					+ "State: " + remove.getVM().getState());
			output.getVMInformation().remove(remove);
		}
		
		return removed;
	}
	
	/**
	 * Helper method to check whether all VMs have an VMInformation object after monitoring
	 * and if there are unused VMInformation objects (maybe because some VMs are destroyed now).
	 * 
	 * @param toConsolidate the IaaSService to consolidate.
	 * @param output The MonitorInformation, containing the Information-objects for the VMs and PMs.
	 * 
	 * @return 
	 */
	private String compareVMInfoWithVMs(IaaSService toConsolidate, MonitorInformation output) {
		List<VMInformation> vmInfo = new ArrayList<>(output.getVMInformation());
		List<VirtualMachine> vms = new ArrayList<>(toConsolidate.listVMs());
		
		// add the suspended and unplaced VMs. For that a copy of each object is made
		vms.addAll(new ArrayList<>(toConsolidate.listSuspendedVMs().keySet()));
		vms.addAll(new ArrayList<>(toBePlaced.keySet()));
		
		String res = "vmInfo.size: " + vmInfo.size() + ", total VMs: " + vms.size();
		
		for(VMInformation info: vmInfo) {
			VirtualMachine infosVM = info.getVM();
			if(vms.contains(infosVM)) {
				vms.remove(infosVM);
			}
			else {
				Logger.getGlobal().info("VMInfo is for an unknown VM " + infosVM.hashCode());
			}
			
		}
		
		String result = "not considered as VMInfo: ";
		if(!vms.isEmpty()) {			
			for(VirtualMachine vm: vms) {
				result += "VM " + vm.hashCode();
			}
			res += "\n" + result;
		}
		
		return res;		
	}
	
	@SuppressWarnings("unused")
	private String getLoadAtMonitoring(List<PhysicalMachine> bins) {
		int pmCounter = 0;
		String result = "", result2 = "";
		
		
		List<PhysicalMachine> orderedBins = new ArrayList<>();
		for(PhysicalMachine pm: bins) {
			orderedBins.add(pm);
		}
		Collections.sort(orderedBins, (a, b) -> a.hashCode() < b.hashCode() ? -1 : a.hashCode() == b.hashCode() ? 0 : 1);
		
		for(PhysicalMachine pm: orderedBins) {
			if(pm.isHostingVMs()) {
				pmCounter++;
				
				AlterableResourceConstraints used = new AlterableResourceConstraints(pm.getCapacities());
				used.subtract(pm.freeCapacities);
				
				result2 += "PM " + pm.hashCode() + " used: [C:" + used.getRequiredCPUs() + 
						" P:" + used.getRequiredProcessingPower() + " M:" + 
						used.getRequiredMemory() + "]" + "\n";
			}
				
		}
		result = "getLoadAtMonitoring(), total amount of running PMs: " + pmCounter + "\n";
		result += result2;
		
		return result;
	}

	/**
	 * @return the activePMs
	 */
	public int getActivePMs() {
		return activePMs;
	}
	

}
