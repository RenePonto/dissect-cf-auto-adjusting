package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust;

import java.util.logging.Logger;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;


/**
 * @author Julian Bellendorf, Rene Ponto
 * 
 * This class represents a VM in this abstract model. It has only the necessary things for consolidation which
 * means the hosting PM, its needed resources and the id of the original VM.
 * The resource consumption happens inside the ModelPM class.
 */
public class ModelVM {
	
	/** the real VM inside the simulator */
	final private VirtualMachine vm;
	/** the current host of this model */
	private ModelPM hostPM;
	/** the initial host of this model */
	final private ModelPM initialHost;
	final private int id;
	private ResourceConstraints resources;	
	/** determines whether this VM is suspended */
	private boolean suspended;

	/**
	 * This represents a VirtualMachine of the simulator. For that this class contains the real VM itself,
	 * the actual abstract host, the resources (cores, perCoreProcessing, memory) and the id for debugging.
	 * With the resources a ResourceVector is created for this VM.
	 * 
	 * @param vm The real Virtual Machine in the Simulator.
	 * @param pm The hosting PM.
	 * @param id The ID of the original VM.
	 */	
	
	/**
	 * This represents a VirtualMachine of the simulator. For that this class contains the real VM itself,
	 * the actual abstract host, the resources (cores, perCoreProcessing, memory) and the id for debugging.
	 * With the resources a ResourceVector is created for this VM.
	 * 
	 * @param vm The real Virtual Machine in the Simulator.
	 * @param res The resources as ResourceConstraints.
	 * @param pm The hosting PM.
	 * @param id A given id to identify this modelVM.
	 * @param suspended Determines whether this VM is suspended.
	 */
	public ModelVM(final VirtualMachine vm, ResourceConstraints resources, final ModelPM pm, final int id, boolean suspended) {
		
		this.vm = vm;
		hostPM = pm;		// save the host PM
		initialHost = pm;	// save the host as the first host PM
//		this.id = id;
		this.id = vm.hashCode();
		this.resources = resources;		
		this.suspended = suspended;
	}
	
	/**
	 * Suspends this ModelVM, so nothing can be done with this object except resuming or migrating it.
	 */
	public void suspend() {
		if(suspended)
			Logger.getGlobal().info("Suspended VM " + hashCode() + "is suspended again.");
		
		suspended = true;
		hostPM.removeVM(this);
		this.hostPM = null;		
	}
	
	/**
	 * Resumes this ModelVM.
	 */
	public void resume(ModelPM newHost) {
		if(!suspended) {
			Logger.getGlobal().info("Running VM " + hashCode() + "is resumed.");
			return;
		}
		suspended = false;
		sethostPM(newHost);
		newHost.addVM(this);
	}
	
	/**
	 * Determines whether this ModelVM is suspended or not.
	 * 
	 * @return True if this ModelVM is suspended, false otherwise.
	 */
	public boolean isSuspended() {
		return suspended;
	}

	/**
	 * toString() is just for debugging and returns the ID, cores, perCoreProcessingPower and memory of this VM.
	 */	
	public String toString() {
		return "VM " + vm.hashCode() + ", res: " + "[C: " + resources.getRequiredCPUs() + " P: " + 
				resources.getRequiredProcessingPower() + " M: " + resources.getRequiredMemory() + "]";
	}

	/** 
	 * Get the resources of this ModelVM represented by ResourceConstraints.
	 * 
	 * @return The resources of this ModelVM represented by ResourceConstraints.
	 */
	public ResourceConstraints getResources() {
		return resources;
	}
	
	/** 
	 * Get the current host of this ModelVM.
	 * 
	 * @return The current host of this ModelVM.	 
	 */
	public ModelPM gethostPM() {
		return  hostPM;
	}
	
	/**
	 * Set a new host for this ModelVM. Should only be used after migration.
	 * 
	 * @param bin The new host for this ModelVM.
	 */
	public void sethostPM(ModelPM bin) {
		this.hostPM = bin;
	}

	/**
	 * Get the id of this ModelVM.
	 * 
	 * @return The id of this ModelVM.
	 */
	@Override
	public int hashCode() {
		return id;
	}
	
	/**
	 * Get the VirtualMachine represented by this ModelVM.
	 * 
	 * @return The VirtualMachine represented by this ModelVM.
	 */
	public VirtualMachine getVM() {
		return vm;
	}
	
	/**
	 * Get the initial host PM of this ModelVM.
	 * 
	 * @return The initial host PM of this ModelVM.
	 */
	public ModelPM getInitialHost() {
		return initialHost;
	}
}
