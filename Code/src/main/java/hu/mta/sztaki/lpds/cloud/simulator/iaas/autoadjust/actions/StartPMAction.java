package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.actions;

import java.util.List;
import java.util.logging.Logger;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine.State;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelPM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AACHelper;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.pmscheduling.IControllablePmScheduler;

/**
 * This class stores actions, which need to start a PM in the simulator.
 */
public class StartPMAction extends Action implements PhysicalMachine.StateChangeListener {

	//Reference to the model of the PM, which needs to start
	protected ModelPM pmToStart;

	/** PM scheduler */
	protected IControllablePmScheduler pmScheduler;

	/**
	 * Constructor of an action to start a PM.
	 * @param id The ID of this action.
	 * @param pmToStart The modelled PM respresenting the PM which shall start.
	 */
	public StartPMAction(int id, ModelPM pmToStart, IControllablePmScheduler pmScheduler) {
		super(id);
		this.pmToStart = pmToStart;
		this.pmScheduler = pmScheduler;
	}

	/**
	 * 
	 * @return The modeled PM representing the PM which shall start.
	 */
	public ModelPM getPmToStart(){
		return pmToStart;
	}

	/**
	 * There are no predecessors for a starting action.
	 */
	@Override
	public void determinePredecessors(List<Action> actions) {
		// debug: check whether this action occurs as a shutdown action, which should not be the case
		for(Action action: actions) {
			if(action.getType().equals(Type.PMSHUTDOWN) && ((StopPMAction) action).getPmToShutDown().equals(pmToStart)) {
				Logger.getGlobal().info("For the StartPMAction for PM " + pmToStart.hashCode() + " exists "
						+ "also a shutdown action, skip the start.");
			}
		}
		// do nothing
	}

	@Override
	public Type getType() {
		return Type.PMSTART;
	}

	@Override
	public String toString() {
		return "Action: " + getType() + "  : PM " + getPmToStart().getPM().hashCode();
	}

	/**
	 * Method for starting a PM inside the simulator.
	 */
	@Override
	public void execute() {
//		Logger.getGlobal().info("Executing at "+Timed.getFireCount()+": "+toString());
		PhysicalMachine pm = this.getPmToStart().getPM();
		
		// check if this PM is already on or about to run
		if(pm.getState().equals(State.RUNNING) || pm.getState().equals(State.SWITCHINGON)) {
			AACHelper.failedStartPM++;
			finished();
		}
		else {
			pm.subscribeStateChangeEvents(this);		//observe the PM before turning it on
			pmScheduler.switchOn(pm);
			AACHelper.pmStartCounter++;
			AACHelper.startPM++;
		}		
		
	}

	/**
	 * The stateChanged-logic, if the PM which has been started changes its state to RUNNING,
	 * we can stop observing it.
	 */
	@Override
	public void stateChanged(PhysicalMachine pm, hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine.State oldState,
			hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine.State newState) {
		if(newState.equals(PhysicalMachine.State.RUNNING)){
			pm.unsubscribeStateChangeEvents(this);			
			finished();
		}
	}
	
}
