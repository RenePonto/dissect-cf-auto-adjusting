package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information;

import java.util.Comparator;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.PMInformation;

public class PmInfoComparator implements Comparator<PMInformation> {

	@Override
	public int compare(PMInformation o1, PMInformation o2) {
		if(o1.getPM().hashCode() < o2.getPM().hashCode()) {
			return -1;
		}
		else if(o1.getPM().hashCode() == o2.getPM().hashCode()) {
			return 0;
		}
		else {
			return 1;
		}
	}

}
