package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import gurobi.GRB;
import gurobi.GRB.IntAttr;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelPM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelVM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.AnalyzerOutput;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.AlterableResourceConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import gurobi.GRBModel;
import gurobi.GRBVar;

/**
 * This consolidator uses gurobi to find the globally best allocation
 * for a specified amount of PMs and VMs.
 */
public class IlpConsolidator {
	
	/** gurobi specific objects and mappings */
	private GRBEnv env;
	private GRBModel model;
	private Map<ModelPM, GRBVar> varActive;
	private Map<ModelVM, Map<ModelPM,GRBVar>> varAlloc;
	private Map<ModelVM, GRBVar> varMigr;
	
	/** counts all created constraints for gurobi to solve the problem correctly */
	private int constraintCounter = 0;
	
	/** determines the amount of unsuccessful runs / first fit uses */
	private static int nrUnsuccessful = 0;
	
	/** time limit for optimization */
	@SuppressWarnings("unused")
	private int gurobiTimeLimit;
	/** threshold for only placing non-correlated VMs together */
	private double correlationThreshold;
	/** the threshold to use in gurobi for a PM */
	private double thresholdGurobiPMs;
	
	/** array containing all ModelPMs used for optimization */
	private ModelPM[] hostArray;
	/** array containing all ModelVMs used for optimization */
	private ModelVM[] vmArray;
	/** the AnalyzerOutput containing the correlations between the objects */
	private AnalyzerOutput input;

	/**
	 * The constructor sets all necessary objects and initializes the maps for gurobi.
	 * 
	 * @param hostArray An array containing all ModelPMs.
	 * @param input The AnalyzerInput.
	 * @param gurobiTimeLimit The timelite within gurobi has to find a solution.
	 * @param correlationThreshold The threshold for the correlations.
	 */
	public IlpConsolidator(ModelPM[] hostArray, ModelVM[] vmArray, AnalyzerOutput input, int gurobiTimeLimit, 
			double correlationThreshold, double thresholdGurobiPMs, boolean isA3SizesUsed) {
		varActive = new HashMap<>();
		varAlloc = new HashMap<>();
		varMigr = new HashMap<>();
		
		// check for sizes of A3
		if(isA3SizesUsed) {
			Logger.getGlobal().info("Size inpt of A3 is used.");
		}
		
		this.hostArray = hostArray;
		this.vmArray = vmArray;
		this.input = input;
		this.gurobiTimeLimit = gurobiTimeLimit;
		this.correlationThreshold = correlationThreshold;
		this.thresholdGurobiPMs = thresholdGurobiPMs;
	}
	
	/**
	 * Adds the constraints of the AnalyzerOutput. In detail, we add constraints to
	 * the model determining which VMs should not be hosted on specific PMs.
	 * 
	 * @param input The AnalyzerInput containing the necessary information.
	 * @throws GRBException 
	 */
	private void addInput() throws GRBException {
		
		// debug: check if all VMs occur inside the list with high correlations
		List<Integer> hashCodes = new ArrayList<>();
		
		// convert the VMs to ModelVMs
		Map<ModelVM[], Double> correlations = perpareCorrelationInput();
		
		if(correlations.isEmpty()) {
			Logger.getGlobal().info("No correlations usable, skipping correlation input.");
		} 
		else {
			for(ModelVM[] pair : correlations.keySet()) {
				if(!hashCodes.contains(pair[0].hashCode()))
					hashCodes.add(pair[0].hashCode());
				
				if(!hashCodes.contains(pair[1].hashCode()))
					hashCodes.add(pair[1].hashCode());
				// add constraint: two positively correlated VMs should not be hosted on the same PM
				for (ModelPM host : hostArray) {
					
					GRBLinExpr expr = new GRBLinExpr();
					GRBVar alloc = varAlloc.get(pair[0]).get(host);
					GRBVar alloc2 = varAlloc.get(pair[1]).get(host);

					expr.addTerm(1, alloc);
					expr.addTerm(1, alloc2);
					model.addConstr(expr, GRB.LESS_EQUAL, 1, "");

					// increase the constraint counter
					constraintCounter++;
				}
			}
			
			Logger.getGlobal().info("Amount of VMs with high correlation: " + hashCodes.size());
		}		
		
	}
	
	/**
	 * Method to convert the result of A3 to ModelVMs created inside the Planner.
	 * 
	 * @param vmArray A list containing all VMs out of the map for conversion.
	 * @param input The AnalyzerOutput to use.
	 * 
	 * @return A map containing the conversion of the AnalyzerOutput.
	 */
	private Map<ModelVM[], Double> perpareCorrelationInput() {
		Map<ModelVM[], Double> result = new HashMap<ModelVM[], Double>();
		Logger.getGlobal().info("Total amount of VM-Pairs with correlations: " + input.getCorrelations().size());
		
		// get all the VMs with a high correlation
		Map<VirtualMachine[], Double> necessaryVMPairs = new HashMap<>();
		for(VirtualMachine[] pair: input.getCorrelations().keySet()) {
			if(input.getCorrelations().get(pair) > correlationThreshold) {
				necessaryVMPairs.put(pair, input.getCorrelations().get(pair));
			}
		}
		
		Logger.getGlobal().info("Total amount of VM-Pairs with a high correlation: " + necessaryVMPairs.size());
		
		// find the corresponding ModelVMs to the normal VMs
		for(VirtualMachine[] pair: necessaryVMPairs.keySet()) {
			VirtualMachine vm1 = pair[0];
			VirtualMachine vm2 = pair[1];
			
			ModelVM mvm1 = null;
			ModelVM mvm2 = null;
			
			for(ModelVM model: vmArray) {
				if(model.getVM().equals(vm1)) {
					mvm1 = model;
					break;
				}				
			}
			for(ModelVM model: vmArray) {
				if(model.getVM().equals(vm2)) {
					mvm2 = model;
					break;
				}				
			}
			
			// if for any reason the correct ModelVM cannot be find, we ignore this pair
			if(mvm1 == null || mvm2 == null) {
//				result.put(new ModelVM[] {mvm1, mvm2}, 0.0);
				Logger.getGlobal().info("One or both of the VMs have not been converted (null) for the correlations.");
				continue;
			}
			else {
				result.put(new ModelVM[] {mvm1, mvm2}, input.getCorrelations().get(pair));
			}			
			
		}
		
		if(result.size() != necessaryVMPairs.size()) {
			Logger.getGlobal().info("The maps are not in sync.");
		}
		
		return result;
	}

	/**
	 * Builds the model for gurobi optimization.
	 * 
	 * @param vmArray All ModelVMs used for optimization.
	 * @param hostArray All ModelPMs used for optimization.
	 * 
	 * @throws GRBException
	 */
	private void buildModel() throws GRBException {
		
		// error cases: lists cannot be empty
		if(vmArray.length == 0)
			Logger.getGlobal().info("vmList is empty");
		if(hostArray.length == 0)
			Logger.getGlobal().info("pmlist is empty");
		
		// Create variables
		for(ModelPM pm : hostArray) {
			varActive.put(pm, model.addVar(0, 1, 0, GRB.BINARY, "Active_" + pm.hashCode()));
		}
		for(ModelVM vm : vmArray) {
			Map<ModelPM, GRBVar> currMap = new HashMap<>();
			for(ModelPM pm : hostArray) {
				currMap.put(pm, model.addVar(0, 1, 0, GRB.BINARY, "Alloc_" + vm.hashCode() + "_" + pm.hashCode()));
			}
			varAlloc.put(vm, currMap);
		}
		for(ModelVM vm : vmArray) {
			varMigr.put(vm, model.addVar(0, 1, 0, GRB.BINARY, "Migr_" + vm.hashCode()));
		}
		//model.update();
		Logger.getGlobal().info("Finished creating variables.");
		
		
		//Constraints
		//If a PM hosts a VM, the PM must be active
		for(ModelVM vm : vmArray) {
			for(ModelPM pm : hostArray) {
				GRBLinExpr expr = new GRBLinExpr();
				GRBVar alloc = varAlloc.get(vm).get(pm);
				GRBVar active = varActive.get(pm);
				expr.addTerm(1, alloc);
				expr.addTerm(-1, active);
				model.addConstr(expr, GRB.LESS_EQUAL, 0, "");
				
				// increase the constraint counter
				constraintCounter++;
			}
		}
		
		//Each VM is mapped to exactly one PM
		for(ModelVM vm : vmArray) {
			GRBLinExpr expr = new GRBLinExpr();
			for(ModelPM pm : hostArray) {
				GRBVar alloc = varAlloc.get(vm).get(pm);
				expr.addTerm(1, alloc);
			}
			model.addConstr(expr, GRB.EQUAL, 1, "");
			
			// increase the constraint counter
			constraintCounter++;
		}
		
		//Capacity constraint
//		for(ModelPM pm : pmList) {
//			GRBLinExpr expr = new GRBLinExpr();
//			for(ModelVM vm : vmList) {
////				Logger.getGlobal().info("VM " + vm.hashCode() + ", State " + vm.getVM().getState());
//				GRBVar alloc = varAlloc.get(vm).get(pm);
//				expr.addTerm(vm.getResources().getTotalProcessingPower(), alloc);
//			}
//			model.addConstr(expr, GRB.LESS_EQUAL, pm.getTotalResources().getTotalProcessingPower(), "");
//		
//		// increase the constraint counter
//		constraintCounter++;
//		}
		
		// individual capacity constraints for each resource
		for(ModelPM pm : hostArray) {
			ResourceConstraints toAchieve = new AlterableResourceConstraints(pm.getTotalResources().getRequiredCPUs() * 
					thresholdGurobiPMs, pm.getTotalResources().getRequiredProcessingPower(), 
					(long) (pm.getTotalResources().getRequiredMemory() * thresholdGurobiPMs));
			
			GRBLinExpr expr = new GRBLinExpr();
			for(ModelVM vm : vmArray) {
//				Logger.getGlobal().info("VM " + vm.hashCode() + ", State " + vm.getVM().getState());
				GRBVar alloc = varAlloc.get(vm).get(pm);
				expr.addTerm(vm.getResources().getTotalProcessingPower(), alloc);
			}
			
			model.addConstr(expr, GRB.LESS_EQUAL, toAchieve.getTotalProcessingPower(), "");			
			// increase the constraint counter
			constraintCounter++;
						
			// FIXME this constraint does not work for PMs with different processingPower
//			for(ModelVM vm : vmArray) {
////				Logger.getGlobal().info("VM " + vm.hashCode() + ", State " + vm.getVM().getState());
//				expr = new GRBLinExpr();
//				GRBVar alloc = varAlloc.get(vm).get(pm);
//				expr.addTerm(vm.getResources().getRequiredProcessingPower(), alloc);				
//			}
//			model.addConstr(expr, GRB.LESS_EQUAL, toAchieve.getRequiredProcessingPower(), "");			
//			// increase the constraint counter
//			constraintCounter++;
			
			expr = new GRBLinExpr();
			for(ModelVM vm : vmArray) {
//				Logger.getGlobal().info("VM " + vm.hashCode() + ", State " + vm.getVM().getState());
				GRBVar alloc = varAlloc.get(vm).get(pm);
				expr.addTerm(vm.getResources().getRequiredMemory(), alloc);
			}
			model.addConstr(expr, GRB.LESS_EQUAL, toAchieve.getRequiredMemory(), "");			
			// increase the constraint counter
			constraintCounter++;
		}
		
		//Setting migration variables
		for(ModelVM vm : vmArray) {
			ModelPM host = vm.gethostPM();
			if(host == null) {
				GRBLinExpr expr = new GRBLinExpr();
				GRBVar migr = varMigr.get(vm);
				expr.addTerm(1, migr);
				model.addConstr(expr, GRB.EQUAL, 0, "");
				
				// increase the constraint counter
				constraintCounter++;
			}
			else {
				GRBLinExpr expr = new GRBLinExpr();
				GRBVar migr = varMigr.get(vm);
				GRBVar alloc = varAlloc.get(vm).get(host);
				expr.addTerm(1, migr);
				expr.addTerm(1, alloc);
				model.addConstr(expr, GRB.EQUAL, 1, "");
				
				// increase the constraint counter
				constraintCounter++;
			}
		}
		
		//Additional constraint to make the migration phase practically feasible:
		//No migration to already overloaded hosts
//		for(ModelPM pm : pmList) {
//			if(pm.isOverAllocated()) {
//				for(ModelVM vm : vmList) {
//					if(vm.gethostPM() != pm) {
//						GRBLinExpr expr = new GRBLinExpr();
//						GRBVar alloc = varAlloc.get(vm).get(pm);
//						expr.addTerm(1, alloc);
//						model.addConstr(expr, GRB.EQUAL, 0, "");
//					}
//				}
//			} 
//		}
		
		int standard = constraintCounter;
		Logger.getGlobal().info("Finished creating standard constraints, total: " + standard + ".");
		
		// additionally, add the A3 input 
		addInput();
		Logger.getGlobal().info("Finished adding the input of the A3-algorithm, added constraints for A3: " + (constraintCounter - 
				standard) + ", total constraints: " + constraintCounter + ".");
		
		// Objective: 10*number of active PMs + 1*number of migrations
		GRBLinExpr expr = new GRBLinExpr();
		
		for(ModelPM pm : hostArray) {
			expr.addTerm(10, varActive.get(pm));
		}
		
		for(ModelVM vm : vmArray) {
			expr.addTerm(0.1, varMigr.get(vm));
		}
		
	    model.setObjective(expr, GRB.MINIMIZE);
//	    Logger.getGlobal().info("Set the optimization objective.");
	}

	/**
	 * Helper method to get a new host for a VM.
	 * 
	 * @param vm The ModelVM to find a new host for.
	 * @param pmList An array containing all ModelPMs.
	 * 
	 * @return Null, if no PM is found, otherwise the targetPM.
	 * 
	 * @throws GRBException
	 */
	private ModelPM getNewHostOfVm(ModelVM vm, ModelPM[] pmList) throws GRBException {
		ModelPM result = null;
		for(ModelPM pm : pmList) {
			GRBVar var = varAlloc.get(vm).get(pm);
			double val = var.get(GRB.DoubleAttr.X);
			if(val > 0.9) {
				result = pm;
				break;
			}
		}
		return result;
	}

	/**
	 * Optimizes the current allocation with the gurobi model. At the
	 * moment we need a conversion of the array at the beginning.
	 * 
	 * @param vmArray An array containing all ModelVMs used for optimization.
	 * 
	 * @return A list containing a mapping for each VM and its hosting PM. At first the objects are 
	 * 		   saved as Strings ("vm" for the VM and "host" for the PM, respectively) and then
	 * 		   the process is repeated for each VM. If gurobi cannot find a solution, null is returned.
	 */
	public List<Map<String, Object>> optimizeAllocation() {
		
		if(vmArray.length == 0) {
			Logger.getGlobal().info("vmArray is empty!");
		}
//		else if(vmList.contains(null)) {
//			Logger.getGlobal().info("list contains null values.");
//			System.exit(-1);
//		}
		
		/**
		 * List contains a mapping for each VM and its hosting PM. At first the objects are 
		 * saved as Strings ("vm" for the VM and "host" for the PM, respectively) and then
		 * the process is repeated for each VM.
		 */
		List<Map<String, Object>> migrationMap = new ArrayList<Map<String, Object>>();
		try {
			Logger.getGlobal().info("Building ILP model");
			env = new GRBEnv("milp.log");
			model = new GRBModel(env);
			
			// set up the variables
			buildModel();
			
			// determine gurobi specific limit
//			model.getEnv().set(GRB.DoubleParam.TimeLimit, gurobiTimeLimit);
			
//			model.set(GRB.DoubleParam.IterationLimit, 100000);
//			model.set(GRB.IntParam.SolutionLimit, 6);			
//			model.set(GRB.DoubleParam.NodeLimit, 3);
//			model.set(GRB.DoubleParam.MIPGapAbs, 15);
			model.set(GRB.DoubleParam.MIPGap, 0.05);
			
			Logger.getGlobal().info("Solving ILP");
			
			// debug: print the VM IDs in the current order and compare with other runs
//			String toLog = "VM order before optimize(): ";
//			for(ModelVM vm: vmArray) {
//				toLog += vm.hashCode() + " ";
//			}
//			Logger.getGlobal().info(toLog);
//			
//			// debug: do the same for the PMs
//			toLog = "PM order before optimize(): ";
//			for(ModelPM pm: hostArray) {
//				toLog += pm.hashCode() + " ";
//			}
//			Logger.getGlobal().info(toLog);
			
			// start to optimize the model
			model.optimize();
			
			// if there is a solution, we do all necessary migrations
			if(model.get(IntAttr.SolCount) > 0) {
				Logger.getGlobal().info("Processing ILP solution");
				for(ModelVM vm : vmArray) {
					GRBVar migrVar = varMigr.get(vm);
					// FIXME get all vars
					if(migrVar.get(GRB.DoubleAttr.X) > 0.9) {
						ModelPM newHostOfVm = getNewHostOfVm(vm, hostArray);
						Map<String, Object> migrate = new HashMap<String, Object>();
						migrate.put("vm", vm);
						migrate.put("pm", newHostOfVm);
						migrationMap.add(migrate);
					}
					// do the vms with lower X, too
//					else {						
//						ModelPM newHostOfVm = getNewHostOfVm(vm, hostArray);
//						Map<String, Object> migrate = new HashMap<String, Object>();
//						migrate.put("vm", vm);
//						migrate.put("pm", newHostOfVm);
//						migrationMap.add(migrate);
//					}
				}
				
			}
			// there is no solution, we migrate by using first fit
			else {
				Logger.getGlobal().info("No solution found in time, using first fit. "
						+ "Unsuccessful runs so far: " + nrUnsuccessful++);
								
				return null;
			}
			
			// Dispose of model and environment
			model.dispose();
		    env.dispose();
		    
		} catch (GRBException e) {
			System.err.println("Gurobi exception");
			e.printStackTrace();
			System.exit(-1);
		}
		
//		try {
//			model.reset(1);
//			env.release();
//		} catch (GRBException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return migrationMap;
	}

	/**
	 * Getter to retrieve the amount of unsuccessful runs of gurobi.
	 * 
	 * @return The amount of unsuccessful runs of gurobi.
	 */
	public static int getNrUnsuccessful() {
		return nrUnsuccessful;
	}
}
