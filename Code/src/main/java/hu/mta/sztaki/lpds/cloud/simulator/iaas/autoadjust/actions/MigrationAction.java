package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.actions;

import java.util.List;
import java.util.logging.Logger;

import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VMManager.VMManagementException;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine.State;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelPM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelVM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AACHelper;
import hu.mta.sztaki.lpds.cloud.simulator.io.NetworkNode.NetworkException;

/**
 * This class stores actions, that need to commit a migration in the simulator.
 */
public class MigrationAction extends Action implements VirtualMachine.StateChange {
	
	//Reference to the model of the PM, which contains the VM before migrating it
	protected ModelPM source;

	//Reference to the model of the PM, which contains the VM after migrating it
	protected ModelPM target;

	//Reference to the model of the VM, which needs be migrated
	protected ModelVM mvm;

	/**
	 * Constructor for an action which shall migrate a VM inside the simulator.
	 * 
	 * @param id The ID of this action.
	 * @param source The PM which is currently hosting the VM.
	 * @param target The PM which shall host this VM after migration.
	 * @param vm The reference to the VM which shall be migrated.
	 */
	public MigrationAction(int id, ModelPM source, ModelPM target, ModelVM vm) {
		super(id);
		this.source = source;
		this.target = target;
		this.mvm = vm;
	}

	/**
	 * 
	 * @return Reference to the model of the PM, which contains the VM before migrating it
	 */
	public ModelPM getSource(){
		return source;
	}
	
	/**
	 * 
	 * @return Reference to the model of the PM, which contains the VM after migrating it
	 */
	public ModelPM getTarget(){
		return target;
	}
	
	/**
	 * 
	 * @return Reference to the model of the VM, which needs be migrated
	 */
	public ModelVM getVm(){
		return mvm;
	}

	/**
	 * This method determines the predecessors of this action. A predecessor of
	 * a migration action is a starting action, which starts the target PM of 
	 * this action. 
	 * Furthermore, migrations from our target PM are also considered 
	 * predecessors, in order to prohibit temporary overloads of the PM.
	 * TODO: this needs improvement, as it can currently lead to deadlocks.
	 */
	@Override
	public void determinePredecessors(List<Action> actions) {
		// looking for actions where a PM gets started, that is the target of this
		// migration
		for (Action action : actions) {
			if (action.getType().equals(Type.PMSTART)) {
				if ((((StartPMAction) action).pmToStart.hashCode() == target.hashCode())) {
					this.addPredecessor(action);
				}
			}
			// If two PMs would like to migrate one VM to each other,
			// there could be a loop. Not solved yet.
			else if (action.getType().equals(Type.MIGRATION)) {
				// in case of suspensions we have null as previous PM
				if ((((MigrationAction) action).getSource()) == null) {
					continue;
				} else if ((((MigrationAction) action).getSource()).equals(this.getTarget())) {
					this.addPredecessor(action);
				}
			}
		}

	}

	@Override
	public Type getType() {
		return Type.MIGRATION;
	}

	@Override
	public String toString() {
		return "Action: " + getType() + " Source:  PM " + getSource().getPM().hashCode() + " Target: PM " + 
				getTarget().getPM().hashCode() + " VM: " + getVm().getVM().hashCode();
	}

	/**
	 * Method for doing the migration inside the simulator.
	 */
	@Override
	public void execute() {
		Logger.getGlobal().info("Executing at " + Timed.getFireCount() + ": " + toString());
		
		// check whether the source PM does not host the VM to migrate anymore
		if(!source.getPM().publicVms.contains(mvm.getVM())) {
			Logger.getGlobal().info("VM is not on the source PM anymore -> there is nothing to do");
			AACHelper.failedMigrations++;
			finished();
		} 
		// check whether the target PM does not have enough resources anymore to host the VM
		else if(mvm.getVM().getMemSize() > target.getPM().freeCapacities.getRequiredMemory() || 
				mvm.getVM().getPerTickProcessingPower() > target.getPM().freeCapacities.getTotalProcessingPower()) {
			
				Logger.getGlobal().info("Target PM does not have sufficient capacity anymore -> there is nothing to do");
				AACHelper.failedMigrations++;
				finished();
		} 
		// check whether the VM to migrate is in an inappropriate state for migration
		else if(mvm.getVM().getState() != VirtualMachine.State.RUNNING && mvm.getVM().getState() != VirtualMachine.State.SUSPENDED) {
			
			Logger.getGlobal().info("State of the VM inappropriate for migration (" + mvm.getVM().getState() + ") -> there is nothing to do");
			AACHelper.failedMigrations++;
			finished();
		} 
		// check whether the target PM is not running anymore
		else if(!(target.getPM().isRunning())) {
			
			Logger.getGlobal().info("Target PM not running -> there is nothing to do");
			AACHelper.failedMigrations++;
			finished();
		} 
		// check whether the source PM is not running anymore
		else if(!(source.getPM().isRunning())) {
			
			Logger.getGlobal().info("Source PM not running -> we cannot migrate VMs form there");
			AACHelper.failedMigrations++;
			finished();
		} 
		else {
			mvm.getVM().subscribeStateChange(this);		// observe the VM which shall be migrated
			try {
				// normal migration
				source.getPM().migrateVM(mvm.getVM(), target.getPM());

				// increase the counter
				AACHelper.migrationCounter++;
				AACHelper.migrations++;
			} catch (VMManagementException e) {
				AACHelper.failedMigrations++;
				e.printStackTrace();

				// log the exception ...
//				StringWriter sw = new StringWriter();
//				e.printStackTrace(new PrintWriter(sw));
//				Logger.getGlobal().info(sw.toString() + "regarding " + toString() + '\n');
			} catch (NetworkException e) {
				AACHelper.failedMigrations++;
				e.printStackTrace();

				// log the exception ...
//				StringWriter sw = new StringWriter();
//				e.printStackTrace(new PrintWriter(sw));
//				Logger.getGlobal().info(sw.toString() + "regarding " + toString() + '\n');
			}
						
			
		}
	}

	/**
	 * The stateChanged-logic, if the VM changes its state to RUNNING after migrating,
	 * then it do not has to be observed any longer.
	 */
	@Override
	public void stateChanged(VirtualMachine vm, State oldState, State newState) {
		if(newState.equals(VirtualMachine.State.RUNNING)){
			vm.unsubscribeStateChange(this);
			//Logger.getGlobal().info("Migration action finished");
			//Logger.getGlobal().info("Finished at "+Timed.getFireCount()+": "+toString()+", hash="+Integer.toHexString(System.identityHashCode(this)));
			finished();			
		}
	}

}
