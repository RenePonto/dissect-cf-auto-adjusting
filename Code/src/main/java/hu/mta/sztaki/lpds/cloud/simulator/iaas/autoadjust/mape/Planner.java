package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelPM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelVM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelPM.State;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AutoAdjustingConsolidator.AnalyzeParameter;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AutoAdjustingConsolidator.PlanningParameter;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.AnalyzerOutput;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.ModelPmIdComparator;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.ModelVmIdComparator;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.Plan;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.AlterableResourceConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;

/**
 * This class is used together with the AutoAdjustingConsolidator. Its function
 * is to plan the next steps to improve the current situation in the cloud 
 * accordingly to the results of the analyze-step and the auto-adjust.
 * In total there are four different scenarios possible:
 * 
 * P1: Quickly find a plan for relieving highly loaded servers with migrations 
 * or VM suspensions
 * 
 * P2: Quickly find a plan to resume suspended VMs and relieve overloaded servers
 * 
 * P3: Find the globally best allocation of VMs to servers
 * 
 * P4: Devise a set of local actions to address changes since the last invocation
 */
public class Planner {
	
	// determines that adjusted sizes are used
	private boolean isA3SizeInputUsed;

	/** the used ModelPMs */
	protected ModelPM[] bins;
	/** the used ModelVMs */
	protected ModelVM[] items;
	
	/** the thresholds to work with */
	private double thresholdP1, thresholdP2, thresholdP4, correlationThreshold, thresholdGurobiPMs;
	
	/** the time limit to stop gurobi while using P3 */
	private int gurobiTimeLimit;
	
	/** time measurement */
	private long duration;
	
	private boolean useDefaults;
	
	/** determines whether suspensions are allowed by the planner algorithms */
	private boolean allowP1suspensions, allowP4suspensions;
	
	/**
	 * Default constructor. Has to be empty, because one instance is intended to
	 * be alive as long as the instance of the consolidator.
	 * 
	 * The inputs and possible parameters are set before each usage separately.
	 */
	public Planner(boolean useDefaults, boolean allowP1suspensions, boolean allowP4suspensions) {
		this.useDefaults = useDefaults;
		this.allowP1suspensions = allowP1suspensions;
		this.allowP4suspensions = allowP4suspensions;
	}
	
	/**
	 * Use the determined algorithm for planning.
	 * 
	 * @param p The PlanningParameter.
	 */
	public Plan plan(PlanningParameter p, AnalyzerOutput input, double thresholdP1, double thresholdP2,
			double thresholdP4, int gurobiTimeLimit, double correlationThreshold,
			double lowerThreshold, double upperThreshold, double thresholdGurobiPMs) {
		Logger.getGlobal().info("started to plan with parameter " + p);	
		
		long startTime = Calendar.getInstance().getTimeInMillis();
		
		this.thresholdP1 = thresholdP1;
		this.thresholdP2 = thresholdP2;
		this.thresholdP4 = thresholdP4;
		this.gurobiTimeLimit = gurobiTimeLimit;
		this.correlationThreshold = correlationThreshold;
		this.thresholdGurobiPMs = thresholdGurobiPMs;
		
		// reset the boolean
		isA3SizeInputUsed = false;
		
		instantiate(input, lowerThreshold, upperThreshold);
		
		// sort the bins and items
		Arrays.sort(bins, new ModelPmIdComparator());
		Arrays.sort(items, new ModelVmIdComparator());
		
		// sort the VM list of each bin
		for(ModelPM bin: bins) {
			bin.sortVmList();
		}
		
		Logger.getGlobal().info(getLoadBeforeOptimization());	// TODO debug
		
		// debug: check the situation right after instantiation
//		Logger.getGlobal().info(toString());
//		Logger.getGlobal().info(input.toString());
		
		Plan output = new Plan(bins, items, p);
		if(isA3SizeInputUsed) {
			output.setA3InputUsed();
		}		
		
		switch(p) {
		case P1:
			useAlgorithmP1(input, output);
			break;
		case P2:
			useAlgorithmP2(input, output);
			break;
		case P3:
			useAlgorithmP3(input, output);
			break;
		case P4:
			useAlgorithmP4(input, output);
			break;
		default:
			Logger.getGlobal().info("This should never happen.");
			System.exit(-1);
			break;		
		}
		
		adaptPmStates();
		Logger.getGlobal().info(getLoadAfterOptimization());	// TODO debug
		
		output.setCurrent(bins, items);
		output.getChangesBetweenBins();
		
		// debug: check the mappings after the used algorithm and adapting the states
//		Logger.getGlobal().info(toString());
		
		// determine the durations
		long endTime = Calendar.getInstance().getTimeInMillis();
		duration = endTime - startTime;
		
		return output;
	}
	
	/**
	 * Use the AnalyzerOutput to create the abstract model. NOTE: Due to possible inconsistencies the PMs and VMs
	 * are identified using new hash codes each time.
	 * 
	 * @param input The AnalyzerOutput containing all necessary information.
	 */
	private void instantiate(AnalyzerOutput input, double lowerThreshold, double upperThreshold) {
		final ArrayList<ModelPM> pminit = new ArrayList<>();
		final ArrayList<ModelVM> vminit = new ArrayList<>();		

		List<VirtualMachine> suspended = input.getSuspendedVMs();
		
		// we have to check if A3 is used, then we have to use different ResourceConstraints (but only if there are any)
		if(input.getUsedParameter().equals(AnalyzeParameter.A3) && !input.getSizes().isEmpty()) {
			Logger.getGlobal().info("A3 is used, instantiate model with calculated sizes.");
			isA3SizeInputUsed = true;
			
			for(PhysicalMachine pm : input.getMapping().keySet()) {
				ModelPM bin = new ModelPM(pm, lowerThreshold, upperThreshold, pminit.size());
				
				// add the hosted VMs by PMs
				for(VirtualMachine vm : pm.publicVms) {
					ModelVM item = new ModelVM(vm, input.getSizes().get(vm), bin, vminit.size(), false);
					bin.addVM(item);
					vminit.add(item);
				}
				bin.checkAllocation();
				pminit.add(bin);
			}
			
			// add the suspended VMs to the model
			for(int i = 0; i < suspended.size(); i++) {
				VirtualMachine sus = suspended.get(i);
				// we use the previous resources for suspended VMs
				ModelVM item = new ModelVM(sus, input.getSizes().get(sus), null, vminit.size(), true);
				vminit.add(item);
			}
			
		}
		else {
			for(PhysicalMachine pm : input.getMapping().keySet()) {
				// now every PM will be put inside the model with its hosted VMs
				
				ModelPM bin = new ModelPM(pm, lowerThreshold, upperThreshold, pminit.size());				
				// add the hosted VMs
				for(VirtualMachine vm : pm.publicVms) {
					ModelVM item = new ModelVM(vm, vm.getResourceAllocation().allocated, bin, 
							vminit.size(), false);
					bin.addVM(item);
					vminit.add(item);
				}
				// set the current state and add the PM
				bin.checkAllocation();
				pminit.add(bin);
			}
			
			// add the suspended VMs to the model
			for(int i = 0; i < suspended.size(); i++) {
				VirtualMachine sus = suspended.get(i);
				// we use the previous resources for suspended VMs
				ModelVM item = new ModelVM(sus, sus.getPrevRes(), null, vminit.size(), true);
				vminit.add(item);
			}
			
		}
		
		bins = pminit.toArray(new ModelPM[0]);
		
		// add the unplaced VMs to the model
		if(!input.getToBePlaced().isEmpty()) {
			List<ModelVM> convertedUnplacedVMs = new ArrayList<>();			
			for(VirtualMachine vm: input.getToBePlaced().keySet()) {
				ModelVM item = new ModelVM(vm, input.getToBePlaced().get(vm), null, vminit.size(), false);
				vminit.add(item);
				convertedUnplacedVMs.add(item);
			}			
			placeUnplacedVMs(convertedUnplacedVMs);
		}
		
		items = vminit.toArray(new ModelVM[0]);
		
		// debug without the currently instantiated model (otherwise the log gets too big)
		Logger.getGlobal().info("Instantiated " + bins.length + " PMs and " + items.length + 
				" VMs at " + Timed.getFireCount());
	}
	
	/**
	 * This method can be called after all migrations were done. It is checked which
	 * PMs do not have any VMs hosted and then this method shut them down. A node is
	 * created to add this information to the graph.
	 * 
	 * @return A list containing the ModelPMs that are shut down now.
	 */
	protected List<ModelPM> shutDownEmptyPMs() {
		// collect the PMs which should be off
		List<ModelPM> toShutDown = new ArrayList<ModelPM>();
		for (ModelPM pm : bins) {
			if (!pm.isHostingVMs() && pm.getState() != State.EMPTY_OFF) {
				toShutDown.add(pm);
				pm.switchOff(); // shut down this PM
			}
		}
		
		return toShutDown;
	}

	/**
	 * PMs that should host at least one VM are switched on.
	 * 
	 * @return A list containing all ModelPMs that are now switched on.
	 */
	protected List<ModelPM> switchOnNonEmptyPMs() {
		// collect the PMs which should be on
		List<ModelPM> toStart = new ArrayList<ModelPM>();
		for (ModelPM pm : bins) {
			if (pm.isHostingVMs() && pm.getState() == State.EMPTY_OFF) {
				toStart.add(pm);
				pm.switchOn();
			}
		}
		
		return toStart;
	}

	/**
	 * PMs that should host at least one VM are switched on; PMs that should host no
	 * VMs are switched off.
	 */
	protected void adaptPmStates() {
		shutDownEmptyPMs();
		switchOnNonEmptyPMs();
//		Logger.getGlobal().info("Amount of PMs that should be turned off: " + shutDownEmptyPMs().size());
//		Logger.getGlobal().info("Amount of PMs that should be started: " + switchOnNonEmptyPMs().size());
	}
	
	/**
	 * Helper method to place VMs on new hosts that are thrown of their previous hosts.
	 * 
	 * @param toBePlaced A list containing all ModelVMs that have to be placed.
	 * 
	 * @return An integer determining the amount of found hosts.
	 */
	private int placeUnplacedVMs(List<ModelVM> toBePlaced) {
		int foundHosts = 0;
		
		for(ModelVM mvm: toBePlaced) {
			for(ModelPM mpm: bins) {
				if(mpm.isMigrationPossible(mvm.getResources())) {
					Logger.getGlobal().info("Placed VM " + mvm.getVM().hashCode() + " on PM " + mpm.getPM().hashCode());
					
					mpm.addVM(mvm);
					foundHosts++;
					break;
				}
			}
		}
		
		if(foundHosts != toBePlaced.size())
			Logger.getGlobal().info("Not all unplaced VMs could be placed.");
		
		return foundHosts;
	}
	
	private void resumeSuspendedVMs(AnalyzerOutput input) {
		List<ModelVM> suspended = new ArrayList<ModelVM>();
		// add the suspended VMs
		for(VirtualMachine vm: input.getSuspendedVMs()) {
			for(ModelVM mvm: items) {
				if(mvm.getVM().hashCode() == vm.hashCode()) {
					suspended.add(mvm);
					break;
				}
			}
		}
		
		// resume suspended
		Logger.getGlobal().info(suspended.size() + " VMs to be resumed.");
		for(ModelVM mvm: suspended) {
			for(ModelPM possible: bins) {
				if(possible.isMigrationPossible(mvm.getResources())) {
					mvm.resume(possible);
					break;
				}
			}
			// no host can be found, VM is left suspended
			if(mvm.isSuspended()) {
				Logger.getGlobal().info("VM " + mvm.getVM().hashCode() + " could not be resumed.");
			}			
		}
	}
	
	/**
	 * Quickly find a plan for relieving highly loaded servers with 
	 * migrations or VM suspensions
	 * 
	 * We have to suspend VMs only if their load is increasing fast or there
	 * is no other PM to host migrations.
	 * 
	 * @param input The AnalyzerOutput to work with.
	 * @param output The Plan which shall be created to be executed.
	 */
	private void useAlgorithmP1(AnalyzerOutput input, Plan output) {		
		
		// collect the overloaded and possibly overloaded PMs
		List<ModelPM> overloaded = convertPMsToMPMs(new ArrayList<PhysicalMachine>(input.getOverloaded()));
		List<ModelPM> possiblyOverloaded = convertPMsToMPMs(new ArrayList<PhysicalMachine>(input.getPossibleOverloaded().keySet()));	
		
		// create the converted possibly overloaded PMs map
		Map<ModelPM, ResourceConstraints> modelpmToFutureRessourcesMapping = new HashMap<>();
		for(int i = 0; i < possiblyOverloaded.size(); i++) {
			ModelPM current = possiblyOverloaded.get(i);
			ResourceConstraints currentRes = input.getPossibleOverloaded().get(current.getPM());
			modelpmToFutureRessourcesMapping.put(current, currentRes);
		}
		
		
		
		for(ModelPM mpm: overloaded) {
			// try to migrate VMs till the given PM reaches normal load with first fit
			while(mpm.getConsumedResources().getTotalProcessingPower() > mpm.getTotalResources().
					getTotalProcessingPower() * thresholdP1 || mpm.getConsumedResources().getRequiredMemory() > 
					mpm.getTotalResources().getRequiredMemory() * thresholdP1) {
				
				ModelVM toMig = mpm.getVM(0);
				ModelPM result = findFirstFitHost(mpm, toMig);
				if(result == null) {
					// do a suspension if allowed, otherwise skip this PM
					if(allowP1suspensions) {
						// no migration possible, we have to suspend VMs
						toMig.suspend();
					}
					else {
						// because we are not allowed to suspend VMs, we have to break the loop here
						break;
					}
					
				}
				else {
					mpm.migrateVM(toMig, result);
				}
			}			
		}
		
		// try to prevent future overloaded PMs from getting overloaded
		for(ModelPM mpm: modelpmToFutureRessourcesMapping.keySet()) {
			
			AlterableResourceConstraints possibleFutureLoad = (AlterableResourceConstraints) modelpmToFutureRessourcesMapping.get(mpm);
			
			// try to migrate VMs at this point to prevent the PM from getting overloaded
			while(possibleFutureLoad.getTotalProcessingPower() > mpm.getTotalResources().
					getTotalProcessingPower() * thresholdP1 || possibleFutureLoad.getRequiredMemory() > 
					mpm.getTotalResources().getRequiredMemory() * thresholdP1) {
				
				// check if there are VMs hosted on this PM (which should always be the case)
				if(!mpm.isHostingVMs()) {
					Logger.getGlobal().info("ModelPM " + mpm.hashCode() + " is not hosting PMs anymore, "
							+ "but the predicted load is still to high. Investigate!");
					break;
				}
				
				ModelVM toMig = mpm.getVM(0);
				ModelPM result = findFirstFitHost(mpm, toMig);
				if(result == null) {					
					// do a suspension if allowed, otherwise skip this PM
					if(allowP1suspensions) {
						// no migration possible, we have to suspend VMs
						toMig.suspend();
					}
					else {
						// because we are not allowed to suspend VMs, we have to break the loop here
						break;
					}
				}
				else {
					// migrate the VM
					mpm.migrateVM(toMig, result);
					
					// adjust the other load afterwards, too
					possibleFutureLoad.subtract(toMig.getResources());
				}
						
			}
		}	
		// in this case no autoadjust is used and we try to resume suspended VMs at the end of the algorithm
		if(useDefaults) {
			resumeSuspendedVMs(input);
		}
		
	}
	
	/**
	 * Quickly find a plan to resume suspended VMs and relieve overloaded 
	 * servers.
	 * 
	 * @param input The AnalyzerOutput to work with.
	 * @param output The Plan which shall be created to be executed.
	 */
	private void useAlgorithmP2(AnalyzerOutput input, Plan output) {
		
		List<ModelPM> overloaded = convertPMsToMPMs(input.getOverloaded());		
		
		// there are no suspended VMs, so we do not need to resume VMs
		if(input.getSuspendedVMs().isEmpty()) {
			Logger.getGlobal().info("There are no suspended VMs to resume.");
		}
		else {
			List<ModelVM> suspended = new ArrayList<ModelVM>();
			// add the suspended VMs
			for(VirtualMachine vm: input.getSuspendedVMs()) {
				for(ModelVM mvm: items) {
					if(mvm.getVM().hashCode() == vm.hashCode()) {
						suspended.add(mvm);
						break;
					}
				}
			}
			
			// resume suspended
			Logger.getGlobal().info(suspended.size() + " VMs to be resumed.");
			for(ModelVM mvm: suspended) {
				for(ModelPM possible: bins) {
					if(possible.isMigrationPossible(mvm.getResources())) {
						mvm.resume(possible);
						break;
					}
				}
				// no host can be found, VM is left suspended
				if(mvm.isSuspended()) {
					Logger.getGlobal().info("VM " + mvm.getVM().hashCode() + " could not be resumed.");
				}			
			}
		}
		
		// relieve overloaded
		for(ModelPM mpm : overloaded) {
			// try to migrate VMs till the given PM reaches normal load with first fit
			while (mpm.getConsumedResources().getTotalProcessingPower() > mpm.getTotalResources().
					getTotalProcessingPower() * thresholdP2 || mpm.getConsumedResources().getRequiredMemory() > 
					mpm.getTotalResources().getRequiredMemory() * thresholdP2) {
				
				// check if there are VMs hosted on this PM (which should always be the case)
				if(!mpm.isHostingVMs()) {
					Logger.getGlobal().info("ModelPM " + mpm.hashCode() + " is not hosting PMs anymore, "
							+ "but the predicted load is still to high. Investigate!");
					break;
				}

				ModelVM toMig = mpm.getVM(0);
				ModelPM result = findFirstFitHost(mpm, toMig);
				if (result == null) {
					// no migration possible, we have to suspend VMs
					toMig.suspend();
				} 
				else {
					mpm.migrateVM(toMig, result);
				}
			}

		}
		
		
	}

	/**
	 * Find the globally best allocation of VMs to servers
	 * 
	 * We use gurobi for this algorithm (IlpConsolidator).
	 * 
	 * @param input The AnalyzerOutput to work with.
	 * @param output The Plan which shall be created to be executed.
	 */
	private void useAlgorithmP3(AnalyzerOutput input, Plan output) {		
		// use Gurobi for optimization
		IlpConsolidator cons = new IlpConsolidator(bins, items, input, gurobiTimeLimit, correlationThreshold, 
				thresholdGurobiPMs, output.isA3SizeInputUsed());
		List<Map<String, Object>> map = cons.optimizeAllocation();
		
		if(map == null) {
			Logger.getGlobal().info("VM-PM mapping is null, first fit is used");
			useFirstFit();
		}
		else {
			for(Map<String, Object> entry: map) {
				ModelVM vm = (ModelVM) entry.get("vm");
				ModelPM pm = (ModelPM) entry.get("pm");
				
				// check whether the found pm exists
				if(pm == null) {
					Logger.getGlobal().info("VM-PM mapping is wrong, target PM is null!");
					useFirstFit();
					break;
				}
				else {					
					vm.gethostPM().migrateVM(vm, pm);
//					Logger.getGlobal().info("Migrated VM " + vm.hashCode() + " to PM " + pm.hashCode() + ".");
				}
			}
		}
		
	}

	/**
	 * Helper-algorithm to solve the optimization problem with a simple first fit algorithm.
	 */
	private void useFirstFit() {
		
		final ArrayList<ModelVM> tempvmlist = new ArrayList<>();
		// relieve overloaded PMs + empty underloaded PMs
		for (final ModelPM pm : bins) {
			final List<ModelVM> vmsOfPm = pm.getVMs();
			int l = vmsOfPm.size() - 1;
			while (l >= 0 && (pm.isOverAllocated() || pm.isUnderAllocated())) {
				final ModelVM vm = vmsOfPm.get(l--);
				tempvmlist.add(vm);
				pm.removeVM(vm);
			}
		}
		
		// find new hosts for the VMs to migrate using First Fit
		for(ModelVM toMigrate: tempvmlist) {
			ModelPM target = null;
			
			for(ModelPM pm: bins) {
				if(pm.isMigrationPossible(toMigrate.getResources())) {
					target = pm;					
					break;
				}
			}
			
			// if there is a new host, we put the VM onto it, otherwise we put it back
			// onto the previous host
			if(target != null)
				target.addVM(toMigrate);
			else {
				Logger.getGlobal().info("VM " + toMigrate.hashCode() + " with resources " + toMigrate.getResources() + 
						" could not be migrated to a new host using first fit.");
				toMigrate.getInitialHost().addVM(toMigrate);
			}
				
		}
	}
	
	/**
	 * Devise a set of local actions to address changes since the last 
	 * invocation
	 * 
	 * Used together with A4. We get the object with the last changes and
	 * improve the situation here, meaning doing migrations or suspensions.
	 * 
	 * @param input The AnalyzerOutput to work with.
	 * @param output The Plan which shall be created to be executed.
	 */
	private void useAlgorithmP4(AnalyzerOutput input, Plan output) {
		
		// in the first iteration we aim for getting the load under the defined thresholds
		if(AACHelper.iterationCount == 0) {
			for(ModelPM current: bins) {
				while(current.getConsumedResources().getTotalProcessingPower() > current.getTotalResources().
						getTotalProcessingPower() * thresholdP4 || current.getConsumedResources().getRequiredMemory() > 
						current.getTotalResources().getRequiredMemory() * thresholdP4 ) {
								
					ModelVM toMig = current.getVM(0);
					ModelPM result = findFirstFitHost(current, toMig);
					if(result == null) {						
						// do a suspension if allowed, otherwise skip this PM
						if(allowP4suspensions) {
							// no migration possible, we have to suspend VMs
							toMig.suspend();
						}
						else {
							// because we are not allowed to suspend VMs, we have to break the loop here
							break;
						}
					}
					else {
						current.migrateVM(toMig, result);
					}
				}
			}
			
			return;		// skip
		}
	
		// if there has been no analysis, we cannot plan changes
		if(input.getSignificantIncreasingPMs().isEmpty() && input.getSignificantDecreasingPMs().isEmpty()) {
			Logger.getGlobal().info("P4 cannot plan without changes.");
		}
		else {
			// check if we have PMs with increasing load
			if(!input.getSignificantIncreasingPMs().isEmpty()) {
				List<ModelPM> increased = convertPMsToMPMs(input.getSignificantIncreasingPMs());
							
				// try to do migrations until the defined threshold
				for(ModelPM current: increased) {					
					while(current.getConsumedResources().getTotalProcessingPower() > current.getTotalResources().
							getTotalProcessingPower() * thresholdP4 || current.getConsumedResources().getRequiredMemory() > 
							current.getTotalResources().getRequiredMemory() * thresholdP4 ) {
									
						ModelVM toMig = current.getVM(0);
						ModelPM result = findFirstFitHost(current, toMig);
						if(result == null) {						
							// do a suspension if allowed, otherwise skip this PM
							if(allowP4suspensions) {
								// no migration possible, we have to suspend VMs
								toMig.suspend();
							}
							else {
								// because we are not allowed to suspend VMs, we have to break the loop here
								break;
							}
						}
						else {
							current.migrateVM(toMig, result);
						}
					}
				}				
			}	
			
			// check if we have PMs with decreasing load
			if(!input.getSignificantDecreasingPMs().isEmpty()) {
				List<ModelPM> decreased = convertPMsToMPMs(input.getSignificantDecreasingPMs());				
				// try to empty the pm
				for(ModelPM current: decreased) {
					// if the PM cannot be completely emptied, all migrations are undone
					Map<ModelVM, ModelPM> migrated = new HashMap<>();
					for(ModelVM toMigrate: current.getVMs()) {
						ModelPM host = findFirstFitHost(current, toMigrate);
						if(host != null) {
							migrated.put(toMigrate, host);
							host.reserveResources(toMigrate.getResources());
						}
						else {
							break;
						}
					}
					
					boolean doMigrations = (migrated.size() == current.getVMs().size());
					for(ModelVM curr: migrated.keySet()) {
						migrated.get(curr).removeReservedResources(curr.getResources());
						if(doMigrations)
							current.migrateVM(curr, migrated.get(curr));
					}
					
				}				
				
			}

		}
		
	}
	
	/**
	 * Helper to find the correct ModelPMs for each PM out of the AnalyzerOutput.
	 * 
	 * @param toConvert A list containing the PhysicalMachines that have to be converted.
	 * 
	 * @return The converted list containing the respective ModelPMs.
	 */
	private List<ModelPM> convertPMsToMPMs(List<PhysicalMachine> toConvert) {
		int converted = 0;
		
		List<ModelPM> result = new ArrayList<>();
		for(PhysicalMachine pm: toConvert) {
			for(ModelPM mpm: bins) {
				if(mpm.getPM().equals(pm)) {
					result.add(mpm);
					converted++;
					break;
				}
			}
		}
		
		if(converted != toConvert.size()) {
			Logger.getGlobal().info("Conversion of PMs to MPMs was not successful, " + 
					(toConvert.size() - converted) + " MPMs could not be found.");
		}
		
		return result;
	}
	
	/**
	 * Finds a possible host for a given ModelVM according to first fit.
	 * 
	 * @param pm The ModelPM hosting the VM.
	 * @param vm The ModelVM to be migrated.
	 * 
	 * @return The first host capable of hosting the VM, null if there is none.
	 */
	private ModelPM findFirstFitHost(ModelPM pm, ModelVM vm) {	
		
		// check if the VM is running
		if(vm.getResources() == null) {
			Logger.getGlobal().info("Ressources are null for VM " + vm.hashCode());
		}
		else {
			for(ModelPM bin: bins) {
				// check whether this is the same ModelPM
				if(bin == pm) {
					// there is no need to test this one
					continue;
				}
				else if(bin.isMigrationPossible(vm.getResources())) {
					return bin;
				}
			}
		}		
		
		return null;
	}
	
	/**
	 * Getter for the duration of the current planning run. Is intended to be called 
	 * after the planner has finished.
	 * 
	 * @return The duration of the current P-algorithm in milliseconds.
	 */
	public long getDuration() {
		return duration;
	}
	
	/**
	 * The toString()-method, used for debugging. It prints the full created
	 * model and additionally the suspended VMs, if there are any.
	 */
	public String toString() {
		String result = "";
		boolean first = true;
		for (ModelPM bin : bins) {
			if (!first)
				result = result + "\n";
			result = result + bin.toString();
			first = false;
		}
		
		// now append the suspended VMs
		result = result + "Additional suspended VMs: ";
		for(ModelVM vm: items) {
			if(vm.isSuspended()) {
				result = result + "VM " + vm.toString() + "\n";
			}
		}
		return result;
	}
	
	@SuppressWarnings("unused")
	private String getLoadBeforeOptimization() {
		int pmCounter = 0;
		String result = "", result2 = "";
		
		
		List<ModelPM> orderedBins = new ArrayList<ModelPM>();
		for(ModelPM pm: bins) {
			orderedBins.add(pm);
		}
		Collections.sort(orderedBins, (a, b) -> a.hashCode() < b.hashCode() ? -1 : a.hashCode() == b.hashCode() ? 0 : 1);
		
		for(ModelPM pm: orderedBins) {
			if(pm.isHostingVMs()) {
				pmCounter++;
				
				result2 += "PM " + pm.hashCode() + " upperThreshold: [C:" + 
						pm.getUpperThreshold().getRequiredCPUs() + " P:" + pm.getUpperThreshold().getRequiredProcessingPower() + 
						" M:" + pm.getUpperThreshold().getRequiredMemory() + ", used: [C:" + pm.getConsumedResources().getRequiredCPUs() + 
						" P:" + pm.getConsumedResources().getRequiredProcessingPower() + " M:" + 
						pm.getConsumedResources().getRequiredMemory() + "]";
				
				// determine the real used resources
				if(isA3SizeInputUsed) {
					AlterableResourceConstraints realConsumed = AACHelper.getNoResources();
					
					// add up all VMs
					for(ModelVM vm: pm.getVMs()) {
						realConsumed.singleAdd(vm.getVM().getResourceAllocation().allocated);
					}
					
					result2 += ", real consumed: [C:" + realConsumed.getRequiredCPUs() + " P:" + 
							realConsumed.getRequiredProcessingPower() + " M:" + realConsumed.getRequiredMemory() + "]";
				}
						
				result2 += "\n";
			}
				
		}
		result = "getLoadBeforeOptimization(), total amount of running PMs: " + pmCounter + "\n";
		result += result2;
		
		return result;
	}
	
	@SuppressWarnings("unused")
	private String getLoadAfterOptimization() {
		int pmCounter = 0;
		String result = "", result2 = "";
		
		
		List<ModelPM> orderedBins = new ArrayList<ModelPM>();
		for(ModelPM pm: bins) {
			orderedBins.add(pm);
		}
		Collections.sort(orderedBins, (a, b) -> a.hashCode() < b.hashCode() ? -1 : a.hashCode() == b.hashCode() ? 0 : 1);
		
		for(ModelPM pm: orderedBins) {
			if(pm.isHostingVMs()) {
				pmCounter++;
				
				result2 += "PM " + pm.hashCode() + " upperThreshold: [C:" + 
						pm.getUpperThreshold().getRequiredCPUs() + " P:" + pm.getUpperThreshold().getRequiredProcessingPower() + 
						" M:" + pm.getUpperThreshold().getRequiredMemory() + ", used: [C:" + pm.getConsumedResources().getRequiredCPUs() + 
						" P:" + pm.getConsumedResources().getRequiredProcessingPower() + " M:" + 
						pm.getConsumedResources().getRequiredMemory() + "]";
				
				// determine the real used resources
				if(isA3SizeInputUsed) {
					AlterableResourceConstraints realConsumed = AACHelper.getNoResources();
					
					// add up all VMs
					for(ModelVM vm: pm.getVMs()) {
						realConsumed.singleAdd(vm.getVM().getResourceAllocation().allocated);
					}
					
					result2 += ", real consumed: [C:" + realConsumed.getRequiredCPUs() + " P:" + 
							realConsumed.getRequiredProcessingPower() + " M:" + realConsumed.getRequiredMemory() + "]";
				}
						
				result2 += "\n";
			}
				
		}
		result = "getLoadAfterOptimization(), total amount of running PMs: " + pmCounter + "\n";
		result += result2;
		
		return result;
	}

}
