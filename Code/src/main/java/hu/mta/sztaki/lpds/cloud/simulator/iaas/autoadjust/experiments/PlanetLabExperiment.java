package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.experiments;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import hu.mta.sztaki.lpds.cloud.simulator.energy.powermodelling.PowerState;
import hu.mta.sztaki.lpds.cloud.simulator.energy.specialized.IaaSEnergyMeter;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine.ResourceAllocation;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine.State;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VMManager.VMManagementException;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine.StateChangeException;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelPM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelVM;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.actions.Action;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AACHelper;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AutoAdjustingConsolidator;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.ModelVmIdComparator;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.VmIdComparator;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.consolidation.Consolidator;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.AlterableResourceConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.helpers.PMComparators;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.pmscheduling.ConsolidationFriendlyPmScheduler;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.pmscheduling.PhysicalMachineController;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.resourcemodel.ConsumptionEventAdapter;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.resourcemodel.ResourceConsumption;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.resourcemodel.ResourceSpreader;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.vmscheduling.FirstFitScheduler;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.vmscheduling.Scheduler;
import hu.mta.sztaki.lpds.cloud.simulator.io.NetworkNode.NetworkException;
import hu.mta.sztaki.lpds.cloud.simulator.io.Repository;
import hu.mta.sztaki.lpds.cloud.simulator.io.VirtualAppliance;
import hu.mta.sztaki.lpds.cloud.simulator.util.PowerTransitionGenerator;

/**
 * 9 combinations to test, namely: A1 - P1, F1 to A4 - P4, F2 plus only autoadjust
 */
	 
public class PlanetLabExperiment extends Timed {
	
	/**
	 * Inside the log the structure is the following:
	 * 		  VM1 | VM2 | VM3 | VM4
	 * ----------------------------
	 * It.1 | Val | Val | Val | Val
	 * It.2 | Val | Val | Val | Val
	 * It.3 | Val | Val | Val | Val
	 */
	private String loadOutputFile = "VmLoads.csv";
	// debug string
	private String allocateLoads = "";
		
	private long freq = (long) MyTime.sec2tick(5 * 60); // frequency for consolidation (300 sec)
	
	private final static int numPms = 40;
	private final int numVms = 150;
	
	private String inputFolderName;
	private final static int timeIntervalSec = 300; //5 minutes
	//private final static int timeIntervalSec=3600; //1 hour
//	private final static int numSamples = 288; //24*60/5		// large scale
	private final static int numSamples = 20;		// small scale
	private boolean first = true;
	
	/** PM info */
	private final static int numPmTypes = 2;
	private final static int[] pmCores = { 2, 2 };
//	private final static int[] pmMips = { 1860, 2660 };
	private final static int[] pmMips = { 2660, 2660 };
//	private final static int[] pmCores = { 4, 4 };
	private final static long[] pmRamMb = { 4096, 4096 };
		
	/** VM structures containing all VMs and their type */
	private SortedMap<VirtualMachine, Integer> sortedVmTypes = new TreeMap<>(new VmIdComparator());
	private Map<VirtualMachine, PhysicalMachine> vmToHostMapping = new HashMap<>();
	
	/** general VM info */
	private VirtualAppliance va;	
	private final static int numVmTypes = 4;
	private final static int[] vmMips = { 2500, 2000, 1000, 500 };
	private final static int[] vmCores = { 1, 1, 1, 1 };
//	private final static int[] vmRamMb = { 870,  1740, 1740, 613 };
	private final static int[] vmRamMb = { 1, 1, 1, 1 };
	
	/** determines the highest MIPS of all machines to allow calculations without conversion */
	private double highestMips = 0.0;
	
	
	/** determines whether default values should be used or autoadjust */
	private boolean useDefaults;
	/** determines whether the important information for each iteration should also be logged separately */
	private boolean doExtendedLogging;
	
	/** consolidation objects */
	private Repository centralRepo;
	private IaaSService iaas;
	private AutoAdjustingConsolidator cons;
	private PhysicalMachineController pmScheduler;
	
	/** workload and sample state */
	private int workload[][];
	private int currSample = 0;
	
	/** metering */
	private IaaSEnergyMeter energyMeter;
	private static double oldEnergy = 0;
	private final static long meteringInterval = MyTime.sec2tick(200);
	public static long duration;
	
	/** stores the broken VMs, that are unplaced and have to be replaced */
	private Map<VirtualMachine, ResourceConstraints> unplacedVMs = new HashMap<>();
	
	/** full results inside defined class */
	public ExperimentResults results;
	
	/**
	 * Instantiates an instance of this experiment and runs the simulation with
	 * previously defined parameters.
	 * 
	 * The first argument is used to determine if autoadjust has to be used or not. The
	 * second argument decides about the usage of the Logger and the third one indicates
	 * the location of the traces.
	 * 
	 * @param args[0] Determines whether defaults should be used.
	 * @param args[1] Determines whether general logging should be activated.
	 * @param args[2] Contains the path to the trace to be used.
	 * @param args[3] Determines whether extended logging (logging the autoadjust decisions 
	 * 					and loads of each iteration) should be activated
	 * 
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {		
		long startTime = System.currentTimeMillis();
		System.setProperty("java.util.logging.SimpleFormatter.format","%4$s [%1$tH:%1$tM:%1$tS] %2$s: %5$s%n");
		Handler logFileHandler;
		try {
			logFileHandler = new FileHandler("log.txt");
			logFileHandler.setFormatter(new SimpleFormatter());
			Logger.getGlobal().addHandler(logFileHandler);
		} catch (Exception e) {
			System.out.println("Could not open log file for output" + e);
			System.exit(-1);
		}
		
		// instantiate an instance of this class
		PlanetLabExperiment inst;
		/** only doing the experiments without logging anything */
		if(args.length == 1) {
			boolean firstArg = Boolean.parseBoolean(args[0]);
			inst = new PlanetLabExperiment(firstArg);
		}
		/** normal execution type, sets all usable parameters */
		else if(args.length >= 4) {
			boolean firstArg = Boolean.parseBoolean(args[0]);
			boolean secondArg = Boolean.parseBoolean(args[1]);
			String thirdArg = args[2];
			boolean fourthArg = Boolean.parseBoolean(args[3]);
			inst = new PlanetLabExperiment(firstArg, secondArg, thirdArg, fourthArg);
		}
		else {
			Logger.getGlobal().info("Wrong amount of arguments.");
			inst = new PlanetLabExperiment(false);
		}
		
		inst.init();
		
		// the actual simulation
		Timed.simulateUntilLastEvent();
		
		// check if there are pending actions, then print them here
		List<Action> pendingActions = ((AutoAdjustingConsolidator) inst.getConsolidator()).getExecuter().getPendingActions();
		if(pendingActions.size() != 0) {
			int resume = 0, suspend = 0, migrate = 0, start = 0, stop = 0;
			
			// count each pending type
			for(Action action: pendingActions) {
				switch(action.getType()) {
				case MIGRATION:
					migrate++;
					break;
				case PMSHUTDOWN:
					stop++;
					break;
				case PMSTART:
					start++;
					break;
				case SUSPENSION:
					suspend++;
					break;
				case VMRESUME:
					resume++;
					break;
				default:
					Logger.getGlobal().info("Unkown Action Type: " + action.getType());
					break;				
				}
			}
			
			// write the result for each type, that is pending
			String result = "Pending actions:";
			
			if(migrate != 0) {
				result += " MIG=" + migrate;
			}
			if(stop != 0) {
				result += " STOP=" + stop;
			}
			if(start != 0) {
				result += " START=" + start;
			}
			if(suspend != 0) {
				result += " SUS=" + suspend;
			}
			if(resume != 0) {
				result += " RES=" + resume;
			}
			
			Logger.getGlobal().info(result);
			
		}
		
		// check if there are still suspended VMs, than measure the time
		if(!inst.iaas.listSuspendedVMs().isEmpty()) {
			for(VirtualMachine current: inst.iaas.listSuspendedVMs().keySet()) {
				AACHelper.addUpSuspendedTime(current.stopTimerAndReturn());
			}
		}
		
		duration = System.currentTimeMillis() - startTime;
		Logger.getGlobal().info("Duration in seconds: " + TimeUnit.SECONDS.convert(duration, TimeUnit.MILLISECONDS) + ".");
		// decisions of autoadjust
		Logger.getGlobal().info("" + AutoAdjustingConsolidator.decisionsToString());
		
		// prepare the results
		ExperimentResults results = inst.getResults();
		if(((AutoAdjustingConsolidator)inst.getConsolidator()).isAutoAdjustUsed()) {
			results.setSimulationConstraints(true, null, null, null);
		}
		else {
			results.setSimulationConstraints( ((AutoAdjustingConsolidator)inst.getConsolidator()).
					isAutoAdjustUsed(), ((AutoAdjustingConsolidator)inst.getConsolidator()).getAnalyzeParameter(), 
					((AutoAdjustingConsolidator)inst.getConsolidator()).getPlanningParameter(), 
					((AutoAdjustingConsolidator)inst.getConsolidator()).getFrequencyParameter());
		}
		
		// print and log the results
		System.out.print(results.toString());
		Logger.getGlobal().info(results.toString());		
		
		// reset the consolidator		
		AutoAdjustingConsolidator.resetAll();
	}
	
	/**
	 * Instantiates an instance of this experiment and runs the simulation with
	 * previously defined parameters.
	 * 
	 * The first argument is used to determine if autoadjust has to be used or not. The
	 * second argument decides about the usage of the Logger and the third one indicates
	 * the location of the traces.
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static ExperimentResults runOneExperiment(String[] args) throws Exception {		
		long startTime = System.currentTimeMillis();
		System.setProperty("java.util.logging.SimpleFormatter.format","%4$s [%1$tH:%1$tM:%1$tS] %2$s: %5$s%n");
		Handler logFileHandler;
		try {
			logFileHandler = new FileHandler("log.txt");
			logFileHandler.setFormatter(new SimpleFormatter());
			Logger.getGlobal().addHandler(logFileHandler);
		} catch (Exception e) {
			System.out.println("Could not open log file for output" + e);
			System.exit(-1);
		}
		
		// instantiate an instance of this class
		PlanetLabExperiment inst;
		if(args.length == 1) {
//			Logger.getGlobal().info("First argument: " + args[0] + ".");
			boolean firstArg = Boolean.parseBoolean(args[0]);
			inst = new PlanetLabExperiment(firstArg);
		}
		else if(args.length == 4) {
			boolean firstArg = Boolean.parseBoolean(args[0]);
			boolean secondArg = Boolean.parseBoolean(args[1]);
			String thirdArg = args[2];
			boolean fourthArg = Boolean.parseBoolean(args[3]);
			inst = new PlanetLabExperiment(firstArg, secondArg, thirdArg, fourthArg);
		}
		else {
			Logger.getGlobal().info("Wrong amount of arguments: " + args.length);
			inst = new PlanetLabExperiment(false);
		}
		
		inst.init();
		
		// the actual simulation
		Timed.simulateUntilLastEvent();
		
		// check if there are still suspended VMs, than measure the time
		if(!inst.iaas.listSuspendedVMs().isEmpty()) {
			for(VirtualMachine current: inst.iaas.listSuspendedVMs().keySet()) {
				AACHelper.addUpSuspendedTime(current.stopTimerAndReturn());
			}
		}
				
		duration = System.currentTimeMillis() - startTime;
		System.out.println("Duration in seconds: " + TimeUnit.SECONDS.convert(duration, TimeUnit.MILLISECONDS));
		// decisions of autoadjust
		Logger.getGlobal().info("" + AutoAdjustingConsolidator.decisionsToString());
		
		// prepare the results
		ExperimentResults results = inst.getResults();
		
		if(((AutoAdjustingConsolidator)inst.getConsolidator()).isAutoAdjustUsed()) {
			results.setSimulationConstraints(true, null, null, null);
		}
		else {
			results.setSimulationConstraints( ((AutoAdjustingConsolidator)inst.getConsolidator()).
					isAutoAdjustUsed(), ((AutoAdjustingConsolidator)inst.getConsolidator()).getAnalyzeParameter(), 
					((AutoAdjustingConsolidator)inst.getConsolidator()).getPlanningParameter(), 
					((AutoAdjustingConsolidator)inst.getConsolidator()).getFrequencyParameter());
		}
		
		// print and log the results
		System.out.print(results.toString());
		Logger.getGlobal().info(results.toString());		
		
		
		// reset the consolidator		
		AutoAdjustingConsolidator.resetAll();
		
		return results;
	}
	
	/**
	 * Default constructor. The parameter is used to determine whether default parameters for the 
	 * AutoAdjustingConsolidator shall be used or not.
	 * 
	 * @param defaults Determines whether default parameters for consolidation shall be used.
	 */
	public PlanetLabExperiment(boolean defaults) {
		useDefaults = defaults;
	}
	
	/**
	 * Constructor for the final experiments, deactivates logging.
	 * 
	 * @param defaults Determines whether default parameters for consolidation shall be used.
	 * @param useLogging Determines whether logging should be active.
	 * @param path The path to the traces.
	 */
	public PlanetLabExperiment(boolean defaults, boolean useLogging, String path, boolean doExtendedLogging) {
		useDefaults = defaults;
		inputFolderName = path;
		this.doExtendedLogging = doExtendedLogging;
		if(!useLogging) {
			Logger.getGlobal().setLevel(Level.OFF);
		}
		else {
			Logger.getGlobal().setLevel(Level.ALL);
		}
	}
	
	/**
	 * Initializes the experiment by creating all necessary objects and preparing 
	 * the cloud infrastructure.
	 * 
	 * @throws Exception
	 */
	private void init() throws Exception {
		
		// reset the Timed
		Timed.resetTimed();
		PlanetLabExperiment.resetTimed();
		
		// determine the highest Mips
		highestMips = determineHighestMips();
		
		// determine the amount of to be used VMs
//		File inputFolder = new File(inputFolderName);
//		numVms = inputFolder.listFiles().length;
		
		// create and initialize all objects of the simulation
		createCloud();
		createVMs();
		Timed.simulateUntilLastEvent();
		readWorkload();
		allocateVms();
		Timed.simulateUntilLastEvent();
		
		// fill the mappings to allow better debugging at this point
		fillMapping();
		
		// create the consolidator at the end to ensure all VMs are started at this point
		if(!areVMsStartedAndHosted()) {
			Logger.getGlobal().info("There are VMs that are not in RUNNING-state and/or not hosted.");
		}
		
		createConsolidator();
		writeLogfile(true, allocateLoads);
		
		// start the energy meter before the first period
		energyMeter.startMeter(meteringInterval, true);	
		
		startPeriod();		
		
		// here is the subscription
		if(subscribe(MyTime.sec2tick(timeIntervalSec))) {
			Logger.getGlobal().info("Subscription was successful!");
		}
		else {
			Logger.getGlobal().info("Subscription was not successful...");
		}
		
	}
	
	/**
	 * Helper to check whether the allocation and state are still correct of each VM.
	 * 
	 * @return
	 */
	private boolean areVMsStartedAndHosted() {
		for(VirtualMachine vm: iaas.listVMs()) {
//			Logger.getGlobal().info("hosts of VM " + vm.hashCode() + ": " + vm.getResourceAllocation().
//					getHost());
			if(vm.getState().equals(VirtualMachine.State.SUSPENDED)) {
				Logger.getGlobal().info("VM " + vm.hashCode() + " is suspended, but in normal vmlist");
			}
			else {
				if(vm.getResourceAllocation().getHost().listVMs().size() == 0) {
					Logger.getGlobal().info("Host of VM: PM" + vm.getResourceAllocation().getHost().hashCode());
					return false;
				}
				
//				Logger.getGlobal().info(vm.toString());
				if(vm.getState().equals(VirtualMachine.State.INITIAL_TR) || vm.getState()
						.equals(VirtualMachine.State.STARTUP)) {
					return false;
				}
			}			
						
		}
		return true;
	}
	
	/**
	 * Instantiates all important objects to prepare the experiment.
	 * 
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 */
	private void createCloud() throws InstantiationException, IllegalAccessException, IllegalArgumentException, 
		InvocationTargetException, NoSuchMethodException, SecurityException, NoSuchFieldException {
		
		// prepare the schedulers for the PMs and VMs, respectively
		Class<? extends Scheduler> vmSched = FirstFitScheduler.class;
		Class<? extends PhysicalMachineController> pmSched = ConsolidationFriendlyPmScheduler.class;
		Timed.simulateUntilLastEvent();
				
		// creating one cloud for the experiments
		int clid = 0;
		System.err.println("Scaling datacenter to " + numPms + " nodes with " + pmCores[0] + " cpu cores each");
		
		// Default constructs
		HashMap<String, Integer> latencyMapRepo = new HashMap<String, Integer>(numPms + 2);
		HashMap<String, Integer> latencyMapMachine = new HashMap<String, Integer>(numPms + 2);
		iaas = new IaaSService(vmSched, pmSched);
		pmScheduler = iaas.pmcontroller;
						
		final String repoid = clid + "VHStorageDell";
		final String machineid = clid + "VHNode";
			
		// Specification of the default power behavior
		final EnumMap<PowerTransitionGenerator.PowerStateKind, Map<String, PowerState>> transitions = PowerTransitionGenerator
				.generateTransitions(20, 296, 493, 50, 108);
		final Map<String, PowerState> cpuTransitions = transitions
				.get(PowerTransitionGenerator.PowerStateKind.host);
		final Map<String, PowerState> stTransitions = transitions
				.get(PowerTransitionGenerator.PowerStateKind.storage);
		final Map<String, PowerState> nwTransitions = transitions
				.get(PowerTransitionGenerator.PowerStateKind.network);

		// scaling the bandwidth according to the size of the cloud
		final double bwRatio = (pmCores[0] * numPms) / (7f * 64f);
			
		// A single repository will hold 36T of data
//		centralRepo = new Repository(36000000000000l, repoid, (long) (bwRatio * 1250000), (long) (bwRatio * 1250000),
//				(long) (bwRatio * 250000), latencyMapRepo, stTransitions, nwTransitions);
		
		// new version with double bandwidth
		centralRepo = new Repository(36000000000000l, repoid, (long) (bwRatio * 2500000), (long) (bwRatio * 2500000),
				(long) (bwRatio * 500000), latencyMapRepo, stTransitions, nwTransitions);
		iaas.registerRepository(centralRepo);
		latencyMapMachine.put(repoid, 5); // 5 ms latency towards the repos

		// Creating the PMs for the cloud
		List<PhysicalMachine> completePMList = new ArrayList<PhysicalMachine>(numPms);
		for(int i = 1; i <= numPms; i++) {
				
			// determine the type 
			int pmType = i % numPmTypes;
				
			String currid = machineid + i;
			Repository repo = new Repository(36000000000000l, currid, (long) (bwRatio * 1250000), (long) (bwRatio * 1250000),
					(long) (bwRatio * 250000), latencyMapMachine, stTransitions, nwTransitions);
//			PhysicalMachine pm = new PhysicalMachine(pmCores[pmType], pmMips[pmType], pmRamMb[pmType],
//					repo, 89000, 29000, cpuTransitions);
			
			// new version with less starting and shut down time
			PhysicalMachine pm = new PhysicalMachine(pmCores[pmType], pmMips[pmType], pmRamMb[pmType],
					repo, 
					0, // time delay before switching on
					0, // time delay before shutting down
					cpuTransitions);
//			PhysicalMachine pm = new PhysicalMachine(pmCores[pmType], pmMips[pmType], pmRamMb[pmType],
//					repo, 44500, 14500, cpuTransitions);
			
//			latencyMapRepo.put(currid, 5);
//			latencyMapMachine.put(currid, 3);
			
			// decreased by one
			latencyMapRepo.put(currid, 4);
			latencyMapMachine.put(currid, 2);
			completePMList.add(pm);
				
			// start the PM directly
			pm.turnon();
		}
			
		// registering the hosts and the IaaS service
		iaas.bulkHostRegistration(completePMList);
				
		// instantiate the energy meter after the preparations
		energyMeter = new IaaSEnergyMeter(iaas);		
			
		// Wait until the PM Controllers finish their initial activities
		Timed.simulateUntilLastEvent();
		
		// create the virtual appliance for the VMs for preparation 
		va = new VirtualAppliance("test", 30, 0, false, 100000000);
		centralRepo.registerObject(va);
	}
	
	/**
	 * Create the consolidator with the parsed arguments of the main()-method.
	 */
	private void createConsolidator() {
		// instantiation of the the consolidator with the created iaas and the standard frequency
		if(useDefaults) {
			cons = new AutoAdjustingConsolidator(iaas, freq, true, true, doExtendedLogging);
		}
		else {
			cons = new AutoAdjustingConsolidator(iaas, freq, false, true, doExtendedLogging);
		}
		
	}
	
	/**
	 * Does the simulation. We start and end periods here to monitor the results after
	 * a specified time interval.
	 */
	@Override
	public void tick(long fires) {
		Logger.getGlobal().info("Tick called, sample: " + (currSample + 1) + ", fires: " + fires);
		endPeriod();
		
		// debug
		// Logger.getGlobal().info("PLE freq: " + super.getFrequency() + ", AAC freq: " + cons.getCurrentFrequency());
		
		if(currSample < numSamples) {
			try {
				startPeriod();
			} catch (VMManagementException e) {
				System.err.println("Unexpected error while starting period " + (currSample + 1));
				e.printStackTrace();
			}
		}
		else {
			unsubscribe();
			energyMeter.stopMeter();
			
			// reset the Timed
			Timed.resetTimed();
			PlanetLabExperiment.resetTimed();
			
			// reset the hashCodeCounter
			ResourceSpreader.resetHashCode();
		}
		
//		Logger.getGlobal().info("Tick finished, sample: " + currSample + ", fires: " + fires);
	}
	
	/**
	 * Starts a period of simulating.
	 * @throws VMManagementException 
	 * 
	 * @throws Exception
	 */
	private void startPeriod() throws VMManagementException {
//		Logger.getGlobal().info("startPeriod() called");
		
//		AlterableResourceConstraints totalBefore = calculateLoad();
//		Logger.getGlobal().info("Calculated total Load before startPeriod(): " + totalBefore + 
//				", in tpp: " + totalBefore.getTotalProcessingPower());
		
		/** after the first period VMs with a wrong allocation are collected and passed to the consolidator */
		if(!first) {
			doVmSizing();
		}
		first = false;
			
//		ResourceConstraints temp = calculateLoad();
//		Logger.getGlobal().info("Calculated total Load after doVmSizing(): " + temp + 
//				", in tpp: " + temp.getTotalProcessingPower());
		
		Timed.simulateUntil(Timed.getFireCount()+1);
		
		// this is not the case anymore
		if(!unplacedVMs.isEmpty()) {
			// debugging the unplaced VMs
			Logger.getGlobal().info("There are " + unplacedVMs.size() + " unplaced VMs, sending to the Consolidator.");
			for(VirtualMachine unplaced: unplacedVMs.keySet()) {
				Logger.getGlobal().info(unplaced.toString());
			}
			cons.passUnplacedVMs(unplacedVMs);
		}
		
		// replace the current task of each vm with a new one considering the current resources
		int errors = setComputeTasks();
		if(errors > 0)
			Logger.getGlobal().info(errors + " new ComputeTasks could not be set.");
		
		energyMeter.stopMeter();
		energyMeter.startMeter(meteringInterval, false);
//		Logger.getGlobal().info(toString());
	}

	/**
	 * Writes the current state of the simulation and updates the energy consumption.
	 */
	private void endPeriod() {
		Logger.getGlobal().info("endPeriod() called");
//		Logger.getGlobal().info(toString());
		energyReading();
		
//		AlterableResourceConstraints totalAfter = calculateLoad();
//		Logger.getGlobal().info("Calculated total Load after startPeriod(): " + totalAfter + 
//				", in tpp: " + totalAfter.getTotalProcessingPower());
		
		// clear the list of unplaced VMs and increase the sample counter
		unplacedVMs.clear();
		currSample++;
	}
	
	/**
	 * Reads all files inside one folder of the PlanetLab-workloads.
	 * 
	 * @throws Exception
	 */
	private void readWorkload() throws Exception {
		workload = new int[numVms][numSamples];
		File inputFolder = new File(inputFolderName);
		File[] files = inputFolder.listFiles();
		int vmIndex = 0;
		for(File file : files) {
			
			// do not read the .gitignore
			if(file.getName().equals(".gitignore"))
				continue;
			
			BufferedReader input = new BufferedReader(new FileReader(file));			
			for (int i = 0; i < numSamples; i++) {
				workload[vmIndex][i] = Integer.valueOf(input.readLine());
			}
			input.close();
			vmIndex++;
			
			if(vmIndex >= numVms)
				break;
		}
		
		Logger.getGlobal().info("Finished reading of the workload.");
	}
	
	/**
	 * Creates all VMs. To remember the type of each VM, they are saved with it inside
	 * a map.
	 * 
	 * @throws Exception
	 */
	private void createVMs() throws Exception {
		sortedVmTypes.clear();
//		Logger.getGlobal().info("numVms: " + numVms);
		
		// determine the vm types by using the amount of VMs divided through the amount of types,
		// if we have a remainder than there will be one more vm of the first types until we have all VMs
		// created
		int remainder = numVms % numVmTypes;
		int base = numVms / numVmTypes;
		for(int i = 0; i < numVmTypes; i++) {
			int j = 1;
			if(remainder > 0) {
				j = 0;
				remainder--;
			}
				
			while(j < base + 1) {
				VirtualMachine vm = new VirtualMachine(va);
				
				// save the reference to the VM and its type
				sortedVmTypes.put(vm, i);
				
				j++;
			}
		}
		
		// now we have to sort the mapping to guarantee a certain order
//		unsortedVmTypes.entrySet().stream()
//		    .sorted(Map.Entry.comparingByValue())
//		    .forEachOrdered(x -> sortedVmTypes.put(x.getKey(), x.getValue()));
	}
	
	/**
	 * Determines the resources for each VM of the IaaS and allocates them.
	 * 
	 * @throws Exception
	 */
	private void allocateVms() throws Exception {
		int i = 0;
		
		for(VirtualMachine vm : sortedVmTypes.keySet()) {
						
			double percentage;
			if(workload[i][currSample] == 1.00) {
				percentage = 1.00;
			}
			else {
				percentage = Double.parseDouble("0." + workload[i][currSample]);
			}
			// check whether the percentage is zero, then we take 0.1
			if(percentage < 0.01)
				percentage = 0.01;
			
			ResourceConstraints cons = createInitialRA(vm, percentage);
			
			// debug: log the constraints
			allocateLoads += "[" + cons.getRequiredCPUs() + "|" + cons.getRequiredProcessingPower() + "|" + cons.getRequiredMemory() 
				+ "] TPP: " + cons.getTotalProcessingPower() + ";";
			
			// allocate this VM to a PM via the scheduler
			i++;
			iaas.sched.scheduleVMrequest(new VirtualMachine[] {vm}, cons, iaas.repositories.get(0), null);
//			scheduleManually(vm, cons);
		}
		
		allocateLoads = allocateLoads.substring(0, allocateLoads.length() - 1);		
		
		Timed.simulateUntil(Timed.getFireCount()+1);
	}
	
	/**
	 * fill the mappings
	 */
	private void fillMapping() {
		for(VirtualMachine current: iaas.listVMs()) {
			if(current.getResourceAllocation() == null) {
				Logger.getGlobal().info("Host of VM " + current.hashCode() + " is null, but VM is not suspended.");
				vmToHostMapping.put(current, null);
			}
			else {
				vmToHostMapping.put(current, current.getResourceAllocation().getHost());
			}
			
		}
		for(VirtualMachine current: iaas.listSuspendedVMs().keySet()) {
			// if a VM is suspended, we put null as the host to synmbolize that
			vmToHostMapping.put(current, null);
		}
	}

	/**
	 * Adjusts the resource constraints of each VM according to a given period of time.
	 * 
	 * To prevent possible errors when adjusting the VM sizes we use a new approach - 
	 * 		- build a new model with all new sizes and do the load adjustments there.
	 * 		- then look which PMs are overloaded and have to migrate some VMs. Those VMs 
	 * 			are collected and migrated by using first fit. If at least one VM cannot 
	 * 			be hosted by any PM, an error is written (since this should never be the case).
	 * 
	 * After this we can do those changes on the real IaaS. 
	 */
	private void doVmSizing() {
		// debug string
		String loads = "";
		
		Logger.getGlobal().info("doVmSizing() called"); 
		// at first we update our vm -> pm mapping
		fillMapping();
		
		// if there are non running PMs that should host VMs, we use another class to queue host events
		// and to do them after the PM has started
		List<PMStartUpQueuer> queues = new ArrayList<>();
		
		// we get the percentages for each PM to be able to change their sizes
		Map<VirtualMachine, ResourceConstraints> vmToSizeMapping = getCurrentSizes();
		
		// do the sizing in the mapping and get the new situation, additionally switch back to normal VMs / PMs		
		Map<ModelVM, ModelPM> vmToNewHostMapping = createModelWithVmSizing(vmToSizeMapping);
		Map<VirtualMachine, PhysicalMachine> migrationPlan = new LinkedHashMap<VirtualMachine, PhysicalMachine>();
		for(ModelVM vm: vmToNewHostMapping.keySet()) {
			PhysicalMachine pm = vmToNewHostMapping.get(vm).getPM();
			
			// switch the machine on if it is not running
			if(!pm.isRunning()) {
//				Logger.getGlobal().info("PM " + pm.hashCode() + " is not running but target of migrations, PMStartUpQueuer created.");
				// FIXME PM is instantly started, we do not need the queuer
				((ConsolidationFriendlyPmScheduler)pmScheduler).switchOn(pm);
//				queues.add(new PMStartUpQueuer(pm));
			}			
			
			migrationPlan.put(vm.getVM(), pm);
		}
		
		if(migrationPlan.size() != sortedVmTypes.size()) {
			Logger.getGlobal().info("migrationPlan has size " + migrationPlan.size() + ", should be " + sortedVmTypes.size());
		}		
		
		// cancel all allocations
		for(VirtualMachine vm: migrationPlan.keySet()) {
			if(vm.getResourceAllocation() != null) {
				vm.setPreviousHostPM(vm.getResourceAllocation().getHost());
				vm.getResourceAllocation().release();				
			}
			else {
				PhysicalMachine pm = vm.getPreviousHostPM();
				if(pm != null) {
					pm.removeSus(vm);
				}
				
				Logger.getGlobal().info("Allocation of VM " + vm.hashCode() + " has already been cancelled before doVmSizing()");
				
			}
		}
		
		// debug: test whether all PMs are completely empty
		List<PhysicalMachine> notEmptyPMs = new ArrayList<>();
		for(PhysicalMachine pm: iaas.machines) {
			// try to remove the errors
			pm.rearrangeResources();
			
			if(pm.freeCapacities.getTotalProcessingPower() != pm.getCapacities().getTotalProcessingPower()) {
//				Logger.getGlobal().info("After releasing all allocations, PM " + pm.hashCode() + " is not empty, "
//						+ "currently free: " + pm.freeCapacities);
				notEmptyPMs.add(pm);
			}
		}
		
		// print the problem into the log
		if(notEmptyPMs.size() > 0) {
			Logger.getGlobal().info("After releasing all allocations, " + notEmptyPMs.size() + " of " + 
					iaas.machines.size() + " have not the correct free resources.");
		}
		
		int sizingCounter = 0;
		int startUpCounter = 0;
		
		/** 
		 * Now we do the collected migrations inside the real model and do the VM sizing afterwards.
		 * There must not be any exception at this point.
		 */
		// contains VM that should in theory be hostable but in reality they are not on the determined PM
		List<VirtualMachine> failedVMs = new ArrayList<>();
		for(VirtualMachine vm: migrationPlan.keySet()) {
			PhysicalMachine futureHost = migrationPlan.get(vm);
			if(futureHost == null) {
				Logger.getGlobal().info("Something went wrong while creating the migration plan.");
				continue;
			}
			
			// the adjusted size
			ResourceConstraints size = null;

			// the percentage of usage of cpu for this VM
			if(!vmToSizeMapping.containsKey(vm)) {
				Logger.getGlobal().info("VM " + vm.hashCode() + " is not inside the sizes mapping.");
				size = new AlterableResourceConstraints(vmCores[sortedVmTypes.get(vm)] * 0.01,
						vmMips[sortedVmTypes.get(vm)], vmRamMb[sortedVmTypes.get(vm)]);
			} 
			else {
				size = vmToSizeMapping.get(vm);
			}
			
			// debug: log the constraints
			loads += "[" + size.getRequiredCPUs() + "|" + size.getRequiredProcessingPower() + "|" + size.getRequiredMemory() 
					+ "] TPP: " + size.getTotalProcessingPower() + ";";			
			
			// VM is suspended: we try to resume it on the newly determined PM
//			if(vm.getState().equals(VirtualMachine.State.SUSPENDED)) {
//				Logger.getGlobal().info("VmSizing for a suspended VM, try to resume with new size for VM " + vm.hashCode());
//				vm.setPrevRes(size);
//				try {
//					vm.resume(futureHost);
//				} catch (VMManagementException e) {
//					Logger.getGlobal().info("VMManagementException: Resuming of VM " + vm.hashCode() + " was unsuccessful on PM " 
//							+ futureHost.hashCode());
//					e.printStackTrace();
//				} catch (NetworkException e) {
//					Logger.getGlobal().info("NetworkException: Resuming of VM " + vm.hashCode() + " was unsuccessful on PM " 
//							+ futureHost.hashCode());
//					e.printStackTrace();
//				}
//			}
//			// VM is not suspended: we just try to allocate the resource on the future PM
//			else  {
				// FIXME because of an error regarding the free resources of a PM to be != the total capacity after
				// releasing all allocations, there is the possibility for a VM to not fit on a PM even if we have
				// calculated inside the previous functions that it should fit (because of the differences previously 
				// mentioned). To ensure an error free run, we test whether the VM fits here again and if not we
				// get another PM with sufficient resources and host it there
				
				if(futureHost.freeCapacities.getTotalProcessingPower() < size.getTotalProcessingPower()) {
					// we need another host, find another host after the other VMs are finished
					failedVMs.add(vm);
					continue;
				}
				
				// FIXME this is strangely never the case even when PMQueuer are started
				if(!futureHost.isRunning()) {					
					
					Logger.getGlobal().info("PM "+ futureHost.hashCode() + " is not running, but should be hosting VM " + 
							vm.hashCode() + ", adding to the list");
					
					PMStartUpQueuer queuer = null;
					for(PMStartUpQueuer queue: queues) {
						if(queue.getPM().equals(futureHost)) {
							queuer = queue;
							break;
						}
					}
					
					if(queuer == null) {
						Logger.getGlobal().info("PMStartUpQueuer not found for PM " + futureHost.hashCode());
					}
					else {
						queuer.addVM(vm, size);
						startUpCounter++;
					}
//					Logger.getGlobal().info("debug: vm state next line \n" + vm.toString());
//					unplacedVMs.put(vm, size);
					
				}
				else {
					ResourceAllocation newResAlloc;
					try {						
						newResAlloc = futureHost.allocateResources(size, true, PhysicalMachine.defaultAllocLen);
						if(newResAlloc != null) {
							
							hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine.State state = vm.getState();
							
							// in case of a suspended VM we have to set the state to running before
							if(state.equals(hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine.State.SUSPENDED)) {
								vm.reallocSetState();
							}
							
							vm.setResourceAllocation(newResAlloc);
							
							// if there was a problem with the resuming, adjust the counter now after successful starting
							if(state.equals(hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine.State.SUSPENDED)) {
								AACHelper.resumeCounter++;
							}
							
							// increase the counter
							sizingCounter++;
						}
						else {
							// if this happens for a VM, it is thrown out of the iaas.listVMs()							
							Logger.getGlobal().info("newResAlloc is null for VM " + vm.hashCode() + " with state " + 
							vm.getState() + ", host should have been PM " + futureHost.hashCode() + ", free caps: " + futureHost.freeCapacities);
							
							// suspend it with the new resources
							vm.setPreviousHostPM(futureHost);
							try {
								vm.suspendWithNewRCs(size, futureHost);
							} catch (NetworkException e) {
								e.printStackTrace();
							}
							Logger.getGlobal().info("suspended VM instead of adding, VM " + vm.hashCode());
							
						}
					} catch (StateChangeException e) {
						e.printStackTrace();
					} catch (VMManagementException e) {
						e.printStackTrace();
					}
					
				}					
			
//			}			
			
		}
		
		/** now try to host the remaining VMs */
		for(VirtualMachine vm: failedVMs) {
			Logger.getGlobal().info("There are " + failedVMs.size() + " VMs that failed, try to find new hosts.");
			// the adjusted size
			ResourceConstraints size = null;

			// the percentage of usage of cpu for this VM
			if(!vmToSizeMapping.containsKey(vm)) {
				Logger.getGlobal().info("VM " + vm.hashCode() + " is not inside the sizes mapping.");
				size = new AlterableResourceConstraints(vmCores[sortedVmTypes.get(vm)] * 0.01,
						vmMips[sortedVmTypes.get(vm)], vmRamMb[sortedVmTypes.get(vm)]);
			} 
			else {
				size = vmToSizeMapping.get(vm);
			}
			PhysicalMachine futureHost;
			futureHost = findFirstFitHost(iaas.runningMachines, null, vm, size);
			
			if(futureHost == null) {
				// now try all PMs
				futureHost = findFirstFitHost(iaas.machines, null, vm, size);
			}
			
			if(!futureHost.isRunning()) {
				Logger.getGlobal().info("PM "+ futureHost.hashCode() + " is not running, but should be hosting VM " + 
						vm.hashCode() + ", adding to the list");
				
				PMStartUpQueuer queuer = null;
				for(PMStartUpQueuer queue: queues) {
					if(queue.getPM().equals(futureHost)) {
						queuer = queue;
						break;
					}
				}
				
				if(queuer == null) {
					Logger.getGlobal().info("PMStartUpQueuer not found for PM " + futureHost.hashCode());
				}
				else {
					queuer.addVM(vm, size);
					startUpCounter++;
				}
				
			}
			else {
				ResourceAllocation newResAlloc;
				try {					
					newResAlloc = futureHost.allocateResources(size, true, PhysicalMachine.defaultAllocLen);
					if(newResAlloc != null) {
						vm.setResourceAllocation(newResAlloc);
						
						// increase the counter
						sizingCounter++;
					}
					else {
						// if this happens for a VM, it is thrown out of the iaas.listVMs()
						
						Logger.getGlobal().info("newResAlloc is null for VM " + vm.hashCode() + " with state " + 
						vm.getState() + ", host should have been PM " + futureHost.hashCode() + ", free caps: " + futureHost.freeCapacities);
						
						// suspend it with the new resources
						vm.setPreviousHostPM(futureHost);
						try {
							vm.suspendWithNewRCs(size, futureHost);
						} catch (NetworkException e) {
							e.printStackTrace();
						}
						Logger.getGlobal().info("suspended VM instead of adding, VM " + vm.hashCode());
						
					}
				} catch (StateChangeException e) {
					e.printStackTrace();
				} catch (VMManagementException e) {
					e.printStackTrace();
				}
				
			}
			
		}
		
		// check whether all VMs run properly again
		if(sizingCounter != sortedVmTypes.size()) {
			int diff = (sortedVmTypes.size() - sizingCounter);			
			
			if(startUpCounter == diff) {
				Logger.getGlobal().info("There are " + diff + " VMs not running properly after doVmSizing(), but are queued "
						+ "and will be running in the near future.");
			}
			else {
				Logger.getGlobal().info("There are " + diff + " VMs not running properly after doVmSizing(), there are " + 
						(diff - startUpCounter) + " not queued VMs");
			}
			
		}
		
		Logger.getGlobal().info("adjusted the load with doVmSizing() at " + Timed.getFireCount());		
		loads = loads.substring(0, loads.length() - 1);
		try {
			writeLogfile(false, loads);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// check the states and the hosts
		if(!areVMsStartedAndHosted()) {
			Logger.getGlobal().info("There are VMs that are not in RUNNING-state and/or hosted.");
		}
	}
	
	/**
	 * Determines the ResourceConstraints for all VMs that have to be used for the next call to the
	 * consolidator.
	 * 
	 * @return A LinkedHashMap, mapping ResourceConstraints to VMs
	 */
	private Map<VirtualMachine, ResourceConstraints> getCurrentSizes() {
		Map<VirtualMachine, ResourceConstraints> result = new TreeMap<>(new VmIdComparator());
		
		int i = 0;
		for(VirtualMachine vm : sortedVmTypes.keySet()) {
			
			if(vm == null) {
				Logger.getGlobal().info("Skipped a VM that is null at this point.");
				continue;
			}
			// determine the new size to allocate
			double percentage;
			if(workload[i][currSample] == 1.00) {
				percentage = 1.00;
			}
			else {
				percentage = Double.parseDouble("0." + workload[i][currSample]);
			}
			// check whether the percentage is zero, then we take 0.1
			if(percentage < 0.01)
				percentage = 0.01;
			
			ResourceConstraints size = createRAforRunningVM(vm, percentage);
			
			// put the result into the map
			result.put(vm, size);
			
			// increase the counter
			i++;
		}
		
		return result;
		
	}
	
	
	
	// used to do VmSizing() with the model and return the changes made
	/**
	 * Adjusts the load of the model of the current situation in the IaaS. Note that suspended VMs
	 * are not considered here, since they are not hosted on any PM. They are considered when the
	 * changes are made on the real situation of the IaaS.
	 * 
	 * TODO debug: Suspended VMs are considered.
	 * 
	 * @param mapping
	 * @param percentages
	 * 
	 * @return A mapping of VMs to PMs which symbolizes the migrations of each VM to a fitting PM. 
	 */
	private Map<ModelVM, ModelPM> createModelWithVmSizing(Map<VirtualMachine, ResourceConstraints> sizes) {
		// counter to create the modelPMs
		int counter = 0;
		
		// counter to measure how much VMs are migrated here
		int migCounter = 0;
		
		// create the model with the sizes
//		Map<ModelPM, List<ModelVM>> result = new LinkedHashMap<ModelPM, List<ModelVM>>();
		List<ModelPM> pms = new ArrayList<>();
		
		// collect additionally suspended VMs
		List<ModelVM> suspended = new ArrayList<>();
		
		// initialize the mapping
		for(PhysicalMachine pm: iaas.machines) {
			// threshold does not matter here
			ModelPM bin = new ModelPM(pm, 0, 1, counter);
			counter++;
			pms.add(bin);
//			result.put(bin, new ArrayList<ModelVM>());
		}
		
		// add the VMs
		int i = 0;
		for(VirtualMachine vm : sortedVmTypes.keySet()) {
			ResourceConstraints adjusted = sizes.get(vm);
			
			// error case: if the VM is suspended and thus does not consume resources, 
			// it has to be directly added to the list
			if(vm.getResourceAllocation() == null) {
				suspended.add(new ModelVM(vm, adjusted, null, i, true));
				i++;
				continue;
			}
			
			PhysicalMachine host = vm.getResourceAllocation().getHost();
			ModelPM mpmHost = null;
			// find the hostPM
			for(ModelPM mpm: pms) {
				if(mpm.getPM().hashCode() == host.hashCode()) {
					mpmHost = mpm;
					break;
				}
			}
			
			// if we cannot find a host, the VM is suspended and added as suspended
			if(mpmHost == null) {
				Logger.getGlobal().info("Host of VM " + vm.hashCode() + " not found, added as suspended.");
				if(iaas.listSuspendedVMs().keySet().contains(vm)) {
					suspended.add(new ModelVM(vm, adjusted, null, i, true));
				}
				else {
					Logger.getGlobal().info("Host of VM " + vm.hashCode() + " not found and VM is not suspended, ERROR");
				}
				
				
			}
			// host is found, added the VM to the host
			else {
				ModelVM item = new ModelVM(vm, adjusted, mpmHost, i, false);
				mpmHost.addVM(item);
				
//				List<ModelVM> added = result.get(mpmHost);
//				added.add(item);
//				result.put(mpmHost, added);
			}
			i++;
		}
		
		// now do the migrations and collect changes
//		Map<ModelVM, ModelPM> adjustedMapping = new LinkedHashMap<ModelVM, ModelPM>();
		Map<ModelVM, ModelPM> adjustedMapping = new TreeMap<>(new ModelVmIdComparator());
		
		// try to place suspended VMs first
		for(ModelVM sus: suspended) {
			ModelPM futureHost = findFirstFitModelHost(new ArrayList<ModelPM>(pms), null, sus, 
					sus.getResources());
			if(futureHost == null) {
				Logger.getGlobal().info("No host found for suspended VM " + sus.hashCode() + " using first fit, ERROR");
			}
			else {
				futureHost.addVM(sus);
				Logger.getGlobal().info("Placed VM " + sus + " in doVMSizing().");
				migCounter++;
//				adjustedMapping.put(sus, futureHost);	// TODO addition
			}
			
		}
		
		for(ModelPM toCheck: pms) {
			// control the current load: if it is not over the max resources, continue.
			// Otherwise migrate VMs till we are under the maximum load according to first fit.
			if(toCheck.getConsumedResources().getTotalProcessingPower() < toCheck.getTotalResources().getTotalProcessingPower()) {
				continue;	// load is okay
			}
			
			// at this point we need to migrate VMs to get the load back to normal
			while(toCheck.getConsumedResources().getTotalProcessingPower() > 
				toCheck.getTotalResources().getTotalProcessingPower()) {
				
				// get the VM
				ModelVM toMigrate = toCheck.getVM(0);
				
				ModelPM futureHost = findFirstFitModelHost(pms, toCheck, toMigrate, 
						toMigrate.getResources());
				if(futureHost == null) {
					Logger.getGlobal().info("No host found for VM " + toMigrate.hashCode() + " using first fit.");
				}
				else {
					// add the PM and the new host
					toCheck.migrateVM(toMigrate, futureHost);
					migCounter++;
					//adjustedMapping.put(toMigrate, futureHost);
				}				
				
				
			}
			
		}
		
		// collect the current situation and return it
		for(ModelPM pm: pms) {
			for(ModelVM vm: pm.getVMs()) {
				adjustedMapping.put(vm, pm);
			}
		}
		
		Logger.getGlobal().info("The placement of " + migCounter + " VMs has been changed due to size adjustments.");
		
		return adjustedMapping;
	}
	
	/**
	 * Helper method for the abstract PMs and VMs. Finds the first PM inside the given list
	 * that is capable of hosting a VM.
	 * 
	 * @param all
	 * @param pm
	 * @param vm
	 * @param toTake
	 * @return
	 */
	private ModelPM findFirstFitModelHost(List<ModelPM> all, ModelPM pm, ModelVM vm, ResourceConstraints toTake) {	
		
		// check if the VM is running
		if(toTake == null) {
			Logger.getGlobal().info("Ressources are null for VM " + vm.hashCode());
		}
		else {
			for(ModelPM bin: all) {
				// check whether this is the same ModelPM
				if(pm != null) {
					if(bin == pm) {
						// there is no need to test this one
						continue;
					}
				}
				
				if(bin.isMigrationPossible(toTake)) {
					return bin;
				}
			}
		}		
		
		return null;
	}
	
	/**
	 * Helper method for the PMs and VMs. Finds the first PM inside the given list
	 * that is capable of hosting a VM.
	 * 
	 * @param all
	 * @param pm
	 * @param vm
	 * @param toTake
	 * @return
	 */
	private PhysicalMachine findFirstFitHost(List<PhysicalMachine> all, PhysicalMachine pm, VirtualMachine vm, ResourceConstraints toTake) {	
		
		// check if the VM is running
		if(toTake == null) {
			Logger.getGlobal().info("Ressources are null for VM " + vm.hashCode());
		}
		else {
			for(PhysicalMachine bin: all) {
				// check whether this is the same ModelPM
				if(pm != null) {
					if(bin == pm) {
						// there is no need to test this one
						continue;
					}
				}
				
				if(bin.freeCapacities.getTotalProcessingPower() >= toTake.getTotalProcessingPower()) {
					return bin;
				}
			}
		}		
		
		return null;
	}
	
	/**
	 * Sets the new compute tasks for all VMs.
	 * 
	 * @return The amount of compute tasks that could not be set.
	 */
	private int setComputeTasks() {
		int errors = 0;
		
		for(VirtualMachine vm : sortedVmTypes.keySet()) {
			
			// cancel all tasks, then set a new one with the currently used resources
			for(int i = 0; i < vm.underProcessing.size(); i++) {
				vm.underProcessing.get(i).cancel();
			}
			try {				
				ResourceConsumption cons = vm.newComputeTask(vm.getResourceAllocation().allocated.getRequiredCPUs() * 
						MyTime.sec2tick(timeIntervalSec), vm.getResourceAllocation().allocated.getRequiredCPUs(), 
						new ConsumptionEventAdapter());
				
				// task could not be set
				if(cons == null) {
					Logger.getGlobal().info("New ComputeTask for VM " + vm.hashCode() + " could not be set.");
					errors+= 1;
				}
			} catch (Exception e) {
				// since no exception is thrown, this is never called. vm.newComputeTask(...) just returns null.
				e.printStackTrace();
			}		
			
		}
		
		return errors;
	}
	
	/**
	 * Does the energy reading and updates the old values.
	 */
	private void energyReading() {		
		double newEnergy = energyMeter.getTotalConsumption() / (MyTime.sec2tick(3600) * 1000);
		Logger.getGlobal().info("Energy in this period [kWh]: " + (newEnergy - oldEnergy));
		Logger.getGlobal().info("Total energy [kWh]: " + newEnergy);
		oldEnergy = newEnergy;
	}
	
	/**
	 * Helper method to allow the ExperimentController to collect the wanted results.
	 * 
	 * total energy consumption in kWh;average amount of active PMs;Total amount of migrations;
	 * total amount of VM suspensions;total amount of VM resumes;total amount of PM starts;
	 * total amount of PM stops;total cpu overload;total duration in ms;nrIterations;
	 * total time of suspended VMs in ms
	 * 
	 * @return A new instance of an ExperimentResults. The constraints for the simulation
	 * need to be set, too, at this point.
	 */
	public ExperimentResults getResults() {
		results = new ExperimentResults(
				AACHelper.roundDouble(oldEnergy, 3), 
				AACHelper.getAverageActivePMs(), 
				AACHelper.migrationCounter, 
				AACHelper.suspensionCounter, 
				AACHelper.resumeCounter, 
				AACHelper.pmStartCounter, 
				AACHelper.pmStopCounter, 
				AACHelper.roundDouble( (double)(AACHelper.totalOverload.getTotalProcessingPower() / 
						AACHelper.totalLoad.getTotalProcessingPower()), 6), 
				AACHelper.roundDouble( (double)(AACHelper.totalOverload.getTotalProcessingPower()), 3), 
				AACHelper.roundDouble( (double)(AACHelper.totalLoad.getTotalProcessingPower()), 3), 
				duration, 
				AACHelper.iterationCount, 
				AACHelper.totalTimeSuspended);
		
		return results;
	}
	
	/**
	 * Used for the toString()-method, provides an organized pmList starting with the lowest id.
	 * 
	 * @param toSort The pmList to sort.
	 */
	private void sortPMList(List<PhysicalMachine> toSort) {
		toSort.sort(PMComparators.lowestToHighestHash);
	}
	
	/**
	 * Writes the current state of the simulation and prints it into the log.
	 */
	public String toString() {	
		String result = "";
		
		List<PhysicalMachine> PMs = new ArrayList<>(iaas.runningMachines);
		sortPMList(PMs);
		int currentlyHostedVMs = 0;
		
		for(PhysicalMachine pm : PMs) {
			result += pm.toString() + "\n";
			Set<VirtualMachine> vmsOfPm = pm.publicVms;
			double load = 0;
			double demand = 0;
			int hosted = 0;
			for(VirtualMachine vm : vmsOfPm) {
				hosted++;
				result += vm.toString() + "\n";
				load += vm.getPerTickProcessingPower();
				demand += vm.getResourceAllocation().allocated.getRequiredProcessingPower();
			}
			result += "amount of hosted VMs: " + hosted + "\n";
			result += "PM's total CPU capacity: " + pm.getCapacities().getTotalProcessingPower() + "\n";
			result += "PM's total CPU load: " + load + "\n";
			result += "PM's total CPU demand: " + demand + "\n";
			
			currentlyHostedVMs += hosted;
		}
		result += "Number of active PMs: " + PMs.size() + "\n";
		result += "Total number of currently hosted VMs: " + currentlyHostedVMs + "\n";
		
		return result;
	}
	
	/**
	 * Return an instance of AlterableResourceConstraints with the purpose to create an initial
	 * ResourceAllocation.
	 * 
	 * @param vm The VM the creation is for.
	 * @param percentage The percentage of the CPU usage.
	 * 
	 * @return An instance of AlterableResourceConstraints with the appropriate values for the VM.
	 */
	private AlterableResourceConstraints createInitialRA(VirtualMachine vm, double percentage) {
		AlterableResourceConstraints init = new AlterableResourceConstraints(vmCores[sortedVmTypes.get(vm)] * percentage, 
				vmMips[sortedVmTypes.get(vm)], false, vmRamMb[sortedVmTypes.get(vm)]);
		return init;
	}
	
	/**
	 * Return an instance of AlterableResourceConstraints with the purpose to create a new
	 * ResourceAllocation for a running VM.
	 * 
	 * @param vm The VM the creation is for.
	 * @param percentage The percentage of the CPU usage.
	 * 
	 * @return An instance of AlterableResourceConstraints with the appropriate values for the VM.
	 */
	private AlterableResourceConstraints createRAforRunningVM(VirtualMachine vm, double percentage) {
		AlterableResourceConstraints running = new AlterableResourceConstraints(vmCores[sortedVmTypes.get(vm)] * percentage, 
				vmMips[sortedVmTypes.get(vm)], false, vmRamMb[sortedVmTypes.get(vm)]);
		return running;
	}
	
	/**
	 * 
	 * @return The used consolidator.
	 */
	public Consolidator getConsolidator() {
		return cons;
	}
	
	/**
	 * A manual first fit scheduling algorithm.
	 * 
	 * @param toPlace
	 * @param load
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	private void scheduleManually(VirtualMachine toPlace, ResourceConstraints load, 
			List<PhysicalMachine> completePMList) throws Exception {
		
		// place the VM according to first fit
		for(PhysicalMachine current: completePMList) {
			// -1 : o1 < o2 | 0 : o1 == o2 | +1 : o1 > o2
			if(current.freeCapacities.compareTo(load) >= 0) {
				ResourceAllocation ra = null;
				try {				
					ra = current.allocateResources(load, false, PhysicalMachine.defaultAllocLen);
					if(ra == null)
						continue;
					current.deployVM(toPlace, ra, centralRepo);
					
					Logger.getGlobal().info("VM " + toPlace.hashCode() + " is now hosted by PM " + 
						current.hashCode() + ", state: " + toPlace.getState());
					
					break;
				} catch (NetworkException e) {
					Logger.getGlobal().info("NetworkException occured in prepartion of VM " + toPlace.hashCode());
					e.printStackTrace();
					break;
				} catch (VMManagementException e) {
					Logger.getGlobal().info("VM " + toPlace.hashCode() + ", VMManagementException");
					e.printStackTrace();
					break;
				}
				
			}
		}
		// error case
		if(toPlace.getResourceAllocation() == null || toPlace.getState().equals(VirtualMachine.State.DESTROYED)) {
			Logger.getGlobal().info("Allocation of VM " + toPlace.hashCode() + " failed.");
		}
			
	}
	
	/**
	 * Calculates the current load of all running VMs.
	 * 
	 * @return
	 */
	@SuppressWarnings("unused")
	private AlterableResourceConstraints calculateLoad() {
		AlterableResourceConstraints current = new AlterableResourceConstraints(0.0, highestMips, 0);
		
		for(VirtualMachine vm: vmToHostMapping.keySet()) {
			if(vm.getResourceAllocation() == null) {
				current.add(vm.getPrevRes());
			}
			else {
				current.add(vm.getResourceAllocation().allocated);
			}
			
		}
		
		return current;
	}
	
	@SuppressWarnings("unused")
	private void printVMs() {
		Logger.getGlobal().info("--------------------------------------");
		for(VirtualMachine toPrint: sortedVmTypes.keySet()) {
			Logger.getGlobal().info(toPrint.toString() + ", tpp: " + toPrint.getResourceAllocation().
					allocated.getTotalProcessingPower());
		}
		Logger.getGlobal().info("--------------------------------------");		
	}
	
	/**
	 * Helper method to determine the highest processingPower. Used for
	 * the totalLoad-object.
	 * 
	 * @return The highest processing power of all PMs.
	 */
	private double determineHighestMips() {
		double res = 0.0;
		
		// the vm mips
		for(int current: vmMips) {
			if(res < current) {
				res = current;
			}
		}
		
		// the pm mips
		for(int current: pmMips) {
			if(res < current) {
				res = current;
			}
		}
		
		return res;
	}
	
	/**
	 * Writes the decisions and loads into a .csv-file after each iteration.
	 * 
	 * @throws IOException 
	 */
	private void writeLogfile(boolean first, String currentLine) throws IOException {
		
		File output;
		if(!useDefaults) {
			output = new File("autoAdjust" + loadOutputFile);	
		}
		else {
			output = new File(cons.getAnalyzeParameter() + "" + cons.getPlanningParameter() + 
					"" + cons.getFrequencyParameter() + loadOutputFile);	
		}		
		
		BufferedWriter writer; 
		if(first) {
			writer = new BufferedWriter(new FileWriter(output));
			
			// create the first line out of the VMs
			String line = "";
			for(VirtualMachine vm: sortedVmTypes.keySet()) {
				line += "VM " + vm.hashCode() + ";";
			}
			line = line.substring(0, line.length() - 1);
			
			// write the first line
			writer.write(line);
			writer.newLine();
		}
		else
			writer = new BufferedWriter(new FileWriter(output, true));
		
		writer.write(currentLine);		
		writer.newLine();		
		writer.close();
	}

}

/**
 * TODO has not been tested since there were no suspended VMs at all
 * 
 * Simple helper class to allow the user to inform a specific PM after its start up
 * about hosting VMs, that are previously determined.
 */
class PMStartUpQueuer implements PhysicalMachine.StateChangeListener {
	
	private Map<VirtualMachine, ResourceConstraints> toHost = new HashMap<>();
	private PhysicalMachine eventFor = null;
	
	public PMStartUpQueuer(PhysicalMachine pm) {
		eventFor = pm;
	}
	
	public void addVM(VirtualMachine vm, ResourceConstraints cons) {
		toHost.put(vm, cons);
	}
	
	public PhysicalMachine getPM() {
		return eventFor;
	}
	
	private void hostVMs() {
		for(VirtualMachine vm: toHost.keySet()) {
			ResourceAllocation newResAlloc;
			try {
				newResAlloc = eventFor.allocateResources(toHost.get(vm), true, PhysicalMachine.defaultAllocLen);
				if(newResAlloc != null) {
					vm.setResourceAllocation(newResAlloc);
					Logger.getGlobal().info("Delayed VM host successful of VM " + vm.hashCode());
				}
				else {
					// if this happens for a VM, it is thrown out of the iaas.listVMs()
					Logger.getGlobal().info("Delayed VM host not successful of VM \" + vm.hashCode()");
					
					// suspend it with the new resources
					vm.setPreviousHostPM(eventFor);
					try {
						vm.suspendWithNewRCs(toHost.get(vm),eventFor);
					} catch (NetworkException e) {
						e.printStackTrace();
					}
					Logger.getGlobal().info("suspended VM instead of adding, VM " + vm.hashCode());
					
//					unplacedVMs.put(vm, size);
				}
			} catch (StateChangeException e) {
				e.printStackTrace();
			} catch (VMManagementException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void stateChanged(PhysicalMachine pm, State oldState, State newState) {
		if(newState.equals(PhysicalMachine.State.RUNNING)){
			pm.unsubscribeStateChangeEvents(this);			
			
			// print something to show the new state
			Logger.getGlobal().info("PM " + pm.hashCode() + " started by the PMQueuer.");
			hostVMs();
			Logger.getGlobal().info("PM " + pm.hashCode() + " started by the PMQueuer, finished adding VMs.");
		}
		
	}
	
}
