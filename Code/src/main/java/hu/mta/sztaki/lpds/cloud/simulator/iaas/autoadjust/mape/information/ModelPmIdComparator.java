package hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information;

import java.util.Comparator;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.ModelPM;


public class ModelPmIdComparator implements Comparator<ModelPM> {

	public ModelPmIdComparator() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int compare(ModelPM o1, ModelPM o2) {
		if(o1.hashCode() < o2.hashCode()) {
			return -1;
		}
		else if(o1.hashCode() == o2.hashCode()) {
			return 0;
		}
		else {
			return 1;
		}
		
	}

}
