package at.ac.uibk.dps.cloud.simulator.test.autoadjust;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.junit.Before;
import org.junit.Test;

import at.ac.uibk.dps.cloud.simulator.test.IaaSRelatedFoundation;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine.State;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AutoAdjustingConsolidator;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.AACHelper;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.AnalyzerOutput;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.MonitorInformation;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.autoadjust.mape.information.Plan;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VMManager.VMManagementException;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.AlterableResourceConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ConstantConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.pmscheduling.ConsolidationFriendlyPmScheduler;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.vmscheduling.FirstFitScheduler;
import hu.mta.sztaki.lpds.cloud.simulator.io.Repository;
import hu.mta.sztaki.lpds.cloud.simulator.io.VirtualAppliance;
import hu.mta.sztaki.lpds.cloud.simulator.io.NetworkNode.NetworkException;

public class AutoAdjustSimpleTest extends IaaSRelatedFoundation {
	
	/**
	 * @author Rene Ponto
	 * 
	 * Testcases:
	 * 
	 * 1.: Check whether the simulation runs without getting interrupted.
	 * 
	 * 2.: Check the MAPE-loop related parts with defaults.
	 * 2.1.: Check the monitoring (correct functionality and output).
	 * 2.2.: Check the analysis (correct functionality and output, handling of previous outputs).
	 * 2.3.: Check the planning (correct functionality and output, usage of modelbased approach).
	 * 2.4.: Check the execution (correct functionality and output).
	 * 
	 * 3.: Check the auto-adjust-part.
	 * 3.1.: Static check with defaults.
	 * 3.2.: Dynamic check without defaults.
	 *
	 */

	// Creation of all necessary objects and variables
	
	private Properties props;

	private IaaSService toConsolidate;
	private long consFreq = 300000;
	private PhysicalMachine testPM1;
	private PhysicalMachine testPM2;
	private PhysicalMachine testPM3;
	private PhysicalMachine testPM4;

	private VirtualMachine VM1;
	private VirtualMachine VM2;
	private VirtualMachine VM3;
	private VirtualMachine VM4;
	private VirtualMachine VM5;
	private VirtualMachine VM6;
//	private VirtualMachine VM7;
//	private VirtualMachine VM8;

	private VirtualAppliance VA1;	
	private VirtualAppliance VA2;
	private VirtualAppliance VA3;	
	private VirtualAppliance VA4;
	private VirtualAppliance VA5;
	private VirtualAppliance VA6;
	private VirtualAppliance VA7;
	private VirtualAppliance VA8;

	HashMap<String, Integer> latmap = new HashMap<String, Integer>();	
	Repository centralRepo;

	final static int reqcores = 8, reqProcessing = 1, reqmem = 16,
			reqond = 2 * (int) aSecond, reqoffd = (int) aSecond, reqDisk=100000;

	// determines usable resources for VMs

	final ResourceConstraints smallConstraints = new ConstantConstraints(1, 1, 2);	
	final ResourceConstraints mediumConstraints = new ConstantConstraints(3, 1, 6);	
	final ResourceConstraints bigConstraints = new ConstantConstraints(5, 1, 8);

	/**
	 * Creates a PM with the parametrized values.
	 * 
	 * @param id The id of the newly created PM.
	 * @param cores The cores of the PM.
	 * @param perCoreProcessing The perCoreProcessingPower of the PM.
	 * @param ramMb The memory of the PM.
	 * @param diskMb The MBs for the disk to be used to host the PM.
	 * @return
	 */
	private PhysicalMachine createPm(String id, double cores, double perCoreProcessing, int ramMb, int diskMb) {
		long bandwidth=1000000;
		Repository disk = new Repository(diskMb*1024*1024, id, bandwidth, bandwidth, bandwidth, latmap, defaultStorageTransitions, defaultNetworkTransitions);
		PhysicalMachine pm = new PhysicalMachine(cores, perCoreProcessing, ramMb*1024*1024, disk, reqond, reqoffd, defaultHostTransitions);
		latmap.put(id,1);
		return pm;
	}

	/**
	 * Deploys one VM to a given PM.
	 * 
	 * @param vm The VM to be deployed.
	 * @param cons The ResourceConstraints to be used by the VM.
	 * @param pm The PM to host the VM.
	 * @param simulate Determines whether to simulate until the last event afterwards.
	 * @throws VMManagementException
	 * @throws NetworkException
	 */
	private void switchOnVM(VirtualMachine vm, ResourceConstraints cons, PhysicalMachine pm, boolean simulate) throws VMManagementException, NetworkException {
		vm.switchOn(pm.allocateResources(cons, true, PhysicalMachine.defaultAllocLen), centralRepo);
		if(simulate) {
			Timed.simulateUntilLastEvent();
		}
	}
	
	/**
	 * Loads the properties to use them.
	 * 
	 * @throws InvalidPropertiesFormatException
	 * @throws IOException
	 */
	private void loadProps() throws InvalidPropertiesFormatException, IOException {
		props = new Properties();
		File file = new File("autoAdjustDefaults.xml");
		FileInputStream fileInput = new FileInputStream(file);
		props.loadFromXML(fileInput);
		fileInput.close();
	}
	
	/**
	 * Saves the properties inside the specified file.
	 */
	private void saveProps() {
		try {
			FileOutputStream fileOutput = new FileOutputStream(
					new File("autoAdjustDefaults.xml"));
			props.storeToXML(fileOutput, null);
			fileOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Resets the defaults for using the AutoAdjustConsolidator.
	 * 
	 * @throws InvalidPropertiesFormatException
	 * @throws IOException
	 */
	private void resetDefaults() throws InvalidPropertiesFormatException, IOException {
		loadProps();
		props.setProperty("defaultA", "A1");
		props.setProperty("defaultP", "P1");
		props.setProperty("defaultFrequency", "F2");
		saveProps();
	}

	/**
	 * At first the PMs are created, after that the VMs are created and before each test
	 * each of them deployed to one PM. 
	 */	
	@Before
	public void testSim() throws Exception {
		Handler logFileHandler;
		try {
			logFileHandler = new FileHandler("log.txt");
			logFileHandler.setFormatter(new SimpleFormatter());
			Logger.getGlobal().addHandler(logFileHandler);
		} catch (Exception e) {
			System.out.println("Could not open log file for output"+e);
			System.exit(-1);
		}

		toConsolidate = new IaaSService(FirstFitScheduler.class, ConsolidationFriendlyPmScheduler.class);

		// create the central repository
		long bandwidth=1000000;
		centralRepo = new Repository(100000, "test1", bandwidth, bandwidth, bandwidth, latmap, defaultStorageTransitions, defaultNetworkTransitions);
		latmap.put("test1", 1);
		toConsolidate.registerRepository(centralRepo);

		// create all PMs
		testPM1 = createPm("pm1", reqcores, reqProcessing, reqmem, reqDisk);
		testPM2 = createPm("pm2", reqcores, reqProcessing, reqmem, reqDisk);
		testPM3 = createPm("pm3", reqcores, reqProcessing, reqmem, reqDisk);
		testPM4 = createPm("pm4", reqcores, reqProcessing, reqmem, reqDisk);
		
		// register all PMs inside the IaaSService
		toConsolidate.registerHost(testPM1);
		toConsolidate.registerHost(testPM2);
		toConsolidate.registerHost(testPM3);
		toConsolidate.registerHost(testPM4);

		// create eight VAs
		VA1 = new VirtualAppliance("VM 1", 1, 0, false, 1);
		VA2 = new VirtualAppliance("VM 2", 1, 0, false, 1);
		VA3 = new VirtualAppliance("VM 3", 1, 0, false, 1);
		VA4 = new VirtualAppliance("VM 4", 1, 0, false, 1);
		VA5 = new VirtualAppliance("VM 5", 1, 0, false, 1);
		VA6 = new VirtualAppliance("VM 6", 1, 0, false, 1);
		VA7 = new VirtualAppliance("VM 7", 1, 0, false, 1);
		VA8 = new VirtualAppliance("VM 8", 1, 0, false, 1);

		// save the VAs in the repository
		centralRepo.registerObject(VA1);
		centralRepo.registerObject(VA2);
		centralRepo.registerObject(VA3);
		centralRepo.registerObject(VA4);		
		centralRepo.registerObject(VA5);
		centralRepo.registerObject(VA6);
		centralRepo.registerObject(VA7);
		centralRepo.registerObject(VA8);	
		
		VM1 = new VirtualMachine(VA1);
		VM2 = new VirtualMachine(VA2);
		VM3 = new VirtualMachine(VA3);
		VM4 = new VirtualMachine(VA4);
		VM5 = new VirtualMachine(VA5);
		VM6 = new VirtualMachine(VA6);
//		VM7 = new VirtualMachine(VA7);
//		VM8 = new VirtualMachine(VA8);
	}
	
	// simple test run for null pointer etc.
	@Test
	public void simpleTestRun() throws VMManagementException, NetworkException, 
	InvalidPropertiesFormatException, IOException {

//		loadProps();
//		props.setProperty("defaultP", "P1");
//		props.setProperty("defaultA", "A1");
//		saveProps();
		
		testPM1.turnon();
		Timed.simulateUntilLastEvent();
		switchOnVM(VM1, smallConstraints, testPM1, false);
		Timed.simulateUntilLastEvent();
		
		new AutoAdjustingConsolidator(toConsolidate, consFreq, true, true, true);
		
		Timed.simulateUntil(Timed.getFireCount() + 400000);
		
		// reset the defaults					
		AACHelper.resetCounter();
		resetDefaults();
	}
	
	// test the monitoring
	@Test
	public void monitoringTestRun() throws VMManagementException, NetworkException, 
	InvalidPropertiesFormatException, IOException {	

//		loadProps();
//		props.setProperty("defaultP", "P1");
//		props.setProperty("defaultA", "A1");
//		saveProps();
		
		testPM1.turnon();
		testPM2.turnon();
		testPM3.turnon();
		testPM4.turnon();
		
		Timed.simulateUntilLastEvent();
		switchOnVM(VM1, mediumConstraints, testPM1, true);
		switchOnVM(VM2, mediumConstraints, testPM2, true);
		switchOnVM(VM3, smallConstraints, testPM3, true);
		switchOnVM(VM4, smallConstraints, testPM3, true);
		Timed.simulateUntilLastEvent();

		AutoAdjustingConsolidator consolidator = new AutoAdjustingConsolidator(toConsolidate, consFreq, true, true, true);
		
		Timed.simulateUntil(Timed.getFireCount() + 400000);			
			
		// assertions
		MonitorInformation result = consolidator.getMonitorInformation();
			
		assertEquals(4, result.getPMInformation().size());
		assertEquals(4, result.getVMInformation().size());
			
		assertEquals(mediumConstraints, result.getSpecificVMInformation(VM1).getLoad());
		assertEquals(mediumConstraints, result.getSpecificVMInformation(VM2).getLoad());
		assertEquals(smallConstraints, result.getSpecificVMInformation(VM3).getLoad());
		assertEquals(smallConstraints, result.getSpecificVMInformation(VM4).getLoad());
			
		assertEquals(testPM1, result.getSpecificVMInformation(VM1).getHost());
		assertEquals(testPM2, result.getSpecificVMInformation(VM2).getHost());
		assertEquals(testPM3, result.getSpecificVMInformation(VM3).getHost());
		assertEquals(testPM3, result.getSpecificVMInformation(VM4).getHost());
			
		assertTrue(result.getMapping().containsKey(testPM1));
		assertTrue(result.getMapping().containsKey(testPM2));
		assertTrue(result.getMapping().containsKey(testPM3));
		assertTrue(result.getMapping().containsKey(testPM4));
			
		// check the totalLoad
		// smallConstraints = new ConstantConstraints(1, 1, 2);	
		// mediumConstraints = new ConstantConstraints(3, 1, 6);	
		// bigConstraints = new ConstantConstraints(5, 1, 8);
			
		AlterableResourceConstraints toCompare = new AlterableResourceConstraints(smallConstraints);
		toCompare.add(smallConstraints);
		toCompare.add(mediumConstraints);
		toCompare.add(mediumConstraints);
			
		assertEquals(result.getTotalLoad().getTotalProcessingPower(), toCompare.getTotalProcessingPower(), 0);
		assertEquals(result.getTotalLoad().getRequiredMemory(), toCompare.getRequiredMemory());
				
		// reset the defaults					
		AACHelper.resetCounter();
		resetDefaults();	
	}
	
	// test the A1-algorithm of analyzing
	/**
	 * For this test we need to create at least two values for each pm and
	 * vm inside the MonitorInformation to be able to say wether a PM could
	 * be overloaded in the future.
	 */
	@Test
	public void analyzingTestRun1() throws VMManagementException, NetworkException, 
	InvalidPropertiesFormatException, IOException {

//		loadProps();
//		props.setProperty("defaultP", "P1");
//		props.setProperty("defaultA", "A1");
//		saveProps();
		
		// instantiate pms and vms
		testPM1.turnon();
		testPM2.turnon();
		testPM3.turnon();
		testPM4.turnon();
		
		Timed.simulateUntilLastEvent();
		switchOnVM(VM1, mediumConstraints, testPM1, true);
		switchOnVM(VM2, mediumConstraints, testPM2, true);
		switchOnVM(VM3, smallConstraints, testPM3, true);
		switchOnVM(VM4, smallConstraints, testPM3, true);
		switchOnVM(VM5, bigConstraints, testPM1, true);
		Timed.simulateUntilLastEvent();

		AutoAdjustingConsolidator consolidator = new AutoAdjustingConsolidator(toConsolidate, consFreq, true, true, true);

		Timed.simulateUntil(Timed.getFireCount() + 400000);
			
		switchOnVM(VM6, mediumConstraints, testPM3, false);
		Timed.simulateUntil(Timed.getFireCount() + 400000);
		
		// define tests
		// by adding one more VM the analysis should think PM3 will be overloaded in the future
			
		AnalyzerOutput analysis = consolidator.getAnalyzerOutput();
			
		assertFalse(analysis.getMapping().isEmpty());
		
		assertTrue(analysis.getOverloaded().isEmpty());
		assertTrue(!analysis.getPossibleOverloaded().isEmpty());
		assertTrue(analysis.getSignificantDecreasingPMs().isEmpty());
		assertTrue(analysis.getSignificantIncreasingPMs().isEmpty());
		assertTrue(analysis.getSuspendedVMs().isEmpty());		
		
		// reset the defaults					
		AACHelper.resetCounter();
		resetDefaults();
	}
	
	// test the A2-algorithm of analyzing
	/**
	 * 
	 */
	@Test
	public void analyzingTestRun2() throws VMManagementException, NetworkException,
	InvalidPropertiesFormatException, IOException {
		
		loadProps();
		props.setProperty("defaultP", "P2");
		props.setProperty("defaultA", "A2");
		saveProps();
		
		// instantiate pms and vms
		testPM1.turnon();
		testPM2.turnon();
		testPM3.turnon();
		testPM4.turnon();
				
		Timed.simulateUntilLastEvent();
		switchOnVM(VM1, mediumConstraints, testPM1, true);
		switchOnVM(VM2, mediumConstraints, testPM2, true);
		switchOnVM(VM3, smallConstraints, testPM3, true);
		switchOnVM(VM4, smallConstraints, testPM3, true);
		switchOnVM(VM5, mediumConstraints, testPM3, true);
		Timed.simulateUntilLastEvent();

		AutoAdjustingConsolidator consolidator = new AutoAdjustingConsolidator(toConsolidate, consFreq, true, true, true);		
		Timed.simulateUntil(Timed.getFireCount() + 400000);
		
		VM5.suspend();
		
		// at this point we should have one suspended VM inside the list
		
		assertEquals(1, toConsolidate.listSuspendedVMs().size());
		Timed.simulateUntil(Timed.getFireCount() + 400000);
			
		// define tests
		// now one suspended VM should be shown
			
		AnalyzerOutput analysis = consolidator.getAnalyzerOutput();
			
		assertFalse(analysis.getMapping().isEmpty());
			
		assertTrue(analysis.getOverloaded().isEmpty());
		assertTrue(analysis.getPossibleOverloaded().isEmpty());
		assertTrue(analysis.getSignificantDecreasingPMs().isEmpty());
		assertTrue(analysis.getSignificantIncreasingPMs().isEmpty());
		assertEquals(1, analysis.getSuspendedVMs().size());
		
		
		// reset the defaults					
		AACHelper.resetCounter();
		resetDefaults();
	}
		
	// test the A3-algorithm of analyzing
	/**
	 * 
	 */
	@Test
	public void analyzingTestRun3() throws VMManagementException, NetworkException,
	InvalidPropertiesFormatException, IOException {

		loadProps();
		props.setProperty("defaultP", "P3");
		props.setProperty("defaultA", "A3");
		saveProps();
		
		// instantiate pms and vms
		testPM1.turnon();
		testPM2.turnon();
		testPM3.turnon();
		testPM4.turnon();
				
		Timed.simulateUntilLastEvent();
		switchOnVM(VM1, mediumConstraints, testPM1, true);
		switchOnVM(VM2, mediumConstraints, testPM2, true);
		switchOnVM(VM3, smallConstraints, testPM3, true);
		switchOnVM(VM4, smallConstraints, testPM3, true);
		Timed.simulateUntilLastEvent();

		AutoAdjustingConsolidator consolidator = new AutoAdjustingConsolidator(toConsolidate, consFreq, true, true, true);
		Timed.simulateUntil(Timed.getFireCount() + 400000);		
		
		
		// define tests TODO fix correlation error
		
		AnalyzerOutput analysis = consolidator.getAnalyzerOutput();
					
		assertFalse(analysis.getMapping().isEmpty());
					
		assertTrue(analysis.getOverloaded().isEmpty());
		assertTrue(analysis.getPossibleOverloaded().isEmpty());
		assertTrue(analysis.getSignificantDecreasingPMs().isEmpty());
		assertTrue(analysis.getSignificantIncreasingPMs().isEmpty());
		assertTrue(analysis.getSuspendedVMs().isEmpty());
		
		assertTrue(!analysis.getCorrelations().isEmpty());		
		
		assertTrue(!analysis.getSizes().isEmpty());
		
		// reset the defaults					
		AACHelper.resetCounter();
		resetDefaults();
	}
		
	// test the A4-algorithm of analyzing
	/**
	 * For this test we need to create at least two values for each pm and
	 * vm inside the MonitorInformation to create and compare significant changes.
	 */
	@Test
	public void analyzingTestRun4() throws VMManagementException, NetworkException,
	InvalidPropertiesFormatException, IOException {

		loadProps();
		props.setProperty("defaultP", "P4");
		props.setProperty("defaultA", "A4");
		saveProps();
		
		// instantiate pms and vms
		testPM1.turnon();
		testPM2.turnon();
		testPM3.turnon();
		testPM4.turnon();
				
		// we need at least two iterations with at least one significant change
		Timed.simulateUntilLastEvent();
		switchOnVM(VM1, mediumConstraints, testPM1, true);
		switchOnVM(VM2, mediumConstraints, testPM2, true);
		switchOnVM(VM3, smallConstraints, testPM3, true);
		switchOnVM(VM4, smallConstraints, testPM3, true);
		Timed.simulateUntilLastEvent();

		AutoAdjustingConsolidator consolidator = new AutoAdjustingConsolidator(toConsolidate, consFreq, true, true, true);
		Timed.simulateUntil(Timed.getFireCount() + 400000);
			
		switchOnVM(VM5, mediumConstraints, testPM3, false);
		VM2.destroy(true);
			
		Timed.simulateUntil(Timed.getFireCount() + 400000);
			
					
		// define tests
		// at this point there should be no change since the load of no VM has been changed,
		// starting or stopping VMs does not count as a significant change
			
		AnalyzerOutput analysis = consolidator.getAnalyzerOutput();
			
		assertFalse(analysis.getMapping().isEmpty());
			
		assertTrue(analysis.getOverloaded().isEmpty());
		assertTrue(analysis.getPossibleOverloaded().isEmpty());
		assertTrue(analysis.getSignificantDecreasingPMs().size() == 0);
		assertTrue(analysis.getSignificantIncreasingPMs().size() == 0);
		assertTrue(analysis.getSuspendedVMs().isEmpty());
		
		
		// reset the defaults					
		AACHelper.resetCounter();
		resetDefaults();
	}
	
	// test the P1-algorithm of planning
	@Test
	public void planningTestRun1() throws VMManagementException, NetworkException,
	InvalidPropertiesFormatException, IOException {

//		loadProps();
//		props.setProperty("defaultP", "P1");
//		props.setProperty("defaultA", "A1");
//		saveProps();
		
		// instantiate pms and vms
		testPM1.turnon();
		testPM2.turnon();
		testPM3.turnon();
		testPM4.turnon();
					
		Timed.simulateUntilLastEvent();
		switchOnVM(VM1, mediumConstraints, testPM1, true);
		switchOnVM(VM2, mediumConstraints, testPM2, true);
		switchOnVM(VM3, smallConstraints, testPM3, true);
		switchOnVM(VM4, smallConstraints, testPM3, true);
		Timed.simulateUntilLastEvent();
		
		AutoAdjustingConsolidator consolidator = new AutoAdjustingConsolidator(toConsolidate, consFreq, true, true, true);
		
		Timed.simulateUntil(Timed.getFireCount() + 400000);
			
			
		// define tests
		// there should be no changes except PM4, which should be off now
			
		Plan plan = consolidator.getPlan();
		for(int i = 0; i < plan.getOldVMs().length; i++) {
			assertEquals(plan.getOldVMs()[i], plan.getImprovedItems()[i]);
		}
		
		assertEquals(testPM4.getState(), PhysicalMachine.State.OFF);
		
		
		// reset the defaults					
		AACHelper.resetCounter();
		resetDefaults();
	}
	
	// test the P2-algorithm of planning
	@Test
	public void planningTestRun2() throws VMManagementException, NetworkException,
	InvalidPropertiesFormatException, IOException {
		
		loadProps();
		props.setProperty("defaultP", "P2");
		props.setProperty("defaultA", "A2");
		saveProps();
		
		// instantiate pms and vms
		// TODO we cannot overload a PM manually
		testPM1.turnon();
		testPM2.turnon();
		testPM3.turnon();
		testPM4.turnon();
					
		Timed.simulateUntilLastEvent();
		switchOnVM(VM1, mediumConstraints, testPM1, true);
		switchOnVM(VM2, mediumConstraints, testPM2, true);
		switchOnVM(VM3, smallConstraints, testPM3, true);
		switchOnVM(VM4, smallConstraints, testPM3, true);
		switchOnVM(VM5, smallConstraints, testPM3, true);
		switchOnVM(VM6, smallConstraints, testPM3, true);
		
		VM2.suspend();
		Timed.simulateUntilLastEvent();
		
		new AutoAdjustingConsolidator(toConsolidate, consFreq, true, true, true);
		Timed.simulateUntil(Timed.getFireCount() + 400000);
			
			
		// define tests
			
		
		
		// reset the defaults					
		AACHelper.resetCounter();
		resetDefaults();
		
		// problem here, sometimes it is running, sometimes suspended
		assertEquals(State.RUNNING, VM2.getState());
	}
		
	// test the P3-algorithm of planning
	@Test
	public void planningTestRun3() throws VMManagementException, NetworkException,
	InvalidPropertiesFormatException, IOException {

		loadProps();
		props.setProperty("defaultP", "P3");
		props.setProperty("defaultA", "A3");
		saveProps();
		
		// instantiate pms and vms
		testPM1.turnon();
		testPM2.turnon();
		testPM3.turnon();
		testPM4.turnon();
					
		Timed.simulateUntilLastEvent();
		switchOnVM(VM1, mediumConstraints, testPM1, true);
		switchOnVM(VM2, mediumConstraints, testPM2, true);
		switchOnVM(VM3, smallConstraints, testPM3, true);
		switchOnVM(VM4, smallConstraints, testPM3, true);
		Timed.simulateUntilLastEvent();
		
		new AutoAdjustingConsolidator(toConsolidate, consFreq, true, true, true);
		Timed.simulateUntil(Timed.getFireCount() + 400000);
			
		// hard to define tests, but if the test finishes, a solution is found
			
		// reset the defaults					
		AACHelper.resetCounter();
		resetDefaults();
		
	}
		
	// test the P4-algorithm of planning
	@Test
	public void planningTestRun4() throws VMManagementException, NetworkException,
	InvalidPropertiesFormatException, IOException {

		loadProps();
		props.setProperty("defaultP", "P4");
		props.setProperty("defaultA", "A4");
		saveProps();
		
		// instantiate pms and vms
		testPM1.turnon();
		testPM2.turnon();
		testPM3.turnon();
		testPM4.turnon();
					
		Timed.simulateUntilLastEvent();
		switchOnVM(VM1, mediumConstraints, testPM1, true);
		switchOnVM(VM2, mediumConstraints, testPM2, true);
		switchOnVM(VM3, smallConstraints, testPM3, true);
		switchOnVM(VM4, smallConstraints, testPM3, true);
		Timed.simulateUntilLastEvent();
		
		new AutoAdjustingConsolidator(toConsolidate, consFreq, true, true, true);
		
		Timed.simulateUntil(Timed.getFireCount() + 400000);
			
			
		// define tests TODO we need to adjust load to do this
		
		// reset the defaults					
		AACHelper.resetCounter();
		resetDefaults();		
	}
	
	// test the execution
	@Test
	public void executionTestRun() throws VMManagementException, NetworkException, 
	InvalidPropertiesFormatException, IOException {

//		loadProps();
//		props.setProperty("defaultP", "P1");
//		props.setProperty("defaultA", "A1");
//		saveProps();
		
		// instantiate pms and vms
		testPM1.turnon();
		testPM2.turnon();
		testPM3.turnon();
		testPM4.turnon();
						
		Timed.simulateUntilLastEvent();
		switchOnVM(VM1, mediumConstraints, testPM1, true);
		switchOnVM(VM2, mediumConstraints, testPM2, true);
		switchOnVM(VM3, smallConstraints, testPM3, true);
		switchOnVM(VM4, smallConstraints, testPM3, true);
		Timed.simulateUntilLastEvent();

		AutoAdjustingConsolidator consolidator = new AutoAdjustingConsolidator(toConsolidate, consFreq, true, true, true);

		Timed.simulateUntil(Timed.getFireCount() + 400000);
			
		// define tests
		assertTrue(consolidator.hasExecuterFinished());
		
		// reset the defaults					
		AACHelper.resetCounter();
		resetDefaults();
	}
	
	// auto-adjust, static
	@Test
	public void testRunStatic() throws VMManagementException, NetworkException {
		
	}
	
	// auto-adjust, dynamic 
	@Test
	public void testRunDynamic() throws VMManagementException, NetworkException {
		// instantiate pms and vms
		testPM1.turnon();
		testPM2.turnon();
		testPM3.turnon();
		testPM4.turnon();
						
		Timed.simulateUntilLastEvent();
		switchOnVM(VM1, mediumConstraints, testPM1, true);
		switchOnVM(VM2, mediumConstraints, testPM2, true);
		switchOnVM(VM3, smallConstraints, testPM3, true);
		switchOnVM(VM4, smallConstraints, testPM3, true);
		Timed.simulateUntilLastEvent();

		new AutoAdjustingConsolidator(toConsolidate, consFreq, false, true, true);
		Timed.simulateUntil(Timed.getFireCount() + 400000);
		AACHelper.resetCounter();
						
		// define tests
		
		
		
	}

}
